﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Character;
using UnityEngine;

public class CursorArrow : MonoBehaviour
{
    private const float BASE_ROTATION = 45.0f;

    [HideInInspector] public Controller controller;

    private void Update()
    {
        if (controller == null) return;

        Vector2 direction = controller.GetCursorPositionInWorldSpace()
                         - (Vector2) controller.transform.position;
        
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, Vector2.SignedAngle(Vector2.up, direction) + BASE_ROTATION);
    }
}
