﻿using Assets.Scripts.Character;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[RequireComponent(typeof(ProjectileWeapon))]
public class NormalProjectileWeapon : ProjectileWeapon
{
    private PlayerNetwork _playerNetwork;

    bool networkHasStarted = false;

    [SerializeField] Transform originOfProjectile;
    
    private void WhenNetworkStart(NetworkBehavior networkBehavior)
    {
        if (!_playerNetwork.networkObject.IsOwner) return;
        networkHasStarted = true;
    }
}