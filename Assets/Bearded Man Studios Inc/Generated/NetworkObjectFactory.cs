using BeardedManStudios.Forge.Networking.Frame;
using System;
using MainThreadManager = BeardedManStudios.Forge.Networking.Unity.MainThreadManager;

namespace BeardedManStudios.Forge.Networking.Generated
{
	public partial class NetworkObjectFactory : NetworkObjectFactoryBase
	{
		public override void NetworkCreateObject(NetWorker networker, int identity, uint id, FrameStream frame, Action<NetworkObject> callback)
		{
			if (networker.IsServer)
			{
				if (frame.Sender != null && frame.Sender != networker.Me)
				{
					if (!ValidateCreateRequest(networker, identity, id, frame))
						return;
				}
			}
			
			bool availableCallback = false;
			NetworkObject obj = null;
			MainThreadManager.Run(() =>
			{
				switch (identity)
				{
					case BlockDropItemNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new BlockDropItemNetworkObject(networker, id, frame);
						break;
					case BuildingNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new BuildingNetworkObject(networker, id, frame);
						break;
					case ChatManagerNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ChatManagerNetworkObject(networker, id, frame);
						break;
					case ChestNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ChestNetworkObject(networker, id, frame);
						break;
					case CubeForgeGameNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new CubeForgeGameNetworkObject(networker, id, frame);
						break;
					case DoubleProjectileWeaponNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new DoubleProjectileWeaponNetworkObject(networker, id, frame);
						break;
					case DropItemNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new DropItemNetworkObject(networker, id, frame);
						break;
					case ExampleProximityPlayerNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ExampleProximityPlayerNetworkObject(networker, id, frame);
						break;
					case ExplosionNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ExplosionNetworkObject(networker, id, frame);
						break;
					case GameManagerNetworkNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GameManagerNetworkNetworkObject(networker, id, frame);
						break;
					case GrapplingHookNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GrapplingHookNetworkObject(networker, id, frame);
						break;
					case MapManagerNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new MapManagerNetworkObject(networker, id, frame);
						break;
					case MoveCubeNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new MoveCubeNetworkObject(networker, id, frame);
						break;
					case NetworkCameraNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new NetworkCameraNetworkObject(networker, id, frame);
						break;
					case PlayerHealthNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerHealthNetworkObject(networker, id, frame);
						break;
					case PlayerNetworkNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerNetworkNetworkObject(networker, id, frame);
						break;
					case ProjectileGunNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ProjectileGunNetworkObject(networker, id, frame);
						break;
					case ProjectileNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ProjectileNetworkObject(networker, id, frame);
						break;
					case ProjectileWeaponNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ProjectileWeaponNetworkObject(networker, id, frame);
						break;
					case TestNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new TestNetworkObject(networker, id, frame);
						break;
				}

				if (!availableCallback)
					base.NetworkCreateObject(networker, identity, id, frame, callback);
				else if (callback != null)
					callback(obj);
			});
		}

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}