﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GrapplingHookObserver : MonoBehaviour, IObserver<bool>
{
    private Image _panel;
    [SerializeField] private Image iconBackground;
    [SerializeField] private Color unselectedColor;
    [SerializeField] private Color selectedColor;
    void Start()
    {
        _panel = GetComponent<Image>();
        Debug.Assert(_panel != null, "panel is null in GrapplingHookObserver");
        _panel.gameObject.SetActive(false);
    }

    public void Subscribe(IObservable<bool> provider)
    {
        if (provider != null)
            _unsubscriber = provider.Subscribe(this);
    }
    
    private IDisposable _unsubscriber;
    public GrapplingHookObserver(IDisposable unsubscriber)
    {
        _unsubscriber = unsubscriber;
    }

    public void OnCompleted()
    {
        _unsubscriber?.Dispose();      
    }

    public void OnError(Exception error)
    {
        Debug.LogError(error.Message);
    }

    public void OnNext(bool value)
    {
        if(value)SetGrapplingHookActive();
        else SetGrapplingHookInactive();
    }

    private void SetGrapplingHookActive()
    {
        _panel.gameObject.SetActive(true);
        iconBackground.color = selectedColor;
    }

    private void SetGrapplingHookInactive()
    {
        _panel.gameObject.SetActive(false);
        iconBackground.color = unselectedColor;

    }
}
