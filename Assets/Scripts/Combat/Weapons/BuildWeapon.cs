﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Character;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using EnvyDeserter.UI;
using UnityEngine;
using UnityEngine.Events;

public class BuildWeapon : Weapon, IFailBroadcaster
{
    #region Fields

    [SerializeField] private ContactFilter2D contactFilter; // Filter used when placing buildings
    private readonly RaycastHit2D[] hitBuffer = new RaycastHit2D[16];

    [SerializeField] private float range = 10.0f;
    public int buildingIndex;
    private Building buildingPrefab;
    private Building buildingIndicator;
    private bool canPlace;

    private Color transparent;
    private Color transparentRed;

    #endregion

    #region Unity Event Functions
    
    private void OnEnable()
    {
        buildingIndicator?.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        buildingIndicator?.gameObject.SetActive(false);
    }

    public override void Shoot(Vector2 direction) {}

    private List<UnityAction> _onFailedBuildCallbacks = new List<UnityAction>();

    public void AddOnFail(UnityAction callback)
    {
        _onFailedBuildCallbacks.Add(callback);
    }
    
    public new void NetworkStart()
    {
        base.NetworkStart();

        buildingPrefab = Game.Instance.BuildingData.Buildings[buildingIndex];
        buildingPrefab.Owner = Player;
        buildingIndicator = Instantiate(buildingPrefab, Game.Instance.Buildings);
        transparent = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        transparentRed = new Color(1.0f, 0.5f, 0.5f, 0.5f);
        buildingIndicator.SpriteRenderer.color = transparentRed;
        buildingIndicator.gameObject.layer = 1; // TransparentFX
        buildingIndicator.gameObject.GetComponent<ShootingTurret>().SetEnabled(false);

        if (!isCarriedByPlayer
            || Controller == null
            || PlayerNetwork != null
            && PlayerNetwork.networkObject != null
            && !PlayerNetwork.networkObject.IsOwner)
            return;

        inputMaster.BasicControls.Fire.performed += delegate
        {
            var target = Controller.GetCursorPositionInWorldSpace();
            PlaceBuilding((target - (Vector2) transform.position).normalized);
        };

    }

    protected void Update()
    {
        if (Controller == null) return;

        Vector2 cursor = Controller.GetCursorPositionInWorldSpace() - (Vector2) transform.position;

        if (cursor.magnitude > range) cursor = cursor.normalized * range;

        // If the buildingPrefab has an odd size, reposition it
        var cellSize = Game.Instance.Grid.cellSize.x;

        var modX = Mathf.Round(buildingIndicator.Collider.bounds.size.x) / cellSize % 2.0f;
        if (modX < 0.1f || modX > 1.9f) cursor.x += cellSize / 2.0f;
        var modY = Mathf.Round(buildingIndicator.Collider.bounds.size.y) / cellSize % 2.0f;
        if (modY < 0.1f || modY > 1.9f) cursor.y += cellSize / 2.0f;

#if UNITY_EDITOR
        gizmoCursor = (Vector2) transform.position + cursor;
#endif

        var count = Physics2D.Raycast(transform.position, cursor.normalized, contactFilter, hitBuffer,
            cursor.magnitude);
        var normal1 = Vector2.zero;
        var normal2 = Vector2.zero;

        if (count > 0)
        {
            var hit = hitBuffer[0];
            for (var i = 1; i < count; i++)
                if (hitBuffer[i].distance < hit.distance)
                    hit = hitBuffer[i];

            normal1 = Orthogonalize(hit.normal);

            var sourceToHit = hit.point - (Vector2) transform.position;
            var hitToCursor = cursor - sourceToHit;
            var projection = Vector2.Dot(hitToCursor, normal1) * normal1;

            var sourceToProjection = cursor - projection;
            var hitToProjection = sourceToProjection - sourceToHit;

            count = Physics2D.Raycast(
                (Vector2) transform.position + sourceToHit + Game.Instance.Grid.cellSize.x / 2.0f * normal1,
                hitToProjection.normalized, contactFilter, hitBuffer, hitToProjection.magnitude);

            if (count > 0)
            {
                hit = hitBuffer[0];
                for (var i = 1; i < count; i++)
                    if (hitBuffer[i].distance < hit.distance)
                        hit = hitBuffer[i];

                normal2 = Orthogonalize(hit.normal);

                hitToProjection = hit.distance * hitToProjection.normalized;
                sourceToProjection = sourceToHit + hitToProjection;
            }

            cursor = sourceToProjection;

#if UNITY_EDITOR
            gizmoHitPosition = hit.point;
            gizmoProjection = cursor + (Vector2) transform.position;
#endif
        }

#if UNITY_EDITOR
        gizmoNormal1 = normal1;
        gizmoNormal2 = normal2;
#endif

        cursor += (Vector2) transform.position;

        canPlace = normal1.magnitude < 0.5f
            ? PlaceNearby(cursor, buildingIndicator)
            : normal2.magnitude < 0.5f
                ? PlaceNearby(cursor, normal1, buildingIndicator)
                : PlaceNearby(cursor, normal1, normal2, buildingIndicator);

        buildingIndicator.SpriteRenderer.color = canPlace
            ? transparent
            : transparentRed;
    }

#if UNITY_EDITOR
    private Vector2 gizmoHitPosition;
    private Vector2 gizmoCursor;
    private Vector2 gizmoProjection;

    private Vector2 gizmoNormal1;
    private Vector2 gizmoNormal2;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(gizmoHitPosition, 0.2f);
        Gizmos.DrawLine(transform.position, gizmoHitPosition);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(gizmoCursor, 0.2f);
        Gizmos.DrawLine(gizmoHitPosition, gizmoCursor);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(gizmoProjection, 0.2f);
        Gizmos.DrawLine(gizmoCursor, gizmoProjection);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(gizmoProjection, gizmoProjection + gizmoNormal1);
        Gizmos.DrawLine(gizmoProjection, gizmoProjection + gizmoNormal2);
    }
#endif

    #endregion

    #region Public Methods

    public void PlaceBuilding(Vector2 direction)
    {
        if (!canPlace || !gameObject.activeSelf) return;

        int totalAmountOfCrystalsPossessed = Player.inventory.NumberOfCrystals + Player.Network.playerChestNetwork.ChestInventory.NumberOfCrystals;
        if (buildingPrefab.Cost > totalAmountOfCrystalsPossessed)
        {
#if UNITY_EDITOR
            Debug.Log("You only have " + totalAmountOfCrystalsPossessed.ToString()
                + " crystals and this building costs " + buildingPrefab.Cost.ToString() + " crystals to build.");
#endif
            foreach (UnityAction callback in _onFailedBuildCallbacks)
                callback.Invoke();
            
            return;
        }

        if (buildingPrefab.Cost <= Player.inventory.NumberOfCrystals)
            Player.inventory.UpdateInventory(DropType.Crystal, -buildingPrefab.Cost);
        else
        {
            int tempAmountOfCrystalsInPlayerInventory = buildingPrefab.Cost - Player.inventory.NumberOfCrystals;
            Player.inventory.UpdateInventory(DropType.Crystal, -Player.inventory.NumberOfCrystals);
            Player.Network.playerChestNetwork.ChestInventory.UpdateInventory(DropType.Crystal, -(tempAmountOfCrystalsInPlayerInventory));
        }

        Building newBuilding;
        if (NetworkManager.Instance == null) newBuilding = Instantiate(buildingPrefab);
        else newBuilding = NetworkManager.Instance.InstantiateBuilding(buildingIndex) as Building;

        if (newBuilding == null) return;

        newBuilding.Owner = Player;
        if (newBuilding.networkObject == null)
        {
            newBuilding.transform.position = buildingIndicator.transform.position;
            newBuilding.Team = Player.Stats.Team;
            newBuilding.Activated = true;
            newBuilding.ownerId = 0;
        }
        else
        {
            newBuilding.networkObject.position = buildingIndicator.transform.position;
            newBuilding.networkObject.team = (byte) Player.Stats.Team;
            newBuilding.networkObject.activated = true;

            
            newBuilding.ownerId = Player.Network.ownerID;
            newBuilding.networkStarted += newBuilding.SetIdAfterNetworkStart;
        }
    }

    #endregion

    #region Private Methods

    // Building indicator hits 2 walls
    private bool PlaceNearby(Vector2 position, Vector2 normal1, Vector2 normal2, Building building)
    {
        if (building == null) return false;

        var cellSize = Game.Instance.Grid.cellSize.x;

        var snapPosition = SnapToGrid(position, normal1 + normal2, building);
        
        var extents = building.Collider.bounds.extents;

        var offset = normal1 + normal2;
        offset.x *= Mathf.Round(extents.x) * cellSize;
        offset.y *= Mathf.Round(extents.y) * cellSize;

        snapPosition += offset;

        // Check for superposition
        building.Rigidbody.position = snapPosition;

        var count = building.Rigidbody.Cast(Vector2.zero, contactFilter, hitBuffer);

        // No superposition
        if (count <= 0)
            return building.Rigidbody.Cast(Vector2.up, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.left, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.down, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.right, contactFilter, hitBuffer, cellSize) > 0;

        // Superposition
        var n = Math.Max(Mathf.FloorToInt(extents.x / cellSize) + 1, Mathf.FloorToInt(extents.y / cellSize) + 1);

        for (var i = 0; i <= n; i++)
            for (var j = 0; j <= i; j++)
                if (             CastBuilding(building, snapPosition + i       * cellSize * normal1 + (i - j) * cellSize * normal2)
                    || i != j && CastBuilding(building, snapPosition + (i - j) * cellSize * normal1 + i       * cellSize * normal2))
                    return true;

        return false;
    }

    // Building indicator hits a wall
    private bool PlaceNearby(Vector2 position, Vector2 normal, Building building)
    {
        if (building == null) return false;

        var cellSize = Game.Instance.Grid.cellSize.x;

        var snapPosition = SnapToGrid(position, normal, building);

        var extents = building.Collider.bounds.extents;

        var offset = normal;
        offset.x *= Mathf.Round(extents.x) * cellSize;
        offset.y *= Mathf.Round(extents.y) * cellSize;

        snapPosition += offset;

        // Check for superposition
        building.Rigidbody.position = snapPosition;
        var count = building.Rigidbody.Cast(Vector2.zero, contactFilter, hitBuffer);

        // No superposition
        if (count <= 0)
            return building.Rigidbody.Cast(Vector2.up, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.left, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.down, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.right, contactFilter, hitBuffer, cellSize) > 0;

        // Superposition
        var n = Math.Max(Mathf.FloorToInt(extents.x / cellSize) + 1, Mathf.FloorToInt(extents.y / cellSize) + 1);

        var perpendicular = Vector2.Perpendicular(normal);

        for (var i = 0; i <= n; i++)
            for (var j = 0; j <= i; j++)
                if (             CastBuilding(building, snapPosition - j * cellSize * perpendicular + i * cellSize * normal)
                    || j != 0 && CastBuilding(building, snapPosition + j * cellSize * perpendicular + i * cellSize * normal)
                    ||           CastBuilding(building, snapPosition - i * cellSize * perpendicular + j * cellSize * normal)
                    || i != 0 && CastBuilding(building, snapPosition + i * cellSize * perpendicular + j * cellSize * normal))
                    return true;

        return false;
    }

    // Building indicator hits nothing
    private bool PlaceNearby(Vector2 position, Building building)
    {
        if (building == null) return false;

        var cellSize = Game.Instance.Grid.cellSize.x;

        var snapPosition = SnapToGrid(position, Vector2.one, building);

        // Check for superposition
        building.Rigidbody.position = snapPosition;
        var count = building.Rigidbody.Cast(Vector2.zero, contactFilter, hitBuffer);

        // No superposition
        if (count <= 0)
            return building.Rigidbody.Cast(Vector2.up, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.left, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.down, contactFilter, hitBuffer, cellSize) > 0
                   || building.Rigidbody.Cast(Vector2.right, contactFilter, hitBuffer, cellSize) > 0;

        // Superposition
        var extents = building.Collider.bounds.extents;
        var n = Math.Max(Mathf.FloorToInt(extents.x / cellSize) + 1, Mathf.FloorToInt(extents.y / cellSize) + 1);

        for (var i = 0; i <= n; i++)
        for (var j = 0; j <= i; j++)
            if (             CastBuilding(building, snapPosition + j * cellSize * Vector2.left  + i * cellSize * Vector2.up)
                || j != 0 && CastBuilding(building, snapPosition + j * cellSize * Vector2.right + i * cellSize * Vector2.up)
                ||           CastBuilding(building, snapPosition + i * cellSize * Vector2.left  + j * cellSize * Vector2.up)
                || i != 0 && CastBuilding(building, snapPosition + i * cellSize * Vector2.right + j * cellSize * Vector2.up)
                ||           CastBuilding(building, snapPosition + i * cellSize * Vector2.left  + j * cellSize * Vector2.down)
                || i != 0 && CastBuilding(building, snapPosition + i * cellSize * Vector2.right + j * cellSize * Vector2.down)
                ||           CastBuilding(building, snapPosition + j * cellSize * Vector2.left  + i * cellSize * Vector2.down)
                || j != 0 && CastBuilding(building, snapPosition + j * cellSize * Vector2.right + i * cellSize * Vector2.down))
                return true;

        return false;
    }

    private bool CastBuilding(Building building, Vector2 position)
    {
        building.Rigidbody.position = position;
        return building.Rigidbody.Cast(Vector2.zero, contactFilter, hitBuffer) <= 0;
    }

    private Vector2 SnapToGrid(Vector2 position, Vector2 direction, Building building)
    {
        if (Mathf.Abs(direction.x) < 0.5f) direction.x = 1.0f;
        if (Mathf.Abs(direction.y) < 0.5f) direction.y = 1.0f;

        var cellSize = Game.Instance.Grid.cellSize.x;
        var snapPosition = new Vector2(Mathf.Floor(position.x + 0.01f / cellSize) * cellSize,
            Mathf.Floor(position.y + 0.01f / cellSize) * cellSize);

        // If the buildingPrefab has an odd size, reposition it
        var modX = Mathf.Round(building.Collider.bounds.size.x) / cellSize % 2.0f;
        if (modX > 0.9f && modX < 1.1f) snapPosition.x += cellSize / 2.0f * direction.x;
        var modY = Mathf.Round(building.Collider.bounds.size.y) / cellSize % 2.0f;
        if (modY > 0.9f && modY < 1.1f) snapPosition.y += cellSize / 2.0f * direction.y;

        return snapPosition;
    }

    private Vector2 Orthogonalize(Vector2 vector)
        => vector.y > 0.7f ? Vector2.up
        : vector.x < -0.7f ? Vector2.left
        : vector.x > 0.7f ? Vector2.right
        : Vector2.down;

    #endregion
}