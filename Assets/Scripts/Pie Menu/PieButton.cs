﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public abstract class PieButton : MonoBehaviour
{
    [SerializeField] private AnimationCurve alphaCurve;

    public bool selected;

    private float currentAlphaTime;

    public Image ButtonImage { get; private set; }
    public RectTransform Child { get; private set; }
    public RectTransform RectTransform { get; private set; }

    public abstract void Activate();

    private void Awake()
    {
        ButtonImage = GetComponent<Image>();
        RectTransform = GetComponent<RectTransform>();
        if (RectTransform.childCount > 0) Child = RectTransform.GetChild(0).GetComponent<RectTransform>();
        
        currentAlphaTime = alphaCurve[0].time;

        selected = false;
        ButtonImage.color = new Color(ButtonImage.color.r,
            ButtonImage.color.g,
            ButtonImage.color.b,
            alphaCurve.Evaluate(currentAlphaTime));
    }

    private void Update()
    {
        currentAlphaTime += selected ? Time.deltaTime : -Time.deltaTime;
        if (currentAlphaTime > alphaCurve[alphaCurve.length - 1].time) currentAlphaTime = alphaCurve[alphaCurve.length - 1].time;
        if (currentAlphaTime < alphaCurve[0].time) currentAlphaTime = alphaCurve[0].time;

        ButtonImage.color = new Color(ButtonImage.color.r,
            ButtonImage.color.g,
            ButtonImage.color.b,
            alphaCurve.Evaluate(currentAlphaTime));
    }
}
