﻿using System;
using System.Collections;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using Platformer.Mechanics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : ColorChangeOnHit, IDamageable
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int currentHealth;

    public int CurrentHealth => currentHealth;
    public int MaxHealth => maxHealth;

    [SerializeField] PlayerNetwork playerNetwork;

    public SpawnPoint spawnPoint;

    private Player player;

    bool canTakeDamage = true;
    bool canHeal = true;
    
    [HideInInspector] public Slider healthBarSlider;  //UI slider used to represent the current HP of a player, 
    
    Animator playerAnimator;
    bool networkHasStarted = false;

    [SerializeField] private float blinkPeriod = 0.1f;
    [SerializeField] private float invincibilityDuration = 0.5f;

    [SerializeField] private float hurtAnimationDuration;
    [SerializeField] private float deathAnimationDuration;
    [SerializeField] private float spawnAnimationDuration;

    //Coroutines
    IEnumerator deathAnimationCoroutine;
    IEnumerator respawnAnimationCoroutine;

    [SerializeField] bool dropIndividualItemsInsteadOfPackWhenDying = false;

    private void Start()
    {
        currentHealth = maxHealth;
        if(healthBarSlider != null) healthBarSlider.value = currentHealth;

        playerAnimator = GetComponent<Animator>();
        AnimationClip[] clips = playerAnimator.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            switch (clip.name)
            {
                case "PlayerHurt":
                    hurtAnimationDuration = clip.length;
                    break;
                case "PlayerDeath":
                    deathAnimationDuration = clip.length;
                    break;
                case "PlayerSpawn":
                    spawnAnimationDuration = clip.length;
                    break;
                default:
                    break;
            }
        }
        
        playerNetwork = GetComponent<PlayerNetwork>();
        if(playerNetwork != null) playerNetwork.networkStarted += InitAfterNetworkStart;

        deathAnimationCoroutine = PlayDeathAnimation();
        respawnAnimationCoroutine = PlaySpawnAnimation();
    }

    void InitAfterNetworkStart(NetworkBehavior networkBehavior)
    {
        networkHasStarted = true;
        player = GetComponentInParent<Player>();
    }
    
    public void Init()
    {
        transform.position = spawnPoint.transform.position;
        currentHealth = maxHealth;
        if (healthBarSlider != null) healthBarSlider.value = currentHealth;
    }

    private Coroutine currentInvincibilityCoroutine;
    public new void TakeDamage(int damage)
    {
        if (damage >= 0 && canTakeDamage || damage < 0 && canHeal)
        {
            currentHealth -= damage;
            
            if(damage >= 0)
            {
                base.TakeDamage(damage);

                if (currentInvincibilityCoroutine != null) StopCoroutine(currentInvincibilityCoroutine);
                currentInvincibilityCoroutine = StartCoroutine(InvincibilityCoroutine(invincibilityDuration));

                playerAnimator.SetTrigger("hurt");

                if (playerNetwork != null && playerNetwork.playerChestNetwork != null)
                    playerNetwork.playerChestNetwork.PlayerWasHit();
            }

            if (healthBarSlider != null) healthBarSlider.value = currentHealth;
        }

        if (currentHealth > maxHealth) currentHealth = maxHealth;

        if (currentHealth <= 0 && !playerAnimator.GetBool("dead")) Die();
    }

    [ContextMenu("Take 40 Damage")]
    public void Take40Damage() => TakeDamage(40);

    [ContextMenu("Die")]
    public new void Die()
    {
        base.Die();

        Inventory myInventory = GetComponent<Inventory>();

        if (myInventory != null)
        {
            if (myInventory.NumberOfDirtBlocks > 0)
                myInventory.DropPack(DropType.Block, myInventory.NumberOfDirtBlocks, dropIndividualItemsInsteadOfPackWhenDying);

            if (myInventory.NumberOfCrystals > 0)
                myInventory.DropPack(DropType.Crystal, myInventory.NumberOfCrystals, dropIndividualItemsInsteadOfPackWhenDying);
        }
        
        StopCoroutine(deathAnimationCoroutine);
        deathAnimationCoroutine = PlayDeathAnimation();
        StartCoroutine(PlayDeathAnimation());
    }

    IEnumerator PlayDeathAnimation()
    {
        canTakeDamage = false;
        canHeal = false;
        player.Controller.GetInputMaster().Disable();
        playerAnimator?.SetBool("dead", true);
        yield return new WaitForSeconds(deathAnimationDuration);

        if (playerNetwork != null
            && playerNetwork.networkObject != null
            && !playerNetwork.networkObject.hasChest)
        {
            GameManagerNetwork.gameManagerNetwork.PlayerLost(GetComponent<Player>());
            yield break;
        }

        Init();
        StopCoroutine(respawnAnimationCoroutine);
        respawnAnimationCoroutine = PlaySpawnAnimation();
        StartCoroutine(respawnAnimationCoroutine);
    }

    IEnumerator PlaySpawnAnimation()
    {
        canTakeDamage = true;
        canHeal = true;
        playerAnimator?.SetBool("dead", false);
        yield return new WaitForSeconds(spawnAnimationDuration);
        player.Controller.GetInputMaster().Enable();
    }

    private IEnumerator InvincibilityCoroutine(float duration)
    {
        canTakeDamage = false;

        var timer = 0.0f;
        var blinkTimer = 0.0f;
        var alpha = renderer.color.a;

        while (timer < duration)
        {
            if (blinkTimer < blinkPeriod / 2.0f)
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha / 2.0f);
            else
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);

            yield return new WaitForSeconds(Time.deltaTime);
            timer += Time.deltaTime;
            blinkTimer += Time.deltaTime;

            if (blinkTimer > blinkPeriod) blinkTimer -= blinkPeriod;
        }

        renderer.color = originalColor;

        canTakeDamage = true;
    }
}
