﻿using System.Collections;
using System.Collections.Generic;
using EnvyDeserter.UI;
using UnityEngine;
using UnityEngine.Events;

public class PickaxeAnimator : MonoBehaviour, IFailBroadcaster
{
    // Visuals
    [SerializeField] private float _blockTravelSpeed = 10.0f; // Units/s

    [SerializeField] private float _blockRotationSpeed = 1440.0f; // Degrees/s
    [SerializeField] private GameObject _blockPrefab;
    private int travellingBlocks;

    private LineRenderer lineRenderer;
    [SerializeField] private SpriteRenderer _endOfRay;

    private PickaxeGun pickaxe;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer == null)
            Debug.LogWarning(name + " has a PickaxeAnimator component but no LineRenderer.");
        else lineRenderer.positionCount = 2;

        if (_endOfRay == null)
            Debug.LogWarning(name + "'s _endOfRay field is null.");
        else _endOfRay.enabled = false;

        if (_blockPrefab == null)
            Debug.LogWarning(name + "'s _blockPrefab field is null.");

        pickaxe = GetComponent<PickaxeGun>(); // Gets deleted if not the owner on the network
    }
    
    public void MineAnimation(Vector2 target, bool enable)
    {
        if (enable)
        {
            _endOfRay.enabled = true;
            lineRenderer.enabled = true;
        }

        _endOfRay.transform.position = target;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, target);

        if (!enable)
        {
            _endOfRay.enabled = false;
            lineRenderer.enabled = false;
        }
    }

    public void PlaceBlockAnimation(Vector2 target)
        => StartCoroutine(PlaceBlockAnimationCoroutine(target));


    public void AddOnFail(UnityAction callback)
    {
        _onFailedToPlaceBlockCallbacks.Add(callback);
    }
    
    private readonly List<UnityAction> _onFailedToPlaceBlockCallbacks = new List<UnityAction>();
    private IEnumerator PlaceBlockAnimationCoroutine(Vector2 toPosition)
    {
        Vector3Int[] hoverTiles = null;
        var freeTiles = 0;

        if (pickaxe != null)
        {
            // Deep copy the hovered tiles at launch
            hoverTiles = new Vector3Int[pickaxe.MapManager.HoverTiles.Length];
            for (var i = 0; i < hoverTiles.Length; i++)
                hoverTiles[i] = pickaxe.MapManager.HoverTiles[i];

            int totalAmountOfBlocksPossessed = pickaxe.Player.inventory.NumberOfDirtBlocks + pickaxe.Player.Network.playerChestNetwork.ChestInventory.NumberOfDirtBlocks;
            freeTiles = pickaxe.MapManager.CheckFreeTilesAtPositions(
                totalAmountOfBlocksPossessed - travellingBlocks, hoverTiles);

            if (freeTiles <= 0)
            {
                foreach (UnityAction callback in _onFailedToPlaceBlockCallbacks)
                    callback.Invoke();
                yield break;
            }

            travellingBlocks += freeTiles;
        }

        GameObject block = Instantiate(_blockPrefab, transform.position, Quaternion.identity);

        Transform blockTransform = block.transform;
        Transform childTransform = blockTransform.GetChild(0);

        Vector2 trajectory = toPosition - (Vector2) transform.position;
        var travelledDistance = 0.0f;
        float distanceToTravel = trajectory.magnitude;
        Vector2 direction = trajectory.normalized;

        while (travelledDistance < distanceToTravel)
        {
            yield return new WaitForSeconds(Time.deltaTime);

            var distance = Time.deltaTime * _blockTravelSpeed;
            blockTransform.Translate(distance * direction);
            if (childTransform != null)
                childTransform.rotation *= Quaternion.Euler(0.0f, 0.0f, Time.deltaTime * _blockRotationSpeed);
            travelledDistance += distance;
        }

        Destroy(block);
        // TODO placement feedback goes here (sound, particles...)

        if (pickaxe != null)
        {
            int totalAmountOfBlocksPossessed = pickaxe.Player.inventory.NumberOfDirtBlocks + pickaxe.Player.Network.playerChestNetwork.ChestInventory.NumberOfDirtBlocks;
            var placedTiles = pickaxe.MapManager.PlaceTilesAtPositions(totalAmountOfBlocksPossessed, hoverTiles);

            if(placedTiles <= pickaxe.Player.inventory.NumberOfDirtBlocks)
            {
                pickaxe.Player.inventory.UpdateInventory(DropType.Block, -placedTiles);
            }
            else
            {
                int tempAmountOfBlocksInPlayerInventory = placedTiles - pickaxe.Player.inventory.NumberOfDirtBlocks;
                pickaxe.Player.inventory.UpdateInventory(DropType.Block, -pickaxe.Player.inventory.NumberOfDirtBlocks);
                pickaxe.Player.Network.playerChestNetwork.ChestInventory.UpdateInventory(DropType.Block, -(tempAmountOfBlocksInPlayerInventory));
            }

            travellingBlocks -= freeTiles;
        }
    }
}
