﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class DoubleProjectileWeaponNetwork : DoubleProjectileWeaponBehavior
{
    private DoubleProjectileWeapon weapon;
    private PlayerNetwork playerNetwork;
    public PlayerNetwork PlayerNetwork => playerNetwork ?? (playerNetwork = weapon.PlayerNetwork);

    private void Awake()
    {
        weapon = GetComponent<DoubleProjectileWeapon>();
        if (weapon == null)
            Debug.LogWarning(name + " has a ProjectileWeaponNetwork component but no ProjectileWeapon.");
    }

    public override void Shoot(RpcArgs args)
    {
        var owner = args.GetNext<uint>();
        if (owner != PlayerNetwork.ownerID) return;

        var position = args.GetNext<Vector2>();
        var direction = args.GetNext<Vector2>();

        if (args.GetNext<bool>())
            MainThreadManager.Run(() => weapon.InitAltProjectile(owner, position, direction));
        else
            MainThreadManager.Run(() => weapon.InitProjectile(owner, position, direction));
    }
}
