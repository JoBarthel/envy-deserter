using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedRPC("{\"types\":[[\"uint\"][\"int\"][\"Vector3\"][\"int\", \"int\"][\"int\", \"uint\", \"int\"][\"Color\"]]")]
	[GeneratedRPCVariableNames("{\"types\":[[\"newOwnerId\"][\"damage\"][\"newPosition\"][\"dropTypeIndex\", \"amountAdded\"][\"amountAdded\", \"playerId\", \"dropItem\"][\"newColor\"]]")]
	public abstract partial class BuildingBehavior : NetworkBehavior
	{
		public const byte RPC_SET_OWNER_ID = 0 + 5;
		public const byte RPC_TAKE_DAMAGE = 1 + 5;
		public const byte RPC_UPDATE_CHEST_POSITION = 2 + 5;
		public const byte RPC_UPDATE_CHEST_INVENTORY = 3 + 5;
		public const byte RPC_ADD_DROP_ITEM_TO_INVENTORY = 4 + 5;
		public const byte RPC_CHANGE_TURRET_RANGE_COLOR = 5 + 5;
		
		public BuildingNetworkObject networkObject = null;

		public override void Initialize(NetworkObject obj)
		{
			// We have already initialized this object
			if (networkObject != null && networkObject.AttachedBehavior != null)
				return;
			
			networkObject = (BuildingNetworkObject)obj;
			networkObject.AttachedBehavior = this;

			base.SetupHelperRpcs(networkObject);
			networkObject.RegisterRpc("SetOwnerId", SetOwnerId, typeof(uint));
			networkObject.RegisterRpc("TakeDamage", TakeDamage, typeof(int));
			networkObject.RegisterRpc("UpdateChestPosition", UpdateChestPosition, typeof(Vector3));
			networkObject.RegisterRpc("UpdateChestInventory", UpdateChestInventory, typeof(int), typeof(int));
			networkObject.RegisterRpc("AddDropItemToInventory", AddDropItemToInventory, typeof(int), typeof(uint), typeof(int));
			networkObject.RegisterRpc("ChangeTurretRangeColor", ChangeTurretRangeColor, typeof(Color));

			networkObject.onDestroy += DestroyGameObject;

			if (!obj.IsOwner)
			{
				if (!skipAttachIds.ContainsKey(obj.NetworkId)){
					uint newId = obj.NetworkId + 1;
					ProcessOthers(gameObject.transform, ref newId);
				}
				else
					skipAttachIds.Remove(obj.NetworkId);
			}

			if (obj.Metadata != null)
			{
				byte transformFlags = obj.Metadata[0];

				if (transformFlags != 0)
				{
					BMSByte metadataTransform = new BMSByte();
					metadataTransform.Clone(obj.Metadata);
					metadataTransform.MoveStartIndex(1);

					if ((transformFlags & 0x01) != 0 && (transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() =>
						{
							transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform);
							transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform);
						});
					}
					else if ((transformFlags & 0x01) != 0)
					{
						MainThreadManager.Run(() => { transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform); });
					}
					else if ((transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() => { transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform); });
					}
				}
			}

			MainThreadManager.Run(() =>
			{
				NetworkStart();
				networkObject.Networker.FlushCreateActions(networkObject);
			});
		}

		protected override void CompleteRegistration()
		{
			base.CompleteRegistration();
			networkObject.ReleaseCreateBuffer();
		}

		public override void Initialize(NetWorker networker, byte[] metadata = null)
		{
			Initialize(new BuildingNetworkObject(networker, createCode: TempAttachCode, metadata: metadata));
		}

		private void DestroyGameObject(NetWorker sender)
		{
			MainThreadManager.Run(() => { try { Destroy(gameObject); } catch { } });
			networkObject.onDestroy -= DestroyGameObject;
		}

		public override NetworkObject CreateNetworkObject(NetWorker networker, int createCode, byte[] metadata = null)
		{
			return new BuildingNetworkObject(networker, this, createCode, metadata);
		}

		protected override void InitializedTransform()
		{
			networkObject.SnapInterpolations();
		}

		/// <summary>
		/// Arguments:
		/// uint newOwnerId
		/// </summary>
		public abstract void SetOwnerId(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int damage
		/// </summary>
		public abstract void TakeDamage(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// Vector3 newPosition
		/// </summary>
		public abstract void UpdateChestPosition(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int dropTypeIndex
		/// int amountAdded
		/// </summary>
		public abstract void UpdateChestInventory(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int amountAdded
		/// uint playerId
		/// int dropItem
		/// </summary>
		public abstract void AddDropItemToInventory(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// Color newColor
		/// </summary>
		public abstract void ChangeTurretRangeColor(RpcArgs args);

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}