﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiModeSelector : MonoBehaviour
{
    private Image _background;

    private void Start()
    {
        _background = GetComponent<Image>();
    }

    public void SetColor(Color color)
    {
        _background.color = color;
    }
}
