﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour
{
    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;

    [Range(0, 100)] public int randomFillPercent;

    [SerializeField] int numberOfSmoothIterations = 5;
    [SerializeField] int numberOfSmoothIterationsAfterFirstSmooth = 5;
    [SerializeField] [Range(0, 9)] int referenceToMapSmoothing = 4;
    [SerializeField] [Range(0, 1)] int smoothMode = 0;
    [SerializeField] [Range(0, 1)] int smoothModeAfterFirstSmooth = 0;

    public int[,] map;
    public int[,] mapBackground;

    //Color[] listOfColors = { Color.white, Color.black, Color.green, Color.blue, Color.yellow };

    [Space]
    [Header("Blocks Settings")]
    [SerializeField] BlockSettings[] blocksSettings;
    [Space]
    [Header("Resources Settings")]
    [SerializeField] ResourcesSettings[] resourcesSettings;

    /*
    [Space]
    [Header("Green Region")]
    [SerializeField] int greenAmountOfBlocksToAdd = 500;
    [SerializeField] float greenProbabilityToGoUp = .5f;
    [SerializeField] float greenProbabilityToGoDown = .5f;
    [SerializeField] float greenProbabilityToGoLeft = .5f;
    [SerializeField] float greenProbabilityToGoRight = .5f;
    [SerializeField] bool greenOverrideOtherBlocks = false;

    [Space]
    [Header("Blue Region")]
    [SerializeField] int blueAmountOfBlocksToAdd = 500;
    [SerializeField] float blueProbabilityToGoUp = .5f;
    [SerializeField] float blueProbabilityToGoDown = .5f;
    [SerializeField] float blueProbabilityToGoLeft = .5f;
    [SerializeField] float blueProbabilityToGoRight = .5f;
    [SerializeField] bool blueOverrideOtherBlocks = false;

    [Space]
    [Header("Yellow Region")]
    [SerializeField] int yellowAmountOfBlocksToAdd = 500;
    [SerializeField] float yellowProbabilityToGoUp = .5f;
    [SerializeField] float yellowProbabilityToGoDown = .5f;
    [SerializeField] float yellowProbabilityToGoLeft = .5f;
    [SerializeField] float yellowProbabilityToGoRight = .5f;
    [SerializeField] bool yellowOverrideOtherBlocks = false;*/


    int totalAmountOfBlocks = 0;

    //private void Start()
    //{
    //    GenerateMap();
    //}

    //private void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        GenerateMap();
    //    }
    //}

    public void GenerateMap()
    {
        map = new int[width, height];
        mapBackground = new int[width, height];
        RandomFillMap();
        SmoothMap(numberOfSmoothIterations, smoothMode);
        SmoothMap(numberOfSmoothIterationsAfterFirstSmooth, smoothModeAfterFirstSmooth);

        for (int i = 0; i < blocksSettings.Length; i++)
        {
            FillRegion(1, i + 2, blocksSettings[i].amountOfBlocksToAdd, blocksSettings[i].probabilityToGoUp, blocksSettings[i].probabilityToGoDown, blocksSettings[i].probabilityToGoLeft, blocksSettings[i].probabilityToGoRight, blocksSettings[i].overrideOtherBlocks, 1);
        }


        for (int i = 0; i < resourcesSettings.Length; i++)
        {
            for (int j = 0; j < resourcesSettings[i].amountOfOreVeins; j++)
            {
                FillRegion(1, 100 + i, resourcesSettings[i].amountOfBlocksToAdd, resourcesSettings[i].probabilityToGoUp, resourcesSettings[i].probabilityToGoDown, resourcesSettings[i].probabilityToGoLeft, resourcesSettings[i].probabilityToGoRight, resourcesSettings[i].overrideOtherBlocks, 1);
            }
        }

    }

    private void FillRegion(int regionGroupToOverride, int regionGroupToAdd, int amountOfBlocksToAdd, float probabilityToGoUp, float probabilityToGoDown, float probabilityToGoLeft, float probabilityToGoRight, bool overrideTile = false, int fillStep = 1)
    {
        int amountOfBlocksAdded = 0;
        do
        {
            int chosenI, chosenJ;
            do
            {
                chosenI = UnityEngine.Random.Range(0, map.GetLength(0));
                chosenJ = UnityEngine.Random.Range(0, map.GetLength(1));
            } while (map[chosenI, chosenJ] == 0 || 
                    (regionGroupToAdd >= 100 && (chosenI <= 2 || chosenI >= map.GetLength(0) - 3 || chosenJ <= 2 || chosenJ >= map.GetLength(1) - 3))
            );


            amountOfBlocksAdded += FloodFill(map, chosenI, chosenJ, regionGroupToOverride, regionGroupToAdd, amountOfBlocksToAdd, probabilityToGoUp, probabilityToGoDown, probabilityToGoLeft, probabilityToGoRight, overrideTile, fillStep);            
            //Debug.Log("amountOfBlocksAdded: " + amountOfBlocksAdded);
        } while (amountOfBlocksAdded < .5f * amountOfBlocksToAdd);
    }

    public void GenerateMapFromParameters(int width, int height, string seed, int randomFillPercent, 
                                            int numberOfSmoothIterations = 5, int numberOfSmoothIterationsAfterFirstSmooth = 5, 
                                            int referenceToMapSmoothing = 4, int smoothMode = 0, int smoothModeAfterFirstSmooth = 0)
    {
        this.width = width;
        this.height = height;
        this.seed = seed;
        this.randomFillPercent = randomFillPercent;
        this.numberOfSmoothIterations = numberOfSmoothIterations;
        this.numberOfSmoothIterationsAfterFirstSmooth = numberOfSmoothIterationsAfterFirstSmooth;
        this.referenceToMapSmoothing = referenceToMapSmoothing;
        this.smoothMode = smoothMode;
        this.smoothModeAfterFirstSmooth = smoothModeAfterFirstSmooth;

        GenerateMap();
    }

    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
            seed = System.DateTime.Now.Second.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }

    }

    void SmoothMap(int numberOfSmoothIterations, int smoothMode)
    {
        for (int smooth = 0; smooth < numberOfSmoothIterations; smooth++)
        {
            int[,] mapTemp = new int[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int neighboorWallTiles = GetSurroundingWallCount(x, y);

                    if (neighboorWallTiles > referenceToMapSmoothing)
                    {
                        if (smoothMode == 0)
                        {
                            map[x, y] = 1;
                        }
                        else if (smoothMode == 1)
                        {
                            mapTemp[x, y] = 1;
                        }


                    }
                    else if (neighboorWallTiles < referenceToMapSmoothing)
                    {
                        if (smoothMode == 0)
                        {
                            map[x, y] = 0;
                        }
                        else if (smoothMode == 1)
                        {
                            mapTemp[x, y] = 0;
                        }
                    }
                }
            }

            if (smoothMode == 1)
            {
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        map[x, y] = mapTemp[x, y];
                    }
                }
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    //private void OnDrawGizmos()
    //{
    //    if (map != null)
    //    {
    //        for (int x = 0; x < width; x++)
    //        {
    //            for (int y = 0; y < height; y++)
    //            {
    //                Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;
    //                Vector3 position = new Vector3(-width / 2f + x + 0.5f, 0f, -height / 2f + y + 0.5f);
    //                Gizmos.DrawCube(position, Vector3.one);
    //            }
    //        }
    //    }
    //}

    List<Tuple<int, int>> positionsList = new List<Tuple<int, int>>();
    int amountToFill = 0;

    int FloodFillAtPosition(int[,] map, /*int posX, int posY, */int targetColorToChange, int newColor, float probabilityToGoUp, float probabilityToGoDown, float probabilityToGoLeft, float probabilityToGoRight, bool overrideTile = false, int fillStep = 1)
    {
        if (amountToFill <= 0)
        {
            amountToFill = 0;
            return 0;
        }

        if (positionsList.Count == 0)
        {
            return 0;
        }

        Tuple<int, int> currentTile = positionsList[0];

        //Debug.Log("(" + currentTile.Item1 + ", " + currentTile.Item2 + ")"); ;
        if (map[currentTile.Item1, currentTile.Item2] != targetColorToChange && map[currentTile.Item1, currentTile.Item2] != 0)
        {
            if (overrideTile)
            {
                map[currentTile.Item1, currentTile.Item2] = newColor;
                positionsList.RemoveAt(0);
            }
            else
            {
                positionsList.RemoveAt(0);
                return 0;
            }
        }
        else if (map[currentTile.Item1, currentTile.Item2] == 0)
        {
            positionsList.RemoveAt(0);
            return 0;
        }
        else
        {
            map[currentTile.Item1, currentTile.Item2] = newColor;
            positionsList.RemoveAt(0);
        }

        amountToFill--;

        //Debug.Log("Adding from (")
        int amountOfNewTilesInTheList = 0;

        int referenceToAmountOfBlocks = (probabilityToGoUp > 0 ? 1 : 0) + (probabilityToGoDown > 0 ? 1 : 0) + (probabilityToGoRight > 0 ? 1 : 0) + (probabilityToGoLeft > 0 ? 1 : 0);

        while (amountOfNewTilesInTheList < referenceToAmountOfBlocks - 1)
        {
            if ((targetColorToChange < 100 && currentTile.Item1 - fillStep >= 0) || (targetColorToChange >= 100 && currentTile.Item1 - fillStep > 2))
            {
                if (UnityEngine.Random.Range(0f, 1f) < probabilityToGoLeft)
                {
                    int posX = currentTile.Item1 - fillStep;
                    int posY = currentTile.Item2;
                    positionsList.Add(Tuple.Create(posX, posY));
                    amountOfNewTilesInTheList++;
                }
            }
            if ((targetColorToChange < 100 && currentTile.Item2 - fillStep >= 0) || (targetColorToChange >= 100 && currentTile.Item2 - fillStep > 2))
            {
                if (UnityEngine.Random.Range(0f, 1f) < probabilityToGoDown)
                {
                    int posX = currentTile.Item1;
                    int posY = currentTile.Item2 - fillStep;
                    positionsList.Add(Tuple.Create(posX, posY));
                    amountOfNewTilesInTheList++;
                }
            }
            if ((targetColorToChange < 100 && currentTile.Item1 + fillStep <= map.GetLength(0) - 1) || (targetColorToChange >= 100 && currentTile.Item1 + fillStep < map.GetLength(0) - 3))
            {
                if (UnityEngine.Random.Range(0f, 1f) < probabilityToGoRight)
                {
                    int posX = currentTile.Item1 + fillStep;
                    int posY = currentTile.Item2;
                    positionsList.Add(Tuple.Create(posX, posY));
                    amountOfNewTilesInTheList++;
                }
            }
            if ((targetColorToChange < 100 && currentTile.Item2 + fillStep <= map.GetLength(1) - 1) || (targetColorToChange >= 100 && currentTile.Item2 + fillStep < map.GetLength(1) - 3))
            {
                if (UnityEngine.Random.Range(0f, 1f) < probabilityToGoUp)
                {
                    int posX = currentTile.Item1;
                    int posY = currentTile.Item2 + fillStep;
                    positionsList.Add(Tuple.Create(posX, posY));
                    amountOfNewTilesInTheList++;
                }
            }

        }

        return 1;
    }

    int FloodFill(int[,]map, int startPosX, int startPosY, int targetColorToChange, int newColor, int amount, float probabilityToGoUp, float probabilityToGoDown, float probabilityToGoLeft, float probabilityToGoRight, bool overrideTile = false, int fillStep = 1)
    {
        //Debug.Log("FloodFill");
        amountToFill = amount;
        positionsList = new List<Tuple<int, int>>();
        positionsList.Add(Tuple.Create(startPosX, startPosY));

        int amountOfBlocks = 0;

        while (positionsList.Count != 0 && amountToFill > 0)
        {
            amountOfBlocks += FloodFillAtPosition(map, /*positionsList[0].Item1, positionsList[0].Item1, */targetColorToChange, newColor, probabilityToGoUp, probabilityToGoDown, probabilityToGoLeft, probabilityToGoRight, overrideTile, fillStep);
        }

        return amountOfBlocks;
    }
}

[System.Serializable]
public class BlockSettings
{
    public string blockRegionName;
    public int amountOfBlocksToAdd = 500;
    public float probabilityToGoUp = .5f;
    public float probabilityToGoDown = .5f;
    public float probabilityToGoLeft = .5f;
    public float probabilityToGoRight = .5f;
    public bool overrideOtherBlocks = false;
}

[System.Serializable]
public class ResourcesSettings : BlockSettings
{
    public int amountOfOreVeins = 20;
}