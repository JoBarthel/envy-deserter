﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Character;
using Platformer.Mechanics;
using UnityEngine;

public class RotateTowardsMouse : MonoBehaviour
{
    private const float BASE_ROTATION = 45.0f;

    private Player player;

    private float xPosition;

    private void Start()
    {
        xPosition = transform.localPosition.x;

        player = GetComponentInParent<Player>();
        if (player == null)
            Debug.LogWarning(name + " has a RotateTowardsMouse component but no Player in its parent.");
    }

    private void Update()
    {
        if (player.Network != null
            && player.Network.networkObject != null
            && !player.Network.networkObject.IsOwner)
        {
            Destroy(this);
            return;
        }

        transform.up = Quaternion.Euler(0.0f, 0.0f, BASE_ROTATION)
            * ( player.Controller.GetCursorPositionInWorldSpace() - (Vector2) transform.position).normalized;

        if (player.Renderer.flipX) transform.localPosition = transform.localPosition.y * Vector2.up - xPosition * Vector2.right;
        else transform.localPosition = transform.localPosition.y * Vector2.up + xPosition * Vector2.right;
    }
}
