﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    public float speed;
    [SerializeField] protected ContactFilter2D contactFilter;

    [Space]
    [SerializeField] protected int damage;
    [SerializeField] protected Vector2 direction;
    [SerializeField] protected uint playerOwner;

    public uint Owner => playerOwner;

    protected int id;

    [Space]
    private new Rigidbody2D rigidbody;
    private new Collider2D collider;
    protected String owner;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];

    public delegate void OnHit(HitInfo hitInfo);
    public event OnHit OnHitEvent;

    public GameObject particles;
    [SerializeField] float timeForParticlesDisapear = 2f;

    protected void Awake()
    {
        collider = GetComponent<Collider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    protected void Update()
    {
        Vector2 move = direction * speed * Time.deltaTime;
        DetectCollision(move);
        transform.position += (Vector3) move;
    }

    public void Init(Vector2 direction, uint playerOwner)
    {
        this.direction = direction;
        this.playerOwner = playerOwner;
    }

    private void DetectCollision(Vector2 move)
    {
        var distance = move.magnitude;
        var count = rigidbody.Cast(move, contactFilter, hitBuffer, distance + collider.bounds.extents.x);

        if (count == 0) return;

        for (var i = 0; i < count; i++) ProcessTargetHit(hitBuffer[i]);
    }

    private void ProcessTargetHit(RaycastHit2D raycastHit2D)
    {
        var player = raycastHit2D.transform.GetComponent<Player>();
        if (player != null && playerOwner != player.Network.ownerID)
        {
            if (playerOwner == GameManagerNetwork.gameManagerNetwork.localPlayerId)
                Hit(player);

            Pooling.Instance.PoolProjectile(this, particles);

            return;
        }

        var building = raycastHit2D.transform.GetComponent<Building>()
                       ?? raycastHit2D.transform.GetComponentInParent<Building>();
        if (building != null && playerOwner != building.ownerId)
        {
            if (playerOwner == GameManagerNetwork.gameManagerNetwork.localPlayerId)
                Hit(building);

            Pooling.Instance.PoolProjectile(this, particles);

            return;
        }

        var collidedTag = raycastHit2D.collider.tag;
        if (String.Equals(collidedTag, "Terrain"))
        {
            if (playerOwner == GameManagerNetwork.gameManagerNetwork.localPlayerId)
                Hit();

            Pooling.Instance.PoolProjectile(this, particles);

            return;
        }
    }

    protected abstract void Hit(Player player);
    protected abstract void Hit(Building building);
    protected abstract void Hit(); // Terrain
}
