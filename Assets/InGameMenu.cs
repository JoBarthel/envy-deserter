﻿using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour
{
    [SerializeField] GameObject quitGamePanel;

    private void Update()
    {
        if(!quitGamePanel.activeSelf && Input.GetKeyDown(KeyCode.Escape))
        {
            quitGamePanel.SetActive(true);
        }else if (quitGamePanel.activeSelf && Input.GetKeyDown(KeyCode.Escape))
        {
            quitGamePanel.SetActive(false);
        }
    }

    public void DisconnectFromServerAndGoToTitleScreen(int titleSceneIndex)
    {
        NetworkManager.Instance.Disconnect();
        SceneManager.LoadScene(titleSceneIndex);
    }
}
