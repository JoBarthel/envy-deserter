﻿using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UiInputDeviceController : MonoBehaviour
{
    [SerializeField] private Image[] keyboardImages;
    [SerializeField] private Image[] gamepadImages;
    // Update is called once per frame
    void Update()
    {
        if (Gamepad.current != null)
        {
            DisplayGamepadUi(true);
            DisplayKeyboardUi(false);
        }
        else
        {
            DisplayGamepadUi(false);
            DisplayKeyboardUi(true);
        }
    }

    private void DisplayKeyboardUi(bool status)
    {
        foreach (Image image in keyboardImages)
        {
            image.gameObject.SetActive(status);
        }
    }

    private void DisplayGamepadUi(bool status)
    {
        foreach (Image image in gamepadImages)
        {
            image.gameObject.SetActive(status);
        }
    }

}
