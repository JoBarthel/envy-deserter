﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildingData", menuName = "Data/BuildingData", order = 1)]
public class BuildingData : ScriptableObject
{
    public List<Building> Buildings;
}
