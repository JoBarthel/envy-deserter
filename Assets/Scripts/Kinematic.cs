﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Kinematic : MonoBehaviour
{
    protected const float shellRadius = 0.01f;
    
    [SerializeField] protected float minGroundNormalY = .65f;
    
    protected ContactFilter2D contactFilter;

    protected new Rigidbody2D rigidbody;
    public Rigidbody2D Rigidbody => rigidbody;

    protected new Collider2D collider;

    protected Vector2 speed;
    
    public bool IsOnGround { get; protected set; }
    public Vector2 Speed => speed;

    private GrapplingHook grapplingHook;
    public GrapplingHook GrapplingHook
        => grapplingHook ?? (grapplingHook = GetComponentInChildren<GrapplingHook>());

    protected void Awake()
    {
        collider = GetComponent<Collider2D>();
        if (collider == null)
        {
            Debug.Log(name + " has a Kinematic component but no Collider2D ; destroying the component.");
            Destroy(this);
            return;
        }

        rigidbody = GetComponent<Rigidbody2D>();
        if (rigidbody == null)
        {
            Debug.Log(name + " has a Kinematic component but no Rigidbody2D ; destroying the component.");
            Destroy(this);
            return;
        }
        rigidbody.isKinematic = true;

        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
    }
    
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected void TryMove()
    {
        GrapplingHookConstraint();

        var x = Time.deltaTime * speed.x;
        var y = Time.deltaTime * speed.y;

        var collisionCount = rigidbody.Cast(x * Vector2.right, contactFilter, hitBuffer, Mathf.Abs(x) + shellRadius);

        for (var i = 0; i < collisionCount; i++)
        {
            var posToHit = hitBuffer[i].centroid - rigidbody.position;
            x = Mathf.Abs(x) < Mathf.Abs(posToHit.x) - shellRadius
                ? x : (posToHit - shellRadius * posToHit.normalized).x;
            speed.x = 0.0f;
        }

        rigidbody.position += x * Vector2.right;
        collisionCount = rigidbody.Cast(y * Vector2.up, contactFilter, hitBuffer, Mathf.Abs(y) + shellRadius);
        IsOnGround = false;

        for (var i = 0; i < collisionCount; i++)
        {
            var posToHit = hitBuffer[i].centroid - rigidbody.position;
            y = Mathf.Abs(y) < Mathf.Abs(posToHit.y) - shellRadius
                ? y : (posToHit - shellRadius * posToHit.normalized).y;
            speed.y = 0.0f;

            // Is this surface flat enough to land on?
            if (hitBuffer[i].normal.y > minGroundNormalY) IsOnGround = true;
        }

        rigidbody.position += y * Vector2.up;
    }

    public void MoveAndCollide(Vector2 move)
    {
        var x = move.x;
        var y = move.y;
        var collisionCount = rigidbody.Cast(x * Vector2.right, contactFilter, hitBuffer, Mathf.Abs(x) + shellRadius);

        for (var i = 0; i < collisionCount; i++)
        {
            var posToHit = hitBuffer[i].centroid - rigidbody.position;
            x = Mathf.Abs(x) < Mathf.Abs(posToHit.x) - shellRadius
                ? x : (posToHit - shellRadius * posToHit.normalized).x;
            speed.x = 0.0f;
        }

        rigidbody.position += x * Vector2.right;
        collisionCount = rigidbody.Cast(y * Vector2.up, contactFilter, hitBuffer, Mathf.Abs(y) + shellRadius);
        IsOnGround = false;

        for (var i = 0; i < collisionCount; i++)
        {
            var posToHit = hitBuffer[i].centroid - rigidbody.position;
            y = Mathf.Abs(y) < Mathf.Abs(posToHit.y) - shellRadius
                ? y : (posToHit - shellRadius * posToHit.normalized).y;
            speed.y = 0.0f;

            // Is this surface flat enough to land on?
            if (hitBuffer[i].normal.y > minGroundNormalY) IsOnGround = true;
        }

        rigidbody.position += y * Vector2.up;
    }

    private void GrapplingHookConstraint()
    {
        if (GrapplingHook == null
            || !GrapplingHook.Hook.IsHooked
            || IsOnGround && GrapplingHook.RopeLength <= GrapplingHook.WantedLength
            || !GrapplingHook.EndOfRope
            || GrapplingHook.PosToHinge.magnitude < 0.1f)
            return;

        if (GrapplingHook.PosToHinge.y < 0.0f
            && GrapplingHook.RopeLength <= GrapplingHook.WantedLength)
            return;

        var wantedMagnitude = GrapplingHook.WantedLength
            - (GrapplingHook.RopeLength - GrapplingHook.PosToHinge.magnitude);

        if (wantedMagnitude <= 0.1f) return;

        var hingeToPos = -wantedMagnitude * GrapplingHook.PosToHinge.normalized;
        var tangeantSpeed = (Vector2) Vector3.Project(speed, Vector2.Perpendicular(hingeToPos).normalized);
        var cross = Vector3.Cross(tangeantSpeed, -hingeToPos);

        var dtheta = tangeantSpeed.magnitude * Time.deltaTime / hingeToPos.magnitude;
        var hingeToNewPos = (Vector2) (Quaternion.Euler(0.0f, 0.0f, (cross.z < 0 ? -dtheta : dtheta) * 180 / Mathf.PI) * hingeToPos);
        
        speed = (GrapplingHook.PosToHinge + hingeToNewPos) / Time.deltaTime;
    }
}