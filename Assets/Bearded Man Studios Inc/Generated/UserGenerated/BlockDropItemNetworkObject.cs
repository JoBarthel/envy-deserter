using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0.15,0,0,0]")]
	public partial class BlockDropItemNetworkObject : NetworkObject
	{
		public const int IDENTITY = 1;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _position;
		public event FieldEvent<Vector3> positionChanged;
		public InterpolateVector3 positionInterpolation = new InterpolateVector3() { LerpT = 0.15f, Enabled = true };
		public Vector3 position
		{
			get { return _position; }
			set
			{
				// Don't do anything if the value is the same
				if (_position == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_position = value;
				hasDirtyFields = true;
			}
		}

		public void SetpositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_position(ulong timestep)
		{
			if (positionChanged != null) positionChanged(_position, timestep);
			if (fieldAltered != null) fieldAltered("position", _position, timestep);
		}
		[ForgeGeneratedField]
		private bool _followTarget;
		public event FieldEvent<bool> followTargetChanged;
		public Interpolated<bool> followTargetInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool followTarget
		{
			get { return _followTarget; }
			set
			{
				// Don't do anything if the value is the same
				if (_followTarget == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_followTarget = value;
				hasDirtyFields = true;
			}
		}

		public void SetfollowTargetDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_followTarget(ulong timestep)
		{
			if (followTargetChanged != null) followTargetChanged(_followTarget, timestep);
			if (fieldAltered != null) fieldAltered("followTarget", _followTarget, timestep);
		}
		[ForgeGeneratedField]
		private uint _targetPlayerId;
		public event FieldEvent<uint> targetPlayerIdChanged;
		public Interpolated<uint> targetPlayerIdInterpolation = new Interpolated<uint>() { LerpT = 0f, Enabled = false };
		public uint targetPlayerId
		{
			get { return _targetPlayerId; }
			set
			{
				// Don't do anything if the value is the same
				if (_targetPlayerId == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_targetPlayerId = value;
				hasDirtyFields = true;
			}
		}

		public void SettargetPlayerIdDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_targetPlayerId(ulong timestep)
		{
			if (targetPlayerIdChanged != null) targetPlayerIdChanged(_targetPlayerId, timestep);
			if (fieldAltered != null) fieldAltered("targetPlayerId", _targetPlayerId, timestep);
		}
		[ForgeGeneratedField]
		private bool _arrivedToTarget;
		public event FieldEvent<bool> arrivedToTargetChanged;
		public Interpolated<bool> arrivedToTargetInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool arrivedToTarget
		{
			get { return _arrivedToTarget; }
			set
			{
				// Don't do anything if the value is the same
				if (_arrivedToTarget == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x8;
				_arrivedToTarget = value;
				hasDirtyFields = true;
			}
		}

		public void SetarrivedToTargetDirty()
		{
			_dirtyFields[0] |= 0x8;
			hasDirtyFields = true;
		}

		private void RunChange_arrivedToTarget(ulong timestep)
		{
			if (arrivedToTargetChanged != null) arrivedToTargetChanged(_arrivedToTarget, timestep);
			if (fieldAltered != null) fieldAltered("arrivedToTarget", _arrivedToTarget, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			positionInterpolation.current = positionInterpolation.target;
			followTargetInterpolation.current = followTargetInterpolation.target;
			targetPlayerIdInterpolation.current = targetPlayerIdInterpolation.target;
			arrivedToTargetInterpolation.current = arrivedToTargetInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _position);
			UnityObjectMapper.Instance.MapBytes(data, _followTarget);
			UnityObjectMapper.Instance.MapBytes(data, _targetPlayerId);
			UnityObjectMapper.Instance.MapBytes(data, _arrivedToTarget);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_position = UnityObjectMapper.Instance.Map<Vector3>(payload);
			positionInterpolation.current = _position;
			positionInterpolation.target = _position;
			RunChange_position(timestep);
			_followTarget = UnityObjectMapper.Instance.Map<bool>(payload);
			followTargetInterpolation.current = _followTarget;
			followTargetInterpolation.target = _followTarget;
			RunChange_followTarget(timestep);
			_targetPlayerId = UnityObjectMapper.Instance.Map<uint>(payload);
			targetPlayerIdInterpolation.current = _targetPlayerId;
			targetPlayerIdInterpolation.target = _targetPlayerId;
			RunChange_targetPlayerId(timestep);
			_arrivedToTarget = UnityObjectMapper.Instance.Map<bool>(payload);
			arrivedToTargetInterpolation.current = _arrivedToTarget;
			arrivedToTargetInterpolation.target = _arrivedToTarget;
			RunChange_arrivedToTarget(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _position);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _followTarget);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _targetPlayerId);
			if ((0x8 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _arrivedToTarget);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (positionInterpolation.Enabled)
				{
					positionInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					positionInterpolation.Timestep = timestep;
				}
				else
				{
					_position = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_position(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (followTargetInterpolation.Enabled)
				{
					followTargetInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					followTargetInterpolation.Timestep = timestep;
				}
				else
				{
					_followTarget = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_followTarget(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (targetPlayerIdInterpolation.Enabled)
				{
					targetPlayerIdInterpolation.target = UnityObjectMapper.Instance.Map<uint>(data);
					targetPlayerIdInterpolation.Timestep = timestep;
				}
				else
				{
					_targetPlayerId = UnityObjectMapper.Instance.Map<uint>(data);
					RunChange_targetPlayerId(timestep);
				}
			}
			if ((0x8 & readDirtyFlags[0]) != 0)
			{
				if (arrivedToTargetInterpolation.Enabled)
				{
					arrivedToTargetInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					arrivedToTargetInterpolation.Timestep = timestep;
				}
				else
				{
					_arrivedToTarget = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_arrivedToTarget(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (positionInterpolation.Enabled && !positionInterpolation.current.UnityNear(positionInterpolation.target, 0.0015f))
			{
				_position = (Vector3)positionInterpolation.Interpolate();
				//RunChange_position(positionInterpolation.Timestep);
			}
			if (followTargetInterpolation.Enabled && !followTargetInterpolation.current.UnityNear(followTargetInterpolation.target, 0.0015f))
			{
				_followTarget = (bool)followTargetInterpolation.Interpolate();
				//RunChange_followTarget(followTargetInterpolation.Timestep);
			}
			if (targetPlayerIdInterpolation.Enabled && !targetPlayerIdInterpolation.current.UnityNear(targetPlayerIdInterpolation.target, 0.0015f))
			{
				_targetPlayerId = (uint)targetPlayerIdInterpolation.Interpolate();
				//RunChange_targetPlayerId(targetPlayerIdInterpolation.Timestep);
			}
			if (arrivedToTargetInterpolation.Enabled && !arrivedToTargetInterpolation.current.UnityNear(arrivedToTargetInterpolation.target, 0.0015f))
			{
				_arrivedToTarget = (bool)arrivedToTargetInterpolation.Interpolate();
				//RunChange_arrivedToTarget(arrivedToTargetInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public BlockDropItemNetworkObject() : base() { Initialize(); }
		public BlockDropItemNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public BlockDropItemNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
