using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0]")]
	public partial class GrapplingHookNetworkObject : NetworkObject
	{
		public const int IDENTITY = 11;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector2 _hookPosition;
		public event FieldEvent<Vector2> hookPositionChanged;
		public InterpolateVector2 hookPositionInterpolation = new InterpolateVector2() { LerpT = 0f, Enabled = false };
		public Vector2 hookPosition
		{
			get { return _hookPosition; }
			set
			{
				// Don't do anything if the value is the same
				if (_hookPosition == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_hookPosition = value;
				hasDirtyFields = true;
			}
		}

		public void SethookPositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_hookPosition(ulong timestep)
		{
			if (hookPositionChanged != null) hookPositionChanged(_hookPosition, timestep);
			if (fieldAltered != null) fieldAltered("hookPosition", _hookPosition, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			hookPositionInterpolation.current = hookPositionInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _hookPosition);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_hookPosition = UnityObjectMapper.Instance.Map<Vector2>(payload);
			hookPositionInterpolation.current = _hookPosition;
			hookPositionInterpolation.target = _hookPosition;
			RunChange_hookPosition(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _hookPosition);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (hookPositionInterpolation.Enabled)
				{
					hookPositionInterpolation.target = UnityObjectMapper.Instance.Map<Vector2>(data);
					hookPositionInterpolation.Timestep = timestep;
				}
				else
				{
					_hookPosition = UnityObjectMapper.Instance.Map<Vector2>(data);
					RunChange_hookPosition(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (hookPositionInterpolation.Enabled && !hookPositionInterpolation.current.UnityNear(hookPositionInterpolation.target, 0.0015f))
			{
				_hookPosition = (Vector2)hookPositionInterpolation.Interpolate();
				//RunChange_hookPosition(hookPositionInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public GrapplingHookNetworkObject() : base() { Initialize(); }
		public GrapplingHookNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public GrapplingHookNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
