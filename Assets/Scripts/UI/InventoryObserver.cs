﻿using System;
using EnvyDeserter.UI;
using UnityEngine;

public class InventoryObserver : MonoBehaviour, IObserver<InventoryState>
{
    [SerializeField] private ItemStockDisplay crystalDisplay;
    [SerializeField] private ItemStockDisplay blockDisplay;
    [SerializeField] private UiSoundPlayer uiSoundPlayer;
    public void Subscribe(IObservable<InventoryState> provider)
    {
        if(provider != null)
            _unsubscriber = provider.Subscribe(this);
    }
    
    private IDisposable _unsubscriber;
    public void OnCompleted()
    {
        _unsubscriber?.Dispose();
    }

    public void OnError(Exception error)
    {
        Debug.LogError(error.Message);
    }

    public void OnNext(InventoryState value)
    {
        crystalDisplay.SetText(value.numberOfCrystals.ToString());
        blockDisplay.SetText(value.numberOfBlocks.ToString());
    }
    
    public void PlayFeedbackOnFailedBuild(IFailBroadcaster broadcaster)
    {
        broadcaster.AddOnFail(FailToBuild);
    }

    public void PlayFeedbackOnFailedPlace(IFailBroadcaster broadcaster)
    {
        broadcaster.AddOnFail(FailToPlace);
    }

    private void FailToPlace()
    {    
        blockDisplay.BlinkColor(Color.red);
        uiSoundPlayer.PlayErrorSound();

    }

    private void FailToBuild()
    {
        crystalDisplay.BlinkColor(Color.red);
        uiSoundPlayer.PlayErrorSound();

    }
    
}
