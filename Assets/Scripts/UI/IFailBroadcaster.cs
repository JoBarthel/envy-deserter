﻿using UnityEngine.Events;

namespace EnvyDeserter.UI
{
    public interface IFailBroadcaster
    {
        void AddOnFail(UnityAction callback);
    }
}