﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class ProjectileWeaponNetwork : ProjectileWeaponBehavior
{
    private ProjectileWeapon projectileWeapon;
    private PlayerNetwork playerNetwork;
    public PlayerNetwork PlayerNetwork => playerNetwork ?? (playerNetwork = projectileWeapon.PlayerNetwork);

    private void Awake()
    {
        projectileWeapon = GetComponent<ProjectileWeapon>();
        if (projectileWeapon == null)
            Debug.LogWarning(name + " has a ProjectileWeaponNetwork component but no ProjectileWeapon.");
    }

    public override void Shoot(RpcArgs args)
    {
        var owner = args.GetNext<uint>();
        if (owner != PlayerNetwork.ownerID) return;

        MainThreadManager.Run(() => projectileWeapon.InitProjectile(owner, args.GetNext<Vector2>(), args.GetNext<Vector2>()));
    }
}