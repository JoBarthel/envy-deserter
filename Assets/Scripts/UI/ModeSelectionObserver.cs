﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelectionObserver : MonoBehaviour, IObserver<State>
{
    [SerializeField] private UiModeSelector diggingSelector;
    [SerializeField] private UiModeSelector firingSelector;
    [SerializeField] private UiModeSelector constructionSelector;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color unselectedColor;

    public void Subscribe(IObservable<State> provider)
    {
        if(provider != null)
            _unsubscriber = provider.Subscribe(this);
    }

    private IDisposable _unsubscriber;
    public void OnCompleted()
    {
        _unsubscriber?.Dispose();
    }

    public void OnError(Exception error)
    {
        Debug.LogError(error.Message);
    }

    public void OnNext(State value)
    {
        switch (value)
        {
            case State.Digging: HighlightDigging();
                break;
            case State.Firing: HighlightFiring();
                break;
            case State.Construction: HighlightConstruction();
                break;
        }
    }
    
    private void HighlightDigging()
    {
        diggingSelector.SetColor(selectedColor);
        firingSelector.SetColor(unselectedColor);
        constructionSelector.SetColor(unselectedColor);
    }
    private void HighlightFiring()
    {
        firingSelector.SetColor(selectedColor);
        diggingSelector.SetColor(unselectedColor);
        constructionSelector.SetColor(unselectedColor);
    }
    private void HighlightConstruction()
    {
        constructionSelector.SetColor(selectedColor);
        diggingSelector.SetColor(unselectedColor);
        firingSelector.SetColor(unselectedColor);
        
    }

}
