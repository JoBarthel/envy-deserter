﻿ using System;
 using BeardedManStudios.Forge.Networking;
 using BeardedManStudios.Forge.Networking.Generated;
 using BeardedManStudios.Forge.Networking.Unity;
 using Platformer.Mechanics;
 using UnityEngine;

public class ProjectileWeapon : Weapon
{
    [SerializeField] protected Projectile projectilePrefab;

    private ProjectileWeaponNetwork network;

    private new void Awake()
    {
        base.Awake();

        network = GetComponent<ProjectileWeaponNetwork>();
    }

    public override void Shoot(Vector2 direction)
    {
        if (network.networkObject == null) InitProjectile(0, transform.position, direction);
        else
        {
            if (network.networkObject.IsOwner && isActiveAndEnabled && PlayerNetwork != null)
                PlayerNetwork.networkObject.SendRpc(PlayerNetworkBehavior.RPC_SHOOT, Receivers.All,
                    direction, (Vector2) transform.position, PlayerNetwork.ownerID);
        }
    }

    public void InitProjectile(uint id, Vector2 position, Vector2 direction)
    {
        Projectile projectile = Pooling.Instance.GetProjectile(projectilePrefab);
        projectile.transform.position = position;
        projectile.Init(direction, id);
    }
}