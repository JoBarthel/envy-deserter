﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchWeaponsFastPieMenu : MonoBehaviour
{
    [SerializeField] Image[] pieMenuButtons = new Image[4];
    [SerializeField] [Range(0, 1)] float alphaButtonActivated = 204f/255f;
    [SerializeField] [Range(0, 1)] float alphaButtonDeactivated = 102f/255f;


    int currentSelectedIndex = 0;

    Color buttonColor;
    public void SelectButtonPieMenu(int newIndex)
    {
        buttonColor = pieMenuButtons[currentSelectedIndex].color;
        pieMenuButtons[currentSelectedIndex].color = new Color(buttonColor.r, buttonColor.g, buttonColor.b, alphaButtonDeactivated);

        buttonColor = pieMenuButtons[newIndex].color;
        pieMenuButtons[newIndex].color = new Color(buttonColor.r, buttonColor.g, buttonColor.b, alphaButtonActivated);

        currentSelectedIndex = newIndex;
    }
}
