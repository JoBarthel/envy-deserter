﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking;

public class DetectPlayerTouchedBlockDropItem : MonoBehaviour
{
    DropItem dropItem;

    [HideInInspector] public bool localAlreadySentRPC = false;

    private void Start()
    {
        dropItem = GetComponentInParent<DropItem>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (dropItem == null || localAlreadySentRPC) return;
        if (dropItem.networkObject.followTarget) return;

        var inventory = collision.gameObject.GetComponent<Inventory>();
        if (inventory != null && inventory.NumberCarriedBlocksByType(dropItem.dropType) >= inventory.MaxCarriedBlocksByType(dropItem.dropType)) return;

        dropItem.targetPlayerNetwork = collision.gameObject.GetComponent<PlayerNetwork>();

        if (dropItem.targetPlayerNetwork == null
            || dropItem.targetPlayerNetwork.networkObject == null
            || !dropItem.targetPlayerNetwork.networkObject.IsOwner)
            return;

        localAlreadySentRPC = true;

        if (dropItem.networkHasStarted)
            dropItem.networkObject.SendRpc(DropItem.RPC_TRIGGER_DROP_ITEM,
                Receivers.Owner,
                dropItem.targetPlayerNetwork.networkObject.NetworkId,
                transform.position,
                (int) dropItem.dropType,
                true);
        else
            dropItem.networkStarted += dropItem.CallRpcAfterNetworkStart;
    }
}
