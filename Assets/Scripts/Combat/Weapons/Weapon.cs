﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Character;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public abstract class Weapon : MonoBehaviour
{
    protected InputMaster inputMaster;

    public Player Player;

    public Controller Controller => Player?.Controller;
    public PlayerNetwork PlayerNetwork => Player?.Network;

    protected bool networkStarted;

    private Image targetUi;

    [SerializeField] protected float shootCooldown = 0.1f;
    protected float shootTimer;

    protected bool isCarriedByPlayer;

    protected void Awake()
    {
        if (Player == null)
        {
            isCarriedByPlayer = false;
            return;
        }

        isCarriedByPlayer = true;

        // The following should be set whenever a weapon is picked up / unlocked

        if (PlayerNetwork != null)
            PlayerNetwork.networkStarted += InitAfterNetworkStarts;

        inputMaster = Controller.GetInputMaster();

        targetUi = GameObject.Find("GunTarget").GetComponent<Image>();
    }

    public void NetworkStart()
    {
        if (!isCarriedByPlayer)
            return;

        if (PlayerNetwork != null
            && PlayerNetwork.networkObject != null
            && !PlayerNetwork.networkObject.IsOwner)
            return;

        inputMaster = Controller.GetInputMaster();
        Debug.Assert(inputMaster != null, "No input master found for Weapon");

        inputMaster.BasicControls.Fire.performed += delegate
        {
            if (!isActiveAndEnabled || shootTimer > 0.0f) return;

            shootTimer = shootCooldown;

            var target = Controller.GetCursorPositionInWorldSpace();
            Shoot((target - (Vector2)transform.position).normalized);
        };
    }

    protected void Update()
    {
        if (shootTimer >= 0.0f)
            shootTimer -= Time.deltaTime;
    }

    private void OnEnable() => targetUi?.gameObject.SetActive(true);

    private void OnDisable() => targetUi?.gameObject.SetActive(false);

    protected void InitAfterNetworkStarts(NetworkBehavior behavior)
    {
        networkStarted = true;
    }

    public abstract void Shoot(Vector2 direction);
}