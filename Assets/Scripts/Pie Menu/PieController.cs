﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieController : MonoBehaviour, IObserver<State>
{
    private enum ControllerType
    {
        None = 0,
        XBox,
        PS4
    }

    [SerializeField] private Camera uiCamera;

    private PieMenu[] pieMenus;
    private RectTransform rectTransform;

    private State _currentInteractionMode;
    public Vector2 CursorPosition { get; private set; }
    public Vector2 CanvasSize => rectTransform.sizeDelta;
    public Vector2 ScreenWorldSize { get; private set; }
    public Vector2 ScreenWorldCenter { get; private set; }
    public bool IsUsingController { get; private set; }
    private ControllerType controllerType;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        pieMenus = GetComponentsInChildren<PieMenu>();

        UpdateProperties();
    }

    private void Update()
    {
        for (var i = 0; i < pieMenus.Length; i++)
        {
            var pieMenu = pieMenus[i];
            if (Input.GetKeyDown(pieMenu.ActivationKey)) pieMenu.Show(_currentInteractionMode);
            if (Input.GetKeyUp(pieMenu.ActivationKey)) pieMenu.SelectAndHide();
        }

        UpdateProperties();
    }

    private void UpdateProperties()
    {
        var joystickNames = Input.GetJoystickNames();
        IsUsingController = joystickNames.Length > 0 && !string.IsNullOrEmpty(joystickNames[0])
                            || joystickNames.Length > 1 && !string.IsNullOrEmpty(joystickNames[1]);
        controllerType = !IsUsingController ? ControllerType.None
            : joystickNames[0] == "Wireless Controller" ? ControllerType.PS4
            : ControllerType.XBox;

        CursorPosition
            = controllerType == ControllerType.PS4 ? Input.GetAxis("PS4TargetingX") * Vector2.right +
                                                     Input.GetAxis("PS4TargetingY") * Vector2.up
            : controllerType == ControllerType.XBox ? Input.GetAxis("XBoxTargetingX") * Vector2.right +
                                                      Input.GetAxis("XBoxTargetingY") * Vector2.up
            : (Vector2) Input.mousePosition;

        ScreenWorldSize = rectTransform.localScale * rectTransform.sizeDelta;
        ScreenWorldCenter = uiCamera.transform.position;
    }

    public void Subscribe(IObservable<State> provider)
    {
            if(provider != null)
                _unsubscriber = provider.Subscribe(this);
    }

    private IDisposable _unsubscriber;
    public void OnCompleted()
    {
        _unsubscriber?.Dispose();
    }

    public void OnError(Exception error)
    {
        Debug.LogError(error.Message);
    }

    public void OnNext(State value)
    {
        _currentInteractionMode = value;
    }
}