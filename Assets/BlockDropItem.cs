﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class BlockDropItem : BlockDropItemBehavior
{
    [HideInInspector] public PlayerNetwork targetPlayerNetwork = null;

    [HideInInspector] public bool followTarget = false;

    public float speed = 1f;
    public float distanceToAddToInventory = 0.2f;

    [HideInInspector] public bool networkHasStarted = false;

    [SerializeField] private bool arrived = false;

    bool alreadyReceivedRPC = false;

    [SerializeField] bool destroyAfterSomeTime = true;
    [SerializeField] float timeToDestroy = 60f;

    float timeSinceStart = 0f;

    [SerializeField] DropType dropType = DropType.Block;

    protected override void NetworkStart()
    {
        base.NetworkStart();

        networkObject.positionInterpolation.Enabled = false;

        networkObject.followTarget = false;
        networkObject.arrivedToTarget = false;
        networkObject.position = transform.position;

        networkHasStarted = true;

        networkObject.UpdateInterval = 25;

        if (!networkObject.IsOwner)
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            transform.position = networkObject.position;
        }
    }

    Collider2D collider2d;
    Rigidbody2D rb;
    bool alreadyUpdatedInterpolation = false;
    private void Update()
    {
        if (networkObject.followTarget)
        {

            if (targetPlayerNetwork == null)
            {
                targetPlayerNetwork = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(networkObject.targetPlayerId);
            }

            if (targetPlayerNetwork != null)
            {
                if (collider2d == null)
                {
                    collider2d = GetComponent<Collider2D>();
                }

                if (collider2d != null)
                {
                    collider2d.enabled = false;
                }
                
                if(rb == null) rb = GetComponent<Rigidbody2D>();
                if(rb != null) Destroy(rb);
            }

            if (networkObject.IsOwner && targetPlayerNetwork != null)
            {
                transform.position += (targetPlayerNetwork.transform.position - transform.position).normalized * speed * Time.deltaTime;
                
                if ((targetPlayerNetwork.transform.position - transform.position).sqrMagnitude <= distanceToAddToInventory * distanceToAddToInventory)
                {
                    targetPlayerNetwork.networkObject.SendRpc(PlayerNetwork.RPC_ADD_BLOCKS_TO_INVENTORY, Receivers.All, 1, networkObject.targetPlayerId);

                    networkObject.arrivedToTarget = true;
                    arrived = true;
                    networkObject.Destroy();
                }
            }
            if (networkObject.arrivedToTarget)
            {
                Destroy(gameObject);
            }

        }

        if (!alreadyUpdatedInterpolation)
        {
            networkObject.positionInterpolation.Enabled = true;
            alreadyUpdatedInterpolation = true;

        }
        
        if (networkObject.IsOwner)
        {
            networkObject.position = transform.position;
            timeSinceStart += Time.deltaTime;
            if(destroyAfterSomeTime && timeSinceStart >= timeToDestroy)
            {
                networkObject.Destroy();
            }
        }
        else
        {
            transform.position = networkObject.position;
        }
    }

    public override void TriggerBlockDropItem(RpcArgs args)
    {
        if (networkObject.IsOwner && !alreadyReceivedRPC)
        {

            alreadyReceivedRPC = true;
            networkObject.followTarget = true;
            networkObject.targetPlayerId = args.GetNext<uint>();
            networkObject.position = args.GetNext<Vector3>();
        }
    }
    
    public void CallRpcAfterNetworkStart(NetworkBehavior networkBehavior)
    {
        networkObject.SendRpc(BlockDropItem.RPC_TRIGGER_BLOCK_DROP_ITEM, Receivers.Owner, targetPlayerNetwork.networkObject.NetworkId, transform.position);
    }
}




