﻿using System;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Lobby;
using BeardedManStudios.Forge.Networking.Unity;
using Platformer.Mechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetwork : PlayerNetworkBehavior
{
    NormalProjectileWeapon _normalProjectileWeapon;
    [SerializeField] public uint ownerID;

    [SerializeField] PlayerHealth playerHealth;

    private Player player;
    public Player Player => player ?? (player = GetComponent<Player>());

    private Animator animator;
    private new SpriteRenderer renderer;

    public ChestNetwork playerChestNetwork;

    private PickaxeAnimator pickaxeAnimator;

    [SerializeField] MinimapMarker minimapPlayerMarker;

    [SerializeField] private DoubleProjectileWeapon doubleProjectileWeapon;
    [SerializeField] private BuildWeapon buildWeapon;

    [Header("Explosion of blocks")]
    [SerializeField] GameObject[] explosions;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        if (animator == null)
            Debug.LogWarning(name + " has a PlayerNetwork component but no Animator.");

        renderer = GetComponent<SpriteRenderer>();
        if (renderer == null)
            Debug.LogWarning(name + " has a PlayerNetwork component but no SpriteRenderer.");

        pickaxeAnimator = GetComponentInChildren<PickaxeAnimator>();
        if (pickaxeAnimator == null)
            Debug.LogWarning(name + " has a PlayerNetwork component but no PickaxeAnimator in its children.");

        player = GetComponent<Player>();
        if (player == null)
            Debug.LogWarning(name + " has a PlayerNetwork component but no Player.");

    }

    private void Start()
    {
        if (networkObject == null)
        {
            Debug.LogWarning("PlayerNetwork's networkObject is null. Destroying the PlayerNetwork.");
            Destroy(this);
            return;
        }

        if (!networkObject.IsOwner) GetComponent<Rigidbody2D>().isKinematic = true;
        else networkObject.UpdateInterval = 25;
    }

    // NOTE: NetworkStart() is never called, is it normal ?
    bool networkHasStarted = false;

    protected override void NetworkStart()
    {
        base.NetworkStart();

        networkHasStarted = true;
        ownerID = networkObject.NetworkId;

        playerHealth.spawnPoint.transform.SetParent(null);

        GameManagerNetwork.gameManagerNetwork.RegisterNewPlayer(this);
        
        SpawnChest(transform.position);

        networkObject.hasChest = true;

        if (networkObject.IsOwner)
        {
            if (minimapPlayerMarker == null)
            {
                minimapPlayerMarker = GameObject.Find("PlayerMarker").GetComponent<MinimapMarker>();
            }
            minimapPlayerMarker.targetToFollow = transform;
        }

        player.Controller.NetworkStart();
        doubleProjectileWeapon.NetworkStart();
        buildWeapon.NetworkStart();
    }

    void SpawnChest(Vector3 position)
    {
        position = (Vector3) Vector3Int.RoundToInt(position);
        Building chestBuilding = null;
        int chestIndex = 3;
        Building buildingPrefab = Game.Instance.BuildingData.Buildings[chestIndex];
        if (NetworkManager.Instance == null) chestBuilding = Instantiate(buildingPrefab);
        else if (networkObject.IsOwner)
        {
            chestBuilding = NetworkManager.Instance.InstantiateBuilding(chestIndex) as Building;
        }

        if (chestBuilding == null) return;

        if (chestBuilding.networkObject == null)
        {
            chestBuilding.transform.position = position;
            chestBuilding.Team = GetComponent<Player>().Stats.Team;
            chestBuilding.Activated = true;
            chestBuilding.ownerId = ownerID;
        }
        else
        {
            chestBuilding.networkObject.position = position;
            chestBuilding.networkObject.team = (byte) GetComponent<Player>().Stats.Team;
            chestBuilding.networkObject.activated = true;

            chestBuilding.ownerId = ownerID;
            chestBuilding.networkStarted += chestBuilding.SetIdAfterNetworkStart;
        }

        playerChestNetwork = chestBuilding.GetComponent<ChestNetwork>();
        playerChestNetwork.playerNetworkOwner = this;
    }

    private void Update()
    {
        if (networkObject.IsOwner) networkObject.position = transform.position;
        else transform.position = networkObject.position;

        animator?.SetBool("grounded", networkObject.isGrounded);
        animator?.SetFloat("velocityX", networkObject.velocityX);
        if (renderer != null) renderer.flipX = networkObject.flipX;

        if (networkObject.IsOwner && (playerChestNetwork == null || playerChestNetwork.networkObject == null))
            networkObject.hasChest = false;
    }


    public override void TakeDamage(RpcArgs args)
    {
        MainThreadManager.Run(()
            =>
        {
            playerHealth.TakeDamage(args.GetNext<int>());
        });
    }

    public override void Shoot(RpcArgs args)
    {
        MainThreadManager.Run(()
            =>
        {
            var direction = args.GetNext<Vector2>();
            var position = args.GetNext<Vector2>();
            var id = args.GetNext<uint>();

            if (id != ownerID) return;

            Projectile projectile = Pooling.Instance.GetProjectile(Pooling.ProjectileType.NORMAL);
            projectile.transform.position = position;
            projectile.Init(direction, id);
        });
    }

    public override void AddBlocksToInventory(RpcArgs args)
    {
        var blocksAmountAdded = args.GetNext<int>();

        var playerId = args.GetNext<uint>();
        if (ownerID != playerId) return;

        Inventory playerInventory = GetComponent<Inventory>();
        if (playerInventory == null) return;

        playerInventory.UpdateInventory(DropType.Block, blocksAmountAdded);
    }

    public override void AddDropItemToInventory(RpcArgs args)
    {
        int amountAdded = args.GetNext<int>();
        uint playerId = args.GetNext<uint>();
        DropType dropType = (DropType) args.GetNext<int>();

        if (ownerID == playerId)
        {
            Inventory playerInventory = GetComponent<Inventory>();
            if (playerInventory != null)
            {
                playerInventory.UpdateInventory(dropType, amountAdded);
            }
        }
    }

    public void SendRpcMine(Vector2 target, bool mining)
        => networkObject.SendRpc(RPC_MINE, Receivers.All, ownerID, target, mining);

    public override void Mine(RpcArgs args)
    {
        var playerId = args.GetNext<uint>();
        if (ownerID != playerId) return;

        var target = args.GetNext<Vector2>();
        var enable = args.GetNext<bool>();

        if (!networkObject.IsOwner) pickaxeAnimator.gameObject.SetActive(true);

        pickaxeAnimator.MineAnimation(target, enable);
    }

    public void SendRpcPlaceBlock(Vector2 target)
        => networkObject.SendRpc(RPC_PLACE_BLOCK, Receivers.All, ownerID, target);

    public override void PlaceBlock(RpcArgs args)
    {
        var playerId = args.GetNext<uint>();
        if (ownerID != playerId) return;

        var target = args.GetNext<Vector2>();

        if (!networkObject.IsOwner) pickaxeAnimator.gameObject.SetActive(true);

        pickaxeAnimator.PlaceBlockAnimation(target);
    }

    public override void Explode(RpcArgs args)
    {
        var position = args.GetNext<Vector2>();
        var radius = args.GetNext<int>();

        for (var i = -radius; i <= radius; i++)
        {
            for (var j = -radius; j <= radius; j++)
            {
                if (i * i + j * j <= radius * radius)
                {
                    MapManager.Instance.RemoveTileAtPosition(position + i * Vector2.up + j * Vector2.right);

                    //explosions
                    int explosionChosenIndex = UnityEngine.Random.Range(0, explosions.Length);

                    Instantiate(explosions[explosionChosenIndex], position + i * Vector2.up + j * Vector2.right, Quaternion.identity);
                }
            }
        }
    }
}