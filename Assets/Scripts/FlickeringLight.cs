﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

[RequireComponent(typeof(UnityEngine.Experimental.Rendering.Universal.Light2D))]
public class FlickeringLight : MonoBehaviour
{
    private float m_RndIntensity;
    private float m_RndInnerRadius;
    private float m_RndOuterRadius;

    [SerializeField] float speed = 1f;
    [SerializeField] float intensityScale = 1f;
    [SerializeField] float innerRadiusScale = 1f;
    [SerializeField] float outerRadiusScale = 1f;

    [SerializeField] float randomScale = 1f;

    UnityEngine.Experimental.Rendering.Universal.Light2D flickeringLight;

    [SerializeField] bool useTransformAsSeed = true;
    [SerializeField] int seed = 10;

    private void Start()
    {
        flickeringLight = GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>();
        if (useTransformAsSeed)
        {
            Random.InitState((int)transform.position.x);
        }
        else
        {
            Random.InitState(seed);
        }

        m_RndIntensity = Random.value * randomScale + flickeringLight.intensity;
        m_RndInnerRadius = Random.value * randomScale + flickeringLight.pointLightInnerRadius;
        m_RndOuterRadius = Random.value * randomScale + flickeringLight.pointLightOuterRadius;

        baseIntensity = flickeringLight.intensity;
        baseInnerRadius = flickeringLight.pointLightInnerRadius;
        baseOuterRadius = flickeringLight.pointLightOuterRadius;
    }

    float baseIntensity;
    float baseInnerRadius;
    float baseOuterRadius;
    private void Update()
    {
        float intensityFlicker = intensityScale * (Mathf.PerlinNoise(m_RndIntensity + 0 + (Time.time * speed * 2), m_RndIntensity + 1 + (Time.time * speed * 2)));
        float innerRadiusFlicker = innerRadiusScale * (Mathf.PerlinNoise(m_RndInnerRadius + 0 + (Time.time * speed * 2), m_RndInnerRadius + 1 + (Time.time * speed * 2)));
        float outerRadiusFlicker = outerRadiusScale * (Mathf.PerlinNoise(m_RndOuterRadius + 0 + (Time.time * speed * 2), m_RndOuterRadius + 1 + (Time.time * speed * 2)));

        flickeringLight.intensity = baseIntensity + intensityFlicker;
        flickeringLight.pointLightInnerRadius = baseInnerRadius + innerRadiusFlicker;
        flickeringLight.pointLightOuterRadius = baseOuterRadius + outerRadiusFlicker;
    }
}


