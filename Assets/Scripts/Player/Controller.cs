﻿using System.Collections.Generic;
using Platformer.Mechanics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Character
{
    [RequireComponent(typeof(Stats))]
    public class Controller : Kinematic
    {
        #region Fields

        private new Camera camera;

        private Stats stats;
        private PlayerNetwork playerNetwork;
        private Animator animator;
        private new SpriteRenderer renderer;

        [SerializeField] float minLimitForHorizontalMove = 0.1f;
        private InputMaster _inputMaster;
        
        public bool IsUsingController { get; private set; }

        #endregion

        #region Unity Event Functions

        private TargetingController _targetingController;
        [SerializeField] private float range = 3f;
        [SerializeField] private float margin = 2f;
        private new void Awake()
        {
            stats = GetComponent<Stats>();
            if (stats == null)
                Debug.LogWarning(name + " has a Controller component but no Stats.");

            playerNetwork = GetComponent<PlayerNetwork>();
            if (playerNetwork == null)
                Debug.LogWarning(name + " has a Controller component but no PlayerNetwork.");

            animator = GetComponent<Animator>();
            if (animator == null)
                Debug.LogWarning(name + " has a Controller component but no Animator.");

            renderer = GetComponent<SpriteRenderer>();
            if (renderer == null)
                Debug.LogWarning(name + " has a Controller component but no SpriteRenderer.");

            camera = Camera.main;
            if (camera == null)
                Debug.LogWarning("Controller : no main Camera found");
            _inputMaster = new InputMaster();
            _inputMaster.BasicControls.Enable();
            _inputMaster.BasicControls.Jump.started += delegate { StartJump(); };
            _inputMaster.BasicControls.Jump.performed += delegate{ StopJump();  };
            
            base.Awake();
            _targetingController = new TargetingController(_inputMaster, range, margin);
        }

        public void NetworkStart()
        {
            if (playerNetwork != null
                && playerNetwork.networkObject != null
                && !playerNetwork.networkObject.IsOwner)
            {
                Debug.Log("You are not the owner of " + name + " ; destroying its Controller component.");
                Destroy(this);
            }
        }

        private void Update()
        {
            _targetingController.DetectDeviceUsed();
            Move();
            Animate();
        }

        #endregion

        #region Methods

        private float _jumpTimer;
        private float _jumpInputDuration;
        private int _jumpCounter;
        private float _coyoteTimeCounter;

        // was jump pressed down last frame ?
        private bool _jumpDown;
        // was jump kept pressed last frame ? 
        private bool _jumpPressed;

        private void     StartJump()
        {
            _jumpPressed = true;
            _jumpDown = true;
        }

        private void StopJump()
        {
            _jumpPressed = false;
        }

        private void Move()
        {
            var inputHorizontal = _inputMaster.BasicControls.Move.ReadValue<float>();

            if (IsOnGround) _coyoteTimeCounter = 0.0f;
            else _coyoteTimeCounter += Time.deltaTime;

            if (IsOnGround || _coyoteTimeCounter < stats.CoyoteTime) _jumpCounter = 0;

            if (_jumpDown && _jumpCounter < stats.JumpCount)
            {
                _jumpTimer = 0.0f;
                _jumpInputDuration = 0.0f;
                _jumpCounter++;
                IsOnGround = false;
            }

            // Keep rising as long as the jump key is pressed (with a maximum time)
            if (_jumpPressed && _jumpInputDuration <= stats.JumpInputTimeMax && _jumpCounter <= stats.JumpCount)
                _jumpInputDuration += Time.deltaTime;
            else
            {
                _jumpTimer += Time.deltaTime;
                _jumpInputDuration = stats.JumpInputTimeMax + 1.0f;
                if (_jumpCounter == 0) _jumpCounter++;
            }

            if (GrapplingHook != null && GrapplingHook.Released)
                _jumpTimer = Vector2.Angle(Vector2.up, speed) / 180.0f * stats.JumpEnd;

            if (GrapplingHook == null || !GrapplingHook.EndOfRope || !GrapplingHook.Hook.IsHooked)
                MoveClassic(inputHorizontal);
            else
                MoveSwing(inputHorizontal);
            
            TryMove();

            if (Mathf.Abs(speed.y) < 0.001f)
            {
                _jumpTimer = stats.JumpMiddle;
                _jumpInputDuration = stats.JumpInputTimeMax + 1.0f;
            }

            if (_jumpDown) _jumpDown = false;
        }

        private void MoveClassic(float inputHorizontal)
        {
            if (!IsOnGround && Mathf.Abs(speed.x) > stats.AirSpeedMax)
                speed.x += inputHorizontal * stats.AirAcceleration * Time.deltaTime
                    + (speed.x > 0.0f ? -1.0f : 1.0f) * stats.AirAcceleration * Time.deltaTime;
            else
            {    
                if (Mathf.Abs(inputHorizontal) > minLimitForHorizontalMove)
                {
                    var speedTarget = inputHorizontal
                                      * (IsOnGround ? stats.HorizontalGroundSpeedMax : stats.AirSpeedMax);

                    var speedDelta = Time.deltaTime
                                     * (speed.x < speedTarget ? 1.0f : -1.0f)
                                     * (IsOnGround ? stats.HorizontalGroundAcceleration : stats.AirAcceleration);

                    speed.x = (speed.x + speedDelta) * speedTarget < speedTarget * speedTarget
                        ? speed.x + speedDelta
                        : speedTarget;
                }
                else
                {
                    var speedDelta = Time.deltaTime
                                     * (speed.x < 0.0f ? 1.0f : -1.0f)
                                     * (IsOnGround ? stats.HorizontalGroundAcceleration : stats.AirAcceleration);

                    speed.x = (speed.x + speedDelta) * speed.x > 0.0f
                        ? speed.x + speedDelta
                        : 0.0f;
                }
            }

            if (speed.y <= stats.GetJumpSpeed(0.0f))
                speed.y = stats.GetJumpSpeed(_jumpTimer);
            else
            {
                speed.y -= stats.AirAcceleration * Time.deltaTime;
                _jumpTimer = 0.0f;
            }
        }

        private void MoveSwing(float inputHorizontal)
        {
            var R = -GrapplingHook.PosToHinge.normalized;
            var T = Vector2.Perpendicular(R);
            
            speed += (R.y < 0.0f ? 1.0f : -1.0f)
                 * Time.deltaTime * stats.SwingSideAcceleration * inputHorizontal * T;

            speed += stats.SwingDownAcceleration * Time.deltaTime * Vector2.down;
            speed *= 1.0f - stats.SwingFriction * Time.deltaTime;

            speed = Vector3.Project(speed, T);

            if (speed.magnitude > stats.SwingSpeedMax) speed = stats.SwingSpeedMax * speed.normalized;
        }

        private void Animate()
        {
            if (playerNetwork == null || playerNetwork.networkObject == null)
            {
                if (speed.x < -0.01f) renderer.flipX = true;
                if (speed.x > 0.01f) renderer.flipX = false;
                // If speed.x is 0.0f, don't change current flip

                animator?.SetBool("grounded", IsOnGround);
                animator?.SetFloat("velocityX", Mathf.Abs(speed.x));
            }
            else
            {
                if (speed.x < -0.01f) playerNetwork.networkObject.flipX = true;
                if (speed.x > 0.01f) playerNetwork.networkObject.flipX = false;
                // If speed.x is 0.0f, don't change current flip

                playerNetwork.networkObject.isGrounded = IsOnGround;
                playerNetwork.networkObject.velocityX = Mathf.Abs(speed.x);
            }
        }
        
        public InputMaster GetInputMaster()
        {
            return _inputMaster;
        }

        public Vector2 GetCursorPositionInWorldSpace()
        {
            return _targetingController.GetCursorPositionInWorldSpace(transform.position);
        }

        public Vector2 GetCursorPositionInScreenSpace()
        {
            return _targetingController.GetCursorPositionInScreenSpace(transform.position);
        }

        public Vector2 GetLookPosition()
        {
            return _targetingController.GetLookPosition(transform.position);
        }
        
        #endregion
    }
}