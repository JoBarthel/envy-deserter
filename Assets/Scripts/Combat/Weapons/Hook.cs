﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class Hook : MonoBehaviour
{
    public float speed = 30.0f;

    [SerializeField] private ContactFilter2D contactFilter;
    
    private new Rigidbody2D rigidbody;
    private new Collider2D collider;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];

    public bool IsHooked { get; private set; }
    public bool JustHooked { get; private set; }

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        JustHooked = false;
        if (IsHooked) return;

        Vector2 move = transform.up * speed * Time.deltaTime;
        transform.position += (Vector3) move;
        DetectCollision(move);
    }
    
    private void DetectCollision(Vector2 move)
    {
        var distance = move.magnitude;
        var count = rigidbody.Cast(move, contactFilter, hitBuffer, distance + collider.bounds.extents.x);
        
        for (int i = 0; i < count; i++)
            ProcessTargetHit(hitBuffer[i]);
    }

    private void ProcessTargetHit(RaycastHit2D raycastHit2D)
    {
        var collidedTag = raycastHit2D.collider.tag;
        if (String.Equals(collidedTag, "Terrain"))
        {
            JustHooked = true;
            IsHooked = true;
        }
    }

    public void Launch(Vector2 dir)
    {
        IsHooked = false;
        transform.up = dir;
    }
}
