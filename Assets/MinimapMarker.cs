﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimapMarker : MonoBehaviour
{
    [SerializeField] MapManager mapManager;

    [SerializeField] RectTransform minimapBordersRectTransform;

    public Transform targetToFollow;

    [SerializeField] float timeBetweenMarkerUpdatesInMinimap = 1f;
    float timeSinceLastMarkerUpdatesInMinimap;

    [SerializeField] Image iconImage;
    Color originalColor;

    private void Start()
    {
        timeSinceLastMarkerUpdatesInMinimap = timeBetweenMarkerUpdatesInMinimap;

        if(iconImage == null)
        {
            iconImage = GetComponent<Image>();
        }

        if (iconImage != null)
        {
            originalColor = iconImage.color;
        }
    }

    void UpdateChestMarkerPosition()
    {
        float posX = -minimapBordersRectTransform.sizeDelta.x + minimapBordersRectTransform.sizeDelta.x * (targetToFollow.position.x - mapManager.transform.position.x) / (mapManager.mapGenerator.map.GetLength(0) * mapManager.grid.cellSize.x - mapManager.transform.position.x);
        float posY = 0f + minimapBordersRectTransform.sizeDelta.y * (targetToFollow.position.y - mapManager.transform.position.y) / (mapManager.mapGenerator.map.GetLength(1) * mapManager.grid.cellSize.y - mapManager.transform.position.y);

        transform.localPosition = new Vector3(posX, posY, 0f);
    }

    private void Update()
    {
        timeSinceLastMarkerUpdatesInMinimap += Time.deltaTime;


        if(timeSinceLastMarkerUpdatesInMinimap >= timeBetweenMarkerUpdatesInMinimap)
        {
            timeSinceLastMarkerUpdatesInMinimap = 0f;

            if(targetToFollow != null)
            {
                UpdateChestMarkerPosition();
                if(iconImage != null)
                {
                    iconImage.color = originalColor;
                }
            }
            else
            {
                if (iconImage != null)
                {
                    Color transparentColor = originalColor;
                    transparentColor.a = 0f;
                    iconImage.color = transparentColor;
                }
            }


        }

    }

}
