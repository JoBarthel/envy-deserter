﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class ExplodingProjectile : Projectile
{
    [SerializeField] private int explosionRadius = 3;
    
    protected override void Hit(Player player) => Explode();

    protected override void Hit(Building building) => Explode();

    protected override void Hit() => Explode();

    private RaycastHit2D[] buffer = new RaycastHit2D[16];

    private void Explode()
    {
        var count = Physics2D.CircleCastNonAlloc(transform.position, explosionRadius, direction,
            buffer, explosionRadius, contactFilter.layerMask);

        for (var i = 0; i < count; i++)
        {
            var player = buffer[i].transform.GetComponent<Player>();
            player?.Network?.networkObject?.SendRpc(PlayerNetworkBehavior.RPC_TAKE_DAMAGE, Receivers.All, damage);

            var building = buffer[i].transform.GetComponent<Building>();
            building?.networkObject?.SendRpc(BuildingBehavior.RPC_TAKE_DAMAGE, Receivers.All, damage);
        }

        if (playerOwner != GameManagerNetwork.gameManagerNetwork.localPlayerId) return;

        var playerNetwork = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(playerOwner);
        playerNetwork.networkObject.SendRpc(PlayerNetworkBehavior.RPC_EXPLODE, Receivers.All,
            (Vector2) transform.position, explosionRadius);
    }
}
