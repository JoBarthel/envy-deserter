﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Character;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;

public enum State
{
    Firing, 
    Digging, 
    Construction
}

// State machine for the current mode of interaction (digging, firing, construction)
// broadcast its state to observers
public class GunSelection : MonoBehaviour, IObservable<State>
{
    [SerializeField] private Controller controller;
    [SerializeField] private State defaultState;
    [SerializeField] private GameObject weapon;
    [SerializeField] private GameObject diggingTool;
    [SerializeField] private GameObject constructionTool;

    private State _currentState;
    private InputMaster _inputMaster;
    public void Start()
    {
        Debug.Assert(weapon != null, "weapon is null in GunSelection");
        Debug.Assert(diggingTool != null, "digging tool is null in GunSelection");
        Debug.Assert(constructionTool != null, "construction tool is null in GunSelection");
        _currentState = defaultState;
        SwitchToState(_currentState);
        _inputMaster = controller.GetInputMaster();
        _inputMaster.BasicControls.SelectDig.performed += delegate { SelectDiggingMode();  };
        _inputMaster.BasicControls.SelectFire.performed += delegate { SelectFiringMode(); };
        _inputMaster.BasicControls.SelectConstruction.performed += delegate { SelectConstructionMode(); };
    }

    private void SwitchToState(State state)
    {
        switch (state)
        {
            case State.Construction:
                SelectConstructionMode();
                break;
            case State.Digging:
                SelectDiggingMode();
                break;
            case State.Firing:
                SelectFiringMode();
                break;
        }
    }

    // we can't set a amount needed for a mouse scroll in the input system 
    // so we need to handle it here
    [SerializeField] private float scrollAmountNeeded;
    private float _currentScrollAmount;
    private void Update()
    {
        float amount = _inputMaster.BasicControls.ScrollMode.ReadValue<float>() * Time.deltaTime;
        _currentScrollAmount += amount;
        if (_currentScrollAmount >= scrollAmountNeeded)
        {
            _currentScrollAmount = 0;
            ScrollForward();
        }

        if(_currentScrollAmount <= -scrollAmountNeeded)
        {
            _currentScrollAmount = 0;
            ScrollBackward();
        }
    }

    private void SelectFiringMode()
    {
        _currentState = State.Firing;
        weapon.SetActive(true);
        diggingTool.SetActive(false);
        constructionTool.SetActive(false);
        NotifyObservers();
    }

    private void SelectDiggingMode()
    {
        _currentState = State.Digging;
        diggingTool.SetActive(true);
        weapon.SetActive(false);
        constructionTool.SetActive(false);
        NotifyObservers();
    }

    private void SelectConstructionMode()
    {
        _currentState = State.Construction;
        constructionTool.SetActive(true);
        weapon.SetActive(false);
        diggingTool.SetActive(false);
        NotifyObservers();
    }

    private void ScrollForward()
    {
        switch (_currentState)
        {
            case State.Digging:
                SwitchToState(State.Firing);
                break;
            case State.Firing:
                SwitchToState(State.Construction);
                break;
            case State.Construction:
                SwitchToState(State.Digging);
                break;
        }
    }
    
    private void ScrollBackward()
    {
        switch (_currentState)
        {
            case State.Digging:
                SwitchToState(State.Construction);
                break;
            case State.Firing:
                SwitchToState(State.Digging);
                break;
            case State.Construction:
                SwitchToState(State.Firing);
                break;
        }
    }

    private readonly List<IObserver<State>> _observers = new List<IObserver<State>>();
    public IDisposable Subscribe(IObserver<State> observer)
    {
        _observers.Add(observer);
        return new Unsubscriber(_observers, observer);
    }

    private void NotifyObservers()
    {
        foreach (IObserver<State> observer in _observers)
        {
            observer.OnNext(_currentState);
        }
    }
    
    private class Unsubscriber : IDisposable
    {
        private readonly List<IObserver<State>> _observers;
        private readonly IObserver<State> _observer;

        public Unsubscriber(List<IObserver<State>> observers, IObserver<State> observer)
        {
            _observers = observers;
            _observer = observer;
        }
        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}


