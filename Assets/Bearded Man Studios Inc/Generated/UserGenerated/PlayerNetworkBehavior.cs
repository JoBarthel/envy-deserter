using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedRPC("{\"types\":[[\"int\"][\"Vector2\", \"Vector2\", \"uint\"][\"int\", \"uint\"][\"int\", \"uint\", \"int\"][\"uint\", \"Vector2\", \"bool\"][\"uint\", \"Vector2\"][\"Vector2\", \"int\"]]")]
	[GeneratedRPCVariableNames("{\"types\":[[\"damage\"][\"angle\", \"position\", \"owner\"][\"blocksAmountAdded\", \"playerId\"][\"amountAdded\", \"playerId\", \"dropType\"][\"ownerID\", \"target\", \"enable\"][\"ownerID\", \"target\"][\"position\", \"radius\"]]")]
	public abstract partial class PlayerNetworkBehavior : NetworkBehavior
	{
		public const byte RPC_TAKE_DAMAGE = 0 + 5;
		public const byte RPC_SHOOT = 1 + 5;
		public const byte RPC_ADD_BLOCKS_TO_INVENTORY = 2 + 5;
		public const byte RPC_ADD_DROP_ITEM_TO_INVENTORY = 3 + 5;
		public const byte RPC_MINE = 4 + 5;
		public const byte RPC_PLACE_BLOCK = 5 + 5;
		public const byte RPC_EXPLODE = 6 + 5;
		
		public PlayerNetworkNetworkObject networkObject = null;

		public override void Initialize(NetworkObject obj)
		{
			// We have already initialized this object
			if (networkObject != null && networkObject.AttachedBehavior != null)
				return;
			
			networkObject = (PlayerNetworkNetworkObject)obj;
			networkObject.AttachedBehavior = this;

			base.SetupHelperRpcs(networkObject);
			networkObject.RegisterRpc("TakeDamage", TakeDamage, typeof(int));
			networkObject.RegisterRpc("Shoot", Shoot, typeof(Vector2), typeof(Vector2), typeof(uint));
			networkObject.RegisterRpc("AddBlocksToInventory", AddBlocksToInventory, typeof(int), typeof(uint));
			networkObject.RegisterRpc("AddDropItemToInventory", AddDropItemToInventory, typeof(int), typeof(uint), typeof(int));
			networkObject.RegisterRpc("Mine", Mine, typeof(uint), typeof(Vector2), typeof(bool));
			networkObject.RegisterRpc("PlaceBlock", PlaceBlock, typeof(uint), typeof(Vector2));
			networkObject.RegisterRpc("Explode", Explode, typeof(Vector2), typeof(int));

			networkObject.onDestroy += DestroyGameObject;

			if (!obj.IsOwner)
			{
				if (!skipAttachIds.ContainsKey(obj.NetworkId)){
					uint newId = obj.NetworkId + 1;
					ProcessOthers(gameObject.transform, ref newId);
				}
				else
					skipAttachIds.Remove(obj.NetworkId);
			}

			if (obj.Metadata != null)
			{
				byte transformFlags = obj.Metadata[0];

				if (transformFlags != 0)
				{
					BMSByte metadataTransform = new BMSByte();
					metadataTransform.Clone(obj.Metadata);
					metadataTransform.MoveStartIndex(1);

					if ((transformFlags & 0x01) != 0 && (transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() =>
						{
							transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform);
							transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform);
						});
					}
					else if ((transformFlags & 0x01) != 0)
					{
						MainThreadManager.Run(() => { transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform); });
					}
					else if ((transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() => { transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform); });
					}
				}
			}

			MainThreadManager.Run(() =>
			{
				NetworkStart();
				networkObject.Networker.FlushCreateActions(networkObject);
			});
		}

		protected override void CompleteRegistration()
		{
			base.CompleteRegistration();
			networkObject.ReleaseCreateBuffer();
		}

		public override void Initialize(NetWorker networker, byte[] metadata = null)
		{
			Initialize(new PlayerNetworkNetworkObject(networker, createCode: TempAttachCode, metadata: metadata));
		}

		private void DestroyGameObject(NetWorker sender)
		{
			MainThreadManager.Run(() => { try { Destroy(gameObject); } catch { } });
			networkObject.onDestroy -= DestroyGameObject;
		}

		public override NetworkObject CreateNetworkObject(NetWorker networker, int createCode, byte[] metadata = null)
		{
			return new PlayerNetworkNetworkObject(networker, this, createCode, metadata);
		}

		protected override void InitializedTransform()
		{
			networkObject.SnapInterpolations();
		}

		/// <summary>
		/// Arguments:
		/// int damage
		/// </summary>
		public abstract void TakeDamage(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// Vector2 angle
		/// Vector2 position
		/// uint owner
		/// </summary>
		public abstract void Shoot(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int blocksAmountAdded
		/// uint playerId
		/// </summary>
		public abstract void AddBlocksToInventory(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int amountAdded
		/// uint playerId
		/// int dropType
		/// </summary>
		public abstract void AddDropItemToInventory(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// uint ownerID
		/// Vector2 target
		/// bool enable
		/// </summary>
		public abstract void Mine(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// uint ownerID
		/// Vector2 target
		/// </summary>
		public abstract void PlaceBlock(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// Vector2 position
		/// int radius
		/// </summary>
		public abstract void Explode(RpcArgs args);

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}