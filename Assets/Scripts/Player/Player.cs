﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Character;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Controller controller;
    public Controller Controller => controller ?? (controller = GetComponent<Controller>());

    private Stats stats;
    public Stats Stats => stats ?? (stats = GetComponent<Stats>());

    private PlayerNetwork network;
    public PlayerNetwork Network => network ?? (network = GetComponent<PlayerNetwork>());

    private new Rigidbody2D rigidbody;
    public Rigidbody2D Rigidbody => rigidbody ?? (rigidbody = GetComponent<Rigidbody2D>());

    private new Collider2D collider;
    public Collider2D Collider => collider ?? (collider = GetComponent<Collider2D>());

    private new SpriteRenderer renderer;
    public SpriteRenderer Renderer => renderer ?? (renderer = GetComponent<SpriteRenderer>());

    public Inventory inventory;

    private void Awake()
    {
        controller = GetComponent<Controller>();
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        renderer = GetComponent<SpriteRenderer>();
        stats = GetComponent<Stats>();
        network = GetComponent<PlayerNetwork>();
        inventory = GetComponent<Inventory>();
    }

    private void Start()
    {
        Game.Instance.RegisterPlayer(this);
    }

}