﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableObjectPieButton : PieButton
{
    [SerializeField] private GameObject objectToEnable;

    public override void Activate()
    {
        if (objectToEnable != null) objectToEnable.SetActive(!objectToEnable.activeSelf);
    }
}
