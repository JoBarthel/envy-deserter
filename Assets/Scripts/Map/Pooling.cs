﻿using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{
    public enum ProjectileType { NORMAL, EXPLODING }

    public static Pooling Instance;
    
    [SerializeField] private int initalProjectileCount = 10;

    private List<NormalProjectile> normalProjectiles = new List<NormalProjectile>();
    private List<ExplodingProjectile> explodingProjectiles = new List<ExplodingProjectile>();

    [SerializeField] private NormalProjectile _normalProjectilePrefab;
    [SerializeField] private ExplodingProjectile _explodingProjectilePrefab;

    [SerializeField] float timeToDeactivateParticles = 3f;

    // TODO instead of all those switches, fill a dictionary with all projectile types pointing to each list

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        for (int i = 0; i < initalProjectileCount; i++)
        {
            InstantiateProjectile(_normalProjectilePrefab);
            InstantiateProjectile(_explodingProjectilePrefab);
        }
    }

    public void InstantiateProjectile(ProjectileType type)
    {
        switch (type)
        {
            case ProjectileType.NORMAL:
                var normalProjectile = Instantiate(_normalProjectilePrefab, transform.position, Quaternion.identity, transform);
                normalProjectiles.Add(normalProjectile);
                normalProjectile.gameObject.SetActive(false);
                break;

            case ProjectileType.EXPLODING:
                var explodingProjectile = Instantiate(_explodingProjectilePrefab, transform.position, Quaternion.identity, transform);
                explodingProjectiles.Add(explodingProjectile);
                explodingProjectile.gameObject.SetActive(false);
                break;
        }
    }

    public void InstantiateProjectile(Projectile prefab)
    {
        switch (prefab)
        {
            case NormalProjectile np:
                InstantiateProjectile(ProjectileType.NORMAL);
                break;

            case ExplodingProjectile ep:
                InstantiateProjectile(ProjectileType.EXPLODING);
                break;
        }
    }

    public Projectile GetProjectile(ProjectileType type)
    {
        Projectile projectile = null;

        switch (type)
        {
            case ProjectileType.NORMAL:
                if (normalProjectiles.Count <= 0)
                    InstantiateProjectile(ProjectileType.NORMAL);
                projectile = normalProjectiles[0];
                normalProjectiles.RemoveAt(0);
                break;

            case ProjectileType.EXPLODING:
                if (explodingProjectiles.Count <= 0)
                    InstantiateProjectile(ProjectileType.EXPLODING);
                projectile = explodingProjectiles[0];
                explodingProjectiles.RemoveAt(0);
                break;
        }
        
        if (projectile == null) return null;

        projectile.gameObject.SetActive(true);
        projectile.transform.SetParent(null);

        return projectile;
    }

    public Projectile GetProjectile(Projectile prefab)
    {
        switch (prefab)
        {
            case NormalProjectile np:
                return GetProjectile(ProjectileType.NORMAL);

            case ExplodingProjectile ep:
                return GetProjectile(ProjectileType.EXPLODING);
        }

        return null;
    }

    public void PoolProjectile(Projectile projectile, GameObject particles)
        => StartCoroutine(DisableParticlesAndPoolProjectile(particles, projectile));

    public void PoolProjectile(Projectile projectile)
    {
        projectile.gameObject.SetActive(false);
        projectile.transform.SetParent(transform);

        switch (projectile)
        {
            case NormalProjectile np:
                normalProjectiles.Add(np);
                return;

            case ExplodingProjectile ep:
                explodingProjectiles.Add(ep);
                break;
        }
    }
    
    IEnumerator DisableParticlesAndPoolProjectile(GameObject particles, Projectile projectile)
    {
        particles.transform.SetParent(null);
        projectile.gameObject.SetActive(false);
        projectile.transform.SetParent(transform);

        yield return new WaitForSeconds(timeToDeactivateParticles);

        particles.transform.position = projectile.transform.position;
        particles.transform.SetParent(projectile.transform);

        PoolProjectile(projectile);
    }
}
