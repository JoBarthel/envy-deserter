﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageDisplay;
    [SerializeField] private Image endScreenPanel;

    private void Start()
    {    
    }

    public void Display(String message)
    {
        messageDisplay.text = message;
        endScreenPanel.gameObject.SetActive(true);
    }
}
