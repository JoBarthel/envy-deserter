﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class ControlLightAfterExplosion : MonoBehaviour
{
    [SerializeField] UnityEngine.Experimental.Rendering.Universal.Light2D light;

    float timeToDisappear;
    private void Awake()
    {
        Animator anim = GetComponentInParent<Animator>();

        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

        timeToDisappear = clips[0].length - 0.1f;

        transform.SetParent(null);
    }

    float timeSinceInstantiation = 0f;
    bool alreadyDestroyedFlicker = false;

    [SerializeField] float speedToTurnOffLight = 1f;
    private void Update()
    {
        timeSinceInstantiation += Time.deltaTime;

        if(timeSinceInstantiation >= timeToDisappear)
        {
            if (!alreadyDestroyedFlicker)
            {
                alreadyDestroyedFlicker = true;
                FlickeringLight flickeringLight = GetComponent<FlickeringLight>();
                if(flickeringLight != null)
                {
                    Destroy(flickeringLight);
                }
            }

            light.intensity -= speedToTurnOffLight * Time.deltaTime;
            if(light.intensity <= 0.35f)
            {
                Destroy(gameObject);
            }
        }
    }


}
