﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using TMPro;
using UnityEngine;

public class DropItem : DropItemBehavior
{
    [HideInInspector] public PlayerNetwork targetPlayerNetwork = null;
    ChestNetwork targetChestNetwork = null;

    Transform targetTransform = null;

    [HideInInspector] public bool followTarget = false;

    public float speed = 1f;
    public float distanceToAddToInventory = 0.2f;

    [HideInInspector] public bool networkHasStarted = false;

    [SerializeField] bool arrived = false;

    bool alreadyReceivedRPC = false;

    [SerializeField] bool destroyAfterSomeTime = true;
    [SerializeField] float timeToDestroy = 60f;

    float timeSinceStart = 0f;

    public DropType dropType = DropType.Block;

    public int amountOfItemsHeld = 1;

    [SerializeField] private TextMeshProUGUI textToShowAmount;
    [SerializeField] float timeAfterSpawnToAllowGrab = 0f;
    float timeSinceSpawn = 0f;


    protected override void NetworkStart()
    {
        base.NetworkStart();

        networkObject.positionInterpolation.Enabled = false;

        networkObject.shouldDeactivate = false;

        if (!alreadyReceivedRPC)
        {
            networkObject.followTarget = false;
        }

        networkObject.arrivedToTarget = false;
        networkObject.position = transform.position;

        networkHasStarted = true;

        networkObject.UpdateInterval = 25;

        if (!networkObject.IsOwner)
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            transform.position = networkObject.position;
        }

        transform.SetParent(GameManagerNetwork.gameManagerNetwork.transform);

        if (timeAfterSpawnToAllowGrab > 0)
        {
            detectorRegion = transform.GetChild(0).gameObject;
            if (detectorRegion != null)
            {
                detectorRegion.SetActive(false);
            }
        }
    }

    private void Awake()
    {
        if (timeAfterSpawnToAllowGrab > 0)
        {
            detectorRegion = transform.GetChild(0).gameObject;
            if (detectorRegion != null)
            {
                detectorRegion.SetActive(false);
            }
        }
    }

    [SerializeField] GameObject detectorRegion;
    bool alreadyActivatedDetectorRegion = false;

    Collider2D collider2d;
    Rigidbody2D rb;
    bool alreadyUpdatedInterpolation = false;
    private void Update()
    {
        if (!alreadyUpdatedInterpolation)
        {
            networkObject.positionInterpolation.Enabled = true;
            alreadyUpdatedInterpolation = true;
        }

        timeSinceSpawn += Time.deltaTime;
        if (timeSinceSpawn <= timeAfterSpawnToAllowGrab)
        {
            return;
        }
        else if (timeAfterSpawnToAllowGrab > 0 && !alreadyActivatedDetectorRegion)
        {
            alreadyActivatedDetectorRegion = true;
            //detectorRegion = GetComponentInChildren<DetectPlayerTouchedBlockDropItem>().gameObject;
            if (detectorRegion != null)
            {
                detectorRegion.SetActive(true);
            }
            //detectorRegion.SetActive(true);
        }


        if (networkObject.followTarget)
        {
            if (targetPlayerNetwork == null)
            {
                targetPlayerNetwork = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(networkObject.targetPlayerId);
            }

            if (targetPlayerNetwork != null)
            {
                targetChestNetwork = targetPlayerNetwork.playerChestNetwork;
            }

            if (targetPlayerNetwork != null && networkObject.doNotGoToChest)
            {
                targetTransform = targetPlayerNetwork.transform;
            }

            if (targetChestNetwork != null && !networkObject.doNotGoToChest)
            {
                targetTransform = targetChestNetwork.transform;
            }

            if (targetTransform != null)
            {
                if (collider2d == null)
                {
                    collider2d = GetComponent<Collider2D>();
                }

                if (collider2d != null)
                {
                    collider2d.enabled = false;
                }

                if (rb == null)
                {
                    rb = GetComponent<Rigidbody2D>();
                }
                if (rb != null)
                {
                    //Destroy(rb);
                    rb.isKinematic = true;
                }

            }
            else
            {
                alreadyReceivedRPC = false;
                networkObject.followTarget = false;
                networkObject.targetPlayerId = 0;
                networkObject.doNotGoToChest = true;
                if (collider2d == null) collider2d = GetComponent<Collider2D>();
                if (collider2d != null) collider2d.enabled = true;
                if (rb == null) rb = GetComponent<Rigidbody2D>();
                if (rb != null) rb.isKinematic = false;
                DetectPlayerTouchedBlockDropItem detectPlayerTouchedBlockDropItem = GetComponentInChildren<DetectPlayerTouchedBlockDropItem>();
                detectPlayerTouchedBlockDropItem.localAlreadySentRPC = false;
            }

            if (networkObject.IsOwner && targetTransform != null)
            {
                transform.position += (targetTransform.position - transform.position).normalized * speed * Time.deltaTime;


                if ((targetTransform.position - transform.position).sqrMagnitude <= distanceToAddToInventory * distanceToAddToInventory)
                {
                    if (networkObject.doNotGoToChest)
                    {
                        targetPlayerNetwork.networkObject.SendRpc(PlayerNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, Receivers.All, amountOfItemsHeld, networkObject.targetPlayerId, (int)dropType);
                    }
                    else
                    {
                        targetChestNetwork.networkObject.SendRpc(ChestNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, Receivers.All, amountOfItemsHeld, networkObject.targetPlayerId, (int)dropType);
                    }


                    networkObject.arrivedToTarget = true;
                    arrived = true;
                    networkObject.Destroy();
                }
            }


            if (networkObject.arrivedToTarget)
            {
                Destroy(gameObject);
            }
        }

        if (!alreadyUpdatedInterpolation)
        {
            networkObject.positionInterpolation.Enabled = true;
            alreadyUpdatedInterpolation = true;
        }

        if (networkObject.IsOwner)
        {
            networkObject.position = transform.position;
            timeSinceStart += Time.deltaTime;
            if (destroyAfterSomeTime && timeSinceStart >= timeToDestroy)
            {
                networkObject.Destroy();
            }
        }
        else
        {
            transform.position = networkObject.position;
        }
    }

    public override void TriggerDropItem(RpcArgs args)
    {
        if (networkObject.IsOwner && !alreadyReceivedRPC)
        {

            alreadyReceivedRPC = true;
            networkObject.followTarget = true;
            networkObject.targetPlayerId = args.GetNext<uint>();
            networkObject.position = args.GetNext<Vector3>();
            int dropType = args.GetNext<int>();
            networkObject.doNotGoToChest = args.GetNext<bool>();

        }
    }

    public void CallRpcAfterNetworkStart(NetworkBehavior networkBehavior)
    {
        networkObject.SendRpc(DropItem.RPC_TRIGGER_DROP_ITEM, Receivers.Owner, targetPlayerNetwork.networkObject.NetworkId, transform.position, (int)dropType, true);
    }

    public void CallRpcAfterNetworkStartToGoToChest(NetworkBehavior networkBehavior)
    {
        networkObject.SendRpc(DropItem.RPC_TRIGGER_DROP_ITEM, Receivers.Owner, targetPlayerNetwork.networkObject.NetworkId, transform.position, (int)dropType, false);
    }

    public override void UpdateAmountOfItemsHeld(RpcArgs args)
    {
        amountOfItemsHeld = args.GetNext<int>();

        if (textToShowAmount != null)
        {
            textToShowAmount.text = "x" + amountOfItemsHeld.ToString();
        }
    }

    public void TransmitMyAmountOfItemsHeldToOtherInstances(NetworkBehavior networkBehavior)
    {
        networkObject.SendRpc(RPC_UPDATE_AMOUNT_OF_ITEMS_HELD, Receivers.All, amountOfItemsHeld);
    }
}


public enum DropType
{
    Block,
    Crystal
}

