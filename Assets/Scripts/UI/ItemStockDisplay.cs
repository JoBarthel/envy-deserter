﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class ItemStockDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI display;

    [SerializeField] private string template = "x {0}";

    [SerializeField] private float blinkColorTime;
    [SerializeField] private float maxSizeReachedInAnimation;
    private Color _baseColor;
    private float _baseSize;
    private Coroutine _blinkAnimation;

    private void Start()
    {
        _baseColor = display.color;
        _baseSize = display.fontSize;
    }

    public void SetText(string numberOfItems)
    {
        display.text = String.Format(template, numberOfItems);
    }

    public void BlinkColor(Color color)
    {
        if(_blinkAnimation != null)
            StopCoroutine(_blinkAnimation);
        _blinkAnimation = StartCoroutine(BlinkColorCoroutine(color, maxSizeReachedInAnimation));
    }

    IEnumerator BlinkColorCoroutine(Color color, float maxSize)
    {
        float currentTime = 0;
        while (currentTime <= blinkColorTime)
        {
            currentTime += Time.deltaTime;
            float fracTime = currentTime / blinkColorTime;
            UpdateColor(color, fracTime);
            UpdateSize(maxSize, fracTime);
            yield return null;
        }

        display.color = _baseColor;
        display.fontSize = _baseSize;
    }

    private void UpdateSize(float maxSize, float fracTime)
    {
        float newSize = Mathf.Lerp(_baseSize, maxSize, fracTime);
        display.fontSize = newSize;
    }


    private void UpdateColor(Color color, float fracTime)
    {
        float r = Mathf.Lerp(_baseColor.r, color.r, fracTime);
        float g = Mathf.Lerp(_baseColor.g, color.g, fracTime);
        float b = Mathf.Lerp(_baseColor.b, color.b, fracTime);
        float a = Mathf.Lerp(_baseColor.a, color.a, fracTime);
        Color newColor = new Color(r, g, b, a);
        display.color = newColor;
    }
}
