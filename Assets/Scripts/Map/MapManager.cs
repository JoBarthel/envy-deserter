﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class MapManager : MapManagerBehavior
{
    public static MapManager Instance { get; private set; }

    public MapGenerator mapGenerator;

    public Tile highlightTile;
    [SerializeField] Tilemap highlightMap;
    [SerializeField] Tilemap frontgroundMapWithoutParallax;
    [SerializeField] Tilemap frontgroundMapWithParallax;
    [SerializeField] Tilemap backgroundMap;
    [SerializeField] Tilemap farBackgroundMap;
    [SerializeField] LayerMask placeTilesLayersMaskToCheck;

    public int blockIndexPlayerPlaces = 5;

    [SerializeField] BlockInfo[] blocksInfo;
    [SerializeField] BlockInfo[] resourcesInfo;     //veins of resources

    [Header("Global Frontground Tiles")]
    [SerializeField] GlobalFrontgroundTile[] frontgroundTilesForCeiling;

    [Header("Global Background Tiles")]
    [SerializeField] GlobalBackgroundTile[] backgroundTilesForEmptySpaces;
    [SerializeField] GlobalBackgroundTile[] globalBackgroundTiles;
    [SerializeField] GlobalBackgroundTile[] globalFarBackgroundTiles;


    [Space]
    [Space]

    public Grid grid;

    [SerializeField] MinimapUncover minimapUncover;

    [SerializeField] Camera playerCamera;

    [Header("Minimap Settings")]
    [SerializeField] Camera minimapCamera;
    [SerializeField] RectTransform minimapBordersRectTransform;
    [SerializeField] RectTransform minimapImageRectTransform;

    bool alreadyGotMapFromServer = false;
    bool alreadyFilledMap = false;

    [Space]
    [Header("Selected Tiles")]
    [SerializeField] Tilemap selectionMap;

    private Vector3Int[] hoverTiles = new Vector3Int[0];
    public Vector3Int[] HoverTiles => hoverTiles;
    private int sizeOfSquareSelectionTiles = 0;
    [SerializeField] TileBase selectionTileModel;

    [Space]
    [Header("Camera Settings")]
    [SerializeField] PolygonCollider2D cinemachineConfinner;

    protected void Awake()
    {
        if (Instance == null) Instance = this;
        else
        {
            Destroy(this);
            return;
        }

        if (playerCamera == null) playerCamera = Camera.main;

        if (networkObject != null) return;

        mapGenerator.GenerateMap();
        alreadyGotMapFromServer = true;
    }

    protected override void NetworkStart()
    {
        base.NetworkStart();
        
        if (networkObject == null || !networkObject.IsServer) return;

        mapGenerator.GenerateMap();
        alreadyGotMapFromServer = true;
        networkObject.SendRpc(RPC_RETRIEVE_GENERATED_MAP, Receivers.All, ObjectToByteArray(mapGenerator.map));
    }

    private void Start()
    {
        selectionMap.ClearAllTiles();

        for (var i = 0; i < numberOfMiningAnimationsToSpawnAtGameStart; i++)
        {
            Animator newlyAddedAnimator = Instantiate(miningAnimationPrefab, Vector3.zero, Quaternion.identity);
            newlyAddedAnimator.transform.SetParent(miningAnimationParent.transform);
            miningAnimationsAvailableToUse.Add(newlyAddedAnimator);
            newlyAddedAnimator.gameObject.SetActive(false);
        }
    }

    /**
     * Resize the hoverTiles vector so it has newSizeOfSquareSelectionTiles size length 
     */
    public void ChangeSizeOfHoverTiles(int newSizeOfSquareSelectionTiles)
    {
        // Deactivating selected tiles before changing size of the array
        DeactivateAllHoveredTiles();

        sizeOfSquareSelectionTiles = newSizeOfSquareSelectionTiles;
        hoverTiles = new Vector3Int[newSizeOfSquareSelectionTiles * newSizeOfSquareSelectionTiles];
    }

    private void DeactivateAllHoveredTiles()
    {
        if (selectionMap == null) return;
        for (var i = 0; i < hoverTiles.Length; i++) selectionMap.SetTile(hoverTiles[i], null);
    }

    /**
     * Fill the tiles from selectionMap at hoverPosition, in a square area of sizeOfSquareSelectionTile size length
     * with selectionTileModel objects
     */ 
    private void SetHoveredTiles(Vector3 hoverPosition)
    {
        DeactivateAllHoveredTiles();
        
        Vector3 initialPosition = hoverPosition - (sizeOfSquareSelectionTiles - 1f) / 2f * new Vector3(grid.cellSize.x, grid.cellSize.y, 0f);

        initialPosition.z = 0f;

        for (var j = 0; j < sizeOfSquareSelectionTiles; j++)
        {
            for (var i = 0; i < sizeOfSquareSelectionTiles; i++)
            {
                Vector3 currentPosition = initialPosition + new Vector3(i * grid.cellSize.x, j * grid.cellSize.y, 0f);
                Vector3Int currentCell = selectionMap.WorldToCell(currentPosition);
                selectionMap.SetTile(currentCell, selectionTileModel);

                 hoverTiles[j * sizeOfSquareSelectionTiles + i] = currentCell;
            }
        }
    }

    [HideInInspector] public Vector3 mouseHover;
    private void Update()
    {
        mouseHover.z = 0f;
        SetHoveredTiles(mouseHover);
    }

    /// <summary>
    /// Tries to place at most hoverTiles.Length tiles in the world.
    /// Checks if the cells are occupied and if the player has enough blocks.
    /// </summary>
    /// <returns>The amount of blocks placed</returns>
    public int PlaceTilesAtHoverPosition(int totalAmountPossessed)
    {
        int amountPlaced = 0;
        for (int i = 0; i < hoverTiles.Length; i++)
        {
            Vector3 cellPosition = selectionMap.CellToWorld(hoverTiles[i]);

            if(totalAmountPossessed > amountPlaced)
            {
                if (cellPosition.x > transform.position.x && cellPosition.x + 1 < (transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x) && cellPosition.y > transform.position.y && cellPosition.y < (transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y - 1.5f))
                {
                    Collider2D collider2Ddetected = Physics2D.OverlapBox((Vector2)cellPosition + new Vector2(0.5f * grid.cellSize.x, 0.5f * grid.cellSize.y), new Vector2(grid.cellSize.x - 0.1f, grid.cellSize.y - 0.1f), 0f, placeTilesLayersMaskToCheck);
                    
                    if (collider2Ddetected == null)
                    {
                        amountPlaced += (GetBlockInfoAtPosition(cellPosition) != null) ? 0 : 1;
                        Vector3 position = selectionMap.CellToWorld(hoverTiles[i]);
                        if (networkObject == null) PlaceTileAtPosition( position, blockIndexPlayerPlaces);
                        else networkObject.SendRpc(RPC_PLACE_TILE_AT_POSITION, Receivers.All, position, blockIndexPlayerPlaces);
                    }
                }
            }
            else
            {
                break;
            }
            
        }
        return amountPlaced;
    }

    public int PlaceTilesAtPositions(int amountPossessed, Vector3Int[] positions)
    {
        int amountPlaced = 0;

        for (int i = 0; i < positions.Length; i++)
        {
            Vector3 cellPosition = selectionMap.CellToWorld(positions[i]);

            if (amountPossessed > amountPlaced
                && cellPosition.x > transform.position.x
                && cellPosition.y > transform.position.y
                && cellPosition.x + 1.0f < transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x
                && cellPosition.y + 1.5f < transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y)
            {
                Collider2D collider2Ddetected = Physics2D.OverlapBox(
                    (Vector2)cellPosition + new Vector2(0.5f * grid.cellSize.x, 0.5f * grid.cellSize.y),
                    new Vector2(grid.cellSize.x - 0.1f, grid.cellSize.y - 0.1f),
                    0.0f,
                    placeTilesLayersMaskToCheck);

                if (collider2Ddetected == null)
                {
                    amountPlaced += GetBlockInfoAtPosition(cellPosition) != null ? 0 : 1;

                    if (networkObject == null) PlaceTileAtPosition(selectionMap.CellToWorld(positions[i]), blockIndexPlayerPlaces);
                    else networkObject.SendRpc(RPC_PLACE_TILE_AT_POSITION, Receivers.All, selectionMap.CellToWorld(positions[i]), blockIndexPlayerPlaces);
                }
            }
            else break;
        }

        return amountPlaced;
    }

    public int CheckFreeTilesAtPositions(int amountPossessed, Vector3Int[] positions)
    {
        int freeTiles = 0;
        for (var i = 0; i < positions.Length; i++)
        {
            Vector3 cellPosition = selectionMap.CellToWorld(positions[i]);

            if (amountPossessed > freeTiles
                && cellPosition.x > transform.position.x
                && cellPosition.y > transform.position.y
                && cellPosition.x + 1.0f < transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x
                && cellPosition.y + 1.5f < transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y)
            {
                Collider2D collider2Ddetected = Physics2D.OverlapBox(
                    (Vector2) cellPosition + new Vector2(0.5f * grid.cellSize.x, 0.5f * grid.cellSize.y),
                    new Vector2(grid.cellSize.x - 0.1f, grid.cellSize.y - 0.1f),
                    0.0f,
                    placeTilesLayersMaskToCheck);
                
                if (collider2Ddetected == null)
                    freeTiles += GetBlockInfoAtPosition(cellPosition) != null ? 0 : 1;


            }
            else break;
        }

        return freeTiles;
    }

    /// <summary>
    ///returns the amount of tiles removed from the map
    /// </summary>
    public int RemoveTilesAtHoverPosition()
    {
        int amountRemoved = 0;
        for (int i = 0; i < hoverTiles.Length; i++)
        {
            Vector3 cellPosition = selectionMap.CellToWorld(hoverTiles[i]);

            amountRemoved += (GetBlockInfoAtPosition(cellPosition) != null) ? 1 : 0;

            if (cellPosition.x > transform.position.x
                && cellPosition.x + 1 < transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x
                && cellPosition.y > transform.position.y
                && cellPosition.y < transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y - 1.5f)
            {
                if (networkObject == null) RemoveTileAtPosition(selectionMap.CellToWorld(hoverTiles[i]));
                else networkObject.SendRpc(RPC_REMOVE_TILE_AT_POSITION, Receivers.All, selectionMap.CellToWorld(hoverTiles[i]));
            }
        }
        return amountRemoved;
    }


    [Space]
    //[Header("Mining Animation")]
    List<Vector3Int> miningTiles = new List<Vector3Int>(); //tiles currently being mined
    List<float> timeSinceStartedMining = new List<float>();
    [SerializeField] float scaleDurabilityToMiningTime = 0.02f;
    [SerializeField] Animator miningAnimationPrefab;
    List<Animator> miningAnimationsAvailableToUse = new List<Animator>();
    List<Animator> miningAnimationsCurrentlyBeingUsed = new List<Animator>();
    [SerializeField] int numberOfMiningAnimationsToSpawnAtGameStart = 25;
    [SerializeField] GameObject miningAnimationParent;

    public void ManageTilesBeingMined()
    {
        List<Vector3Int> tilesToRemoveFromListOfMiningTiles = new List<Vector3Int>();

        for (int i = 0; i < hoverTiles.Length; i++)
        {
            if (!miningTiles.Contains(hoverTiles[i]))
            {
                Vector3 cellPosition = selectionMap.CellToWorld(hoverTiles[i]);
                BlockInfo currentMiningTile = GetBlockInfoAtPosition(cellPosition);
                if(currentMiningTile != null && cellPosition.x > transform.position.x && 
                                                cellPosition.x + 1 < (transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x) && 
                                                cellPosition.y > transform.position.y && 
                                                cellPosition.y < (transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y - 1.5f))
                {
                    miningTiles.Add(hoverTiles[i]);
                    timeSinceStartedMining.Add(0f);
                    if(miningAnimationsAvailableToUse.Count == 0)
                    {
                        Animator newlyAddedAnimator = Instantiate(miningAnimationPrefab, Vector3.zero, Quaternion.identity);
                        miningAnimationsCurrentlyBeingUsed.Add(newlyAddedAnimator);
                    }
                    else
                    {
                        miningAnimationsCurrentlyBeingUsed.Add(miningAnimationsAvailableToUse[miningAnimationsAvailableToUse.Count - 1]);
                        miningAnimationsAvailableToUse.RemoveAt(miningAnimationsAvailableToUse.Count - 1);
                    }
                    miningAnimationsCurrentlyBeingUsed[miningAnimationsCurrentlyBeingUsed.Count - 1].gameObject.SetActive(true);
                    miningAnimationsCurrentlyBeingUsed[miningAnimationsCurrentlyBeingUsed.Count - 1].transform.position = cellPosition + new Vector3(0.5f * grid.cellSize.x, 0.5f * grid.cellSize.y, 0f);
                    miningAnimationsCurrentlyBeingUsed[miningAnimationsCurrentlyBeingUsed.Count - 1].Play("MiningAnimation");
                    miningAnimationsCurrentlyBeingUsed[miningAnimationsCurrentlyBeingUsed.Count - 1].speed = miningAnimationsCurrentlyBeingUsed[miningAnimationsCurrentlyBeingUsed.Count - 1].GetCurrentAnimatorClipInfo(0)[0].clip.length / (scaleDurabilityToMiningTime * currentMiningTile.durabilityStrength); 
                }
            }
        }

        for (int i = 0; i < miningTiles.Count; i++)
        {
            Vector3 cellPosition = selectionMap.CellToWorld(miningTiles[i]);
            BlockInfo currentMiningTile = GetBlockInfoAtPosition(cellPosition);

            if (currentMiningTile == null) tilesToRemoveFromListOfMiningTiles.Add(miningTiles[i]); 
            else
            {
                bool found = false;
                for (int j = 0; j < hoverTiles.Length; j++)
                {
                    if (miningTiles[i] == hoverTiles[j])
                    {
                        found = true;
                        break;
                    }
                }

                if (!found) tilesToRemoveFromListOfMiningTiles.Add(miningTiles[i]);
                else
                {
                    timeSinceStartedMining[i] += Time.deltaTime;

                    if(timeSinceStartedMining[i] >= scaleDurabilityToMiningTime * currentMiningTile.durabilityStrength)
                    {
                        //Finished mining this tile
                        tilesToRemoveFromListOfMiningTiles.Add(miningTiles[i]);

                        NetworkManager.Instance.InstantiateDropItem(currentMiningTile.dropIndex, cellPosition + new Vector3(0.5f * grid.cellSize.x, 0.5f * grid.cellSize.y, 0f), Quaternion.identity, true);
                        

                        if (networkObject == null) RemoveTileAtPosition(selectionMap.CellToWorld(miningTiles[i]));
                        else networkObject.SendRpc(RPC_REMOVE_TILE_AT_POSITION, Receivers.All, selectionMap.CellToWorld(miningTiles[i]));
                    }
                }
            }
        }
        //removing tiles that don't contain any block or that are no longer being hovered by the mining tool (that are not being hovered by the mouse)
        for (int i = 0; i < tilesToRemoveFromListOfMiningTiles.Count; i++)
        {
            int index = miningTiles.IndexOf(tilesToRemoveFromListOfMiningTiles[i]);
            //miningTiles.Remove(tilesToRemoveFromListOfMiningTiles[i]);
            miningTiles.RemoveAt(index);
            timeSinceStartedMining.RemoveAt(index);
            miningAnimationsAvailableToUse.Add(miningAnimationsCurrentlyBeingUsed[index]);
            miningAnimationsCurrentlyBeingUsed[index].gameObject.SetActive(false);
            miningAnimationsCurrentlyBeingUsed.RemoveAt(index);
        }

    }

    public void ResetTilesBeingMined()
    {
        miningTiles.Clear();
        timeSinceStartedMining.Clear();
        for (int i = 0; i < miningAnimationsCurrentlyBeingUsed.Count; i++)
        {
            miningAnimationsAvailableToUse.Add(miningAnimationsCurrentlyBeingUsed[i]);
            miningAnimationsCurrentlyBeingUsed[i].gameObject.SetActive(false);
        }
        miningAnimationsCurrentlyBeingUsed.Clear();
    }

    void GenerateMapAndFillTileMap()
    {
        mapGenerator.GenerateMap();

        FillTileMap();
    }

    int backgroundOrderInLayer = 0;
    int frontgroundOrderInLayer = 0;
    [Space]
    [Range(0, 1)] [SerializeField] float backgroundAlpha = .5f;
    [Range(0, 1)] [SerializeField] float frontgroundAlpha = .5f;
    void FillTileMap()
    {
        highlightMap.ClearAllTiles();
        backgroundMap.ClearAllTiles();

        for (int i = 0; i < mapGenerator.map.GetLength(0); i++)
        {
            for (int j = 0; j < mapGenerator.map.GetLength(1); j++)
            {
                
                Vector3 currentCellPosition = transform.position + new Vector3(i * grid.cellSize.x, j * grid.cellSize.y, 0);
                currentCellPosition.z = 0;
                Vector3Int currentCell = highlightMap.WorldToCell(currentCellPosition);

                if (mapGenerator.map[i, j] != 0)
                {
                    if (mapGenerator.map[i, j] < 100) highlightMap.SetTile(currentCell, blocksInfo[mapGenerator.map[i, j] - 1].tile);
                    else
                    {
                        highlightMap.SetTile(currentCell, resourcesInfo[mapGenerator.map[i, j] % 100].tile);
                    }

                    if(mapGenerator.map[i, j] < 100)
                    {
                        // Background tiles
                        if (blocksInfo[mapGenerator.map[i, j] - 1].backgroundGameObjects?.Length > 0)
                        {
                            if (UnityEngine.Random.Range(0f, 1f) < blocksInfo[mapGenerator.map[i, j] - 1].probabilityToPlaceBackgroundBlock)
                            {
                                int selectedIndexOfBackgroundBlock = UnityEngine.Random.Range(0, blocksInfo[mapGenerator.map[i, j] - 1].backgroundGameObjects.Length);
                                GameObject backgrountImage = Instantiate(blocksInfo[mapGenerator.map[i, j] - 1].backgroundGameObjects[selectedIndexOfBackgroundBlock], currentCellPosition, Quaternion.identity);
                                SpriteRenderer backgroundImageSpriteRenderer = backgrountImage.GetComponent<SpriteRenderer>();
                                Color backgroundImageColor = backgroundImageSpriteRenderer.color;
                                backgroundImageSpriteRenderer.color = new Color(backgroundImageColor.r, backgroundImageColor.g, backgroundImageColor.b, backgroundAlpha);
                                backgroundImageSpriteRenderer.sortingOrder = backgroundOrderInLayer;
                                backgroundOrderInLayer++;
                                backgrountImage.transform.SetParent(backgroundMap.transform);
                            }
                        }

                        // Frontground tiles
                        if (blocksInfo[mapGenerator.map[i, j] - 1].frontgroundGameObjects?.Length > 0)
                        {
                            if (UnityEngine.Random.Range(0f, 1f) < blocksInfo[mapGenerator.map[i, j] - 1].probabilityToPlaceFrontgroundBlock)
                            {
                                int selectedIndexOfFrontgroundBlock = UnityEngine.Random.Range(0, blocksInfo[mapGenerator.map[i, j] - 1].frontgroundGameObjects.Length);
                                GameObject frontgrountImage = Instantiate(blocksInfo[mapGenerator.map[i, j] - 1].frontgroundGameObjects[selectedIndexOfFrontgroundBlock], currentCellPosition, Quaternion.identity);
                                SpriteRenderer frontgroundImageSpriteRenderer = frontgrountImage.GetComponent<SpriteRenderer>();
                                Color frontgroundImageColor = frontgroundImageSpriteRenderer.color;
                                frontgroundImageSpriteRenderer.color = new Color(frontgroundImageColor.r, frontgroundImageColor.g, frontgroundImageColor.b, frontgroundAlpha);
                                frontgroundImageSpriteRenderer.sortingOrder = frontgroundOrderInLayer;
                                frontgroundOrderInLayer++;
                                //frontgrountImage.transform.SetParent(backgroundMap.transform);
                            }
                        }

                    }
                    else
                    {
                        //Background tiles for resources veins
                        if (resourcesInfo[mapGenerator.map[i, j] % 100].backgroundGameObjects?.Length > 0/*blocksInfo[mapGenerator.map[i, j] - 1].backgroundBlocks.Length > 0*/)
                        {
                            if (UnityEngine.Random.Range(0f, 1f) < resourcesInfo[mapGenerator.map[i, j] % 100].probabilityToPlaceBackgroundBlock)
                            {
                                int selectedIndexOfBackgroundBlock = UnityEngine.Random.Range(0, resourcesInfo[mapGenerator.map[i, j] % 100].backgroundGameObjects.Length);
                                GameObject backgrountImage = Instantiate(resourcesInfo[mapGenerator.map[i, j] % 100].backgroundGameObjects[selectedIndexOfBackgroundBlock], currentCellPosition, Quaternion.identity);
                                SpriteRenderer backgroundImageSpriteRenderer = backgrountImage.GetComponent<SpriteRenderer>();
                                Color backgroundImageColor = backgroundImageSpriteRenderer.color;
                                backgroundImageSpriteRenderer.color = new Color(backgroundImageColor.r, backgroundImageColor.g, backgroundImageColor.b, backgroundAlpha);
                                backgroundImageSpriteRenderer.sortingOrder = backgroundOrderInLayer;
                                backgroundOrderInLayer++;
                                backgrountImage.transform.SetParent(backgroundMap.transform);
                            }
                        }
                    }
                    
                }
                else
                {
                    if (backgroundTilesForEmptySpaces.Length > 0)
                    {
                        int tileIndexToPlace = UnityEngine.Random.Range(0, UnityEngine.Random.Range(0, backgroundTilesForEmptySpaces.Length));

                        if (UnityEngine.Random.Range(0f, 1f) < backgroundTilesForEmptySpaces[tileIndexToPlace].probabilityToPlace)
                        {
                            GameObject gameObject = Instantiate(backgroundTilesForEmptySpaces[tileIndexToPlace].gameObjectPrefab, currentCellPosition, Quaternion.identity);
                            gameObject.transform.SetParent(backgroundMap.transform);
                            //backgroundMap.SetTile(currentCell, backgroundTilesForEmptySpaces[tileIndexToPlace].tile);
                            SpriteRenderer backgroundImageSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                            Color backgroundImageColor = backgroundImageSpriteRenderer.color;
                            backgroundImageSpriteRenderer.color = new Color(backgroundImageColor.r, backgroundImageColor.g, backgroundImageColor.b, backgroundAlpha);
                            backgroundImageSpriteRenderer.sortingOrder = backgroundOrderInLayer;
                            backgroundOrderInLayer++;
                        }
                    }
                }



                //Global background tiles
                if (globalBackgroundTiles.Length > 0)
                {
                    int tileIndexToPlace = UnityEngine.Random.Range(0, UnityEngine.Random.Range(0, globalBackgroundTiles.Length));

                    if (UnityEngine.Random.Range(0f, 1f) < globalBackgroundTiles[tileIndexToPlace].probabilityToPlace)
                    {
                        GameObject gameObject = Instantiate(globalBackgroundTiles[tileIndexToPlace].gameObjectPrefab, currentCellPosition, Quaternion.identity);
                        gameObject.transform.SetParent(backgroundMap.transform);
                        //backgroundMap.SetTile(currentCell, globalBackgroundTiles[tileIndexToPlace].tile);
                        SpriteRenderer backgroundImageSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                        Color backgroundImageColor = backgroundImageSpriteRenderer.color;
                        backgroundImageSpriteRenderer.color = new Color(backgroundImageColor.r, backgroundImageColor.g, backgroundImageColor.b, backgroundAlpha);
                        backgroundImageSpriteRenderer.sortingOrder = backgroundOrderInLayer;
                        backgroundOrderInLayer++;

                    }
                }


                if (globalFarBackgroundTiles.Length > 0)
                {
                    int tileIndexToPlace = UnityEngine.Random.Range(0, UnityEngine.Random.Range(0, globalFarBackgroundTiles.Length));

                    if (UnityEngine.Random.Range(0f, 1f) < globalFarBackgroundTiles[tileIndexToPlace].probabilityToPlace)
                    {
                        GameObject gameObject = Instantiate(globalFarBackgroundTiles[tileIndexToPlace].gameObjectPrefab, currentCellPosition, Quaternion.identity);
                        gameObject.transform.SetParent(farBackgroundMap.transform);
                        //farBackgroundMap.SetTile(currentCell, globalFarBackgroundTiles[tileIndexToPlace].tile);
                        SpriteRenderer backgroundImageSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                        Color backgroundImageColor = backgroundImageSpriteRenderer.color;
                        //backgroundImageSpriteRenderer.color = new Color(backgroundImageColor.r, backgroundImageColor.g, backgroundImageColor.b, backgroundAlpha);
                        backgroundImageSpriteRenderer.sortingOrder = backgroundOrderInLayer;
                        backgroundOrderInLayer++;
                    }
                }
                //else highlightMap.RefreshTile(currentCell);
            }
        }

        minimapUncover.CoverMinimap();
        //RemoveTileAtPosition_(transform.position);
        //Vector3 point_ = new Vector3(0.1f, 0.1f, 0f);
        //networkObject.SendRpc(RPC_REMOVE_TILE_AT_POSITION, Receivers.All, transform.position + new Vector3(0.1f, 0.1f, 0));
    }

    void FillTilesWithCondition()
    {
        for (int i = 0; i < mapGenerator.map.GetLength(0); i++)
        {
            for (int j = 0; j < mapGenerator.map.GetLength(1); j++)
            {
                if(mapGenerator.map[i, j] != 0)
                {
                    Vector3 currentCellPosition = transform.position + new Vector3(i * grid.cellSize.x, j * grid.cellSize.y, 0);
                    //Debug.Log("currentCellPosition.z: " + currentCellPosition.z);
                    currentCellPosition.z = 0;
                    Vector3Int currentCell = highlightMap.WorldToCell(currentCellPosition);

                    BlockInfo currentBlockInfo = GetBlockInfoAtPosition(currentCellPosition);

                    BlockInfo upperBlockInfo = GetBlockInfoAtPosition(currentCellPosition + new Vector3(0f, grid.cellSize.y, 0f));
                    BlockInfo underBlockInfo = GetBlockInfoAtPosition(currentCellPosition - new Vector3(0f, grid.cellSize.y, 0f));

                    if (i > 0 && j > 0 && currentBlockInfo != null && underBlockInfo == null && currentBlockInfo.ceilingGameObjects?.Length > 0)
                    {

                        if (UnityEngine.Random.Range(0f, 1f) < currentBlockInfo.probabilityToPlaceCeilingBlock)
                        {
                            int selectedIndexOfCeilingBlock = UnityEngine.Random.Range(0, currentBlockInfo.ceilingGameObjects.Length);
                            GameObject ceilingImage = Instantiate(currentBlockInfo.ceilingGameObjects[selectedIndexOfCeilingBlock], currentCellPosition, Quaternion.identity);
                            SpriteRenderer ceilingImageSpriteRenderer = ceilingImage.GetComponentInChildren<SpriteRenderer>();
                            Color ceilingImageColor = ceilingImageSpriteRenderer.color;
                            ceilingImageSpriteRenderer.color = new Color(ceilingImageColor.r, ceilingImageColor.g, ceilingImageColor.b, frontgroundAlpha);
                            ceilingImageSpriteRenderer.sortingOrder = frontgroundOrderInLayer;
                            frontgroundOrderInLayer++;
                            ceilingImage.transform.SetParent(frontgroundMapWithoutParallax.transform);
                        }
                    }
                }
            }
        }
    }

    public override void RemoveTileAtPosition(RpcArgs args)
        => RemoveTileAtPosition(args.GetNext<Vector3>());

    public void RemoveTileAtPosition(Vector3 position)
    {
        Vector3Int currentCell = highlightMap.WorldToCell(position);

        Vector3 cellPosition = selectionMap.CellToWorld(currentCell);
        BlockInfo currentMiningTile = GetBlockInfoAtPosition(cellPosition);
        if (currentMiningTile != null && cellPosition.x > transform.position.x &&
                                        cellPosition.x + 1 < (transform.position.x + mapGenerator.map.GetLength(0) * grid.cellSize.x) &&
                                        cellPosition.y > transform.position.y &&
                                        cellPosition.y < (transform.position.y + mapGenerator.map.GetLength(1) * grid.cellSize.y - 1.5f))
        {
            highlightMap.SetTile(currentCell, null);
        }
        
    }

    public override void PlaceTileAtPosition(RpcArgs args)
        => PlaceTileAtPosition(args.GetNext<Vector3>(), args.GetNext<int>());

    void PlaceTileAtPosition(Vector3 position, int indexOfTileToBePlaced)
    {
        //position.z = 0;
        Vector3Int currentCell = highlightMap.WorldToCell(position);

        if (highlightMap.GetTile(currentCell) == null)
        {
            highlightMap.SetTile(currentCell, blocksInfo[indexOfTileToBePlaced].tile);
            //highlightMap.SetTile(currentCell, blocksPlayerCanPlace[blockIndexPlayerPlaces]);
        }
    }
    
    // Converts an Object, to a byte[]
    public static byte[] ObjectToByteArray(int[,] obj)
    {
        if (obj == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);

        return ms.ToArray();
    }

    // Converts a byte[] to an Object<T>
    public static T ByteArrayToObject<T>(byte[] arrBytes)
    {
        MemoryStream ms = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();
        ms.Write(arrBytes, 0, arrBytes.Length);
        ms.Seek(0, SeekOrigin.Begin);
        T obj = (T)bf.Deserialize(ms);

        return obj;
    }
    
    public override void RetrieveGeneratedMap(RpcArgs args)
    {
        // This line below should be in pretty much every
        // RPC that takes custom-data types.
        byte[] receivedBytes = args.GetNext<byte[]>();

        // We convert receivedBytes to the original object of data-type <Day>
        int[,] retrievedMap = ByteArrayToObject<int[,]>(receivedBytes);

        DrawGeneratedMap(retrievedMap);
    }

    private void DrawGeneratedMap(int[,] generatedMap)
    {
        mapGenerator.map = new int[generatedMap.GetLength(0), generatedMap.GetLength(1)];

        for (int i = 0; i < generatedMap.GetLength(0); i++)
        {
            for (int j = 0; j < generatedMap.GetLength(1); j++)
            {
                mapGenerator.map[i, j] = generatedMap[i, j];
            }
        }

        SetMinimapSettings(mapGenerator.map.GetLength(0), mapGenerator.map.GetLength(1));

        alreadyGotMapFromServer = true;

        FillMapAfterDrawing();
        alreadyFilledMap = true;
    }

    void FillMapAfterDrawing()
    {
        FillTileMap();
        //FillTilesWithCondition();
        float boxSpawnSize = 10f;
        Vector3 positionToSpawn = transform.position + new Vector3(
                                                    Random.Range(0.1f + ((boxSpawnSize / 2) + 1) * grid.cellSize.x, -0.1f + (mapGenerator.map.GetLength(0) - (boxSpawnSize / 2)) * grid.cellSize.x),
                                                    Random.Range(0.1f + ((boxSpawnSize / 2) + 1) * grid.cellSize.y, -0.1f + (mapGenerator.map.GetLength(1) - (boxSpawnSize / 2)) * grid.cellSize.y),
                                                    0f);
        
        for (int i = 0; i < boxSpawnSize; i++)
        {
            for (int j = 0; j < boxSpawnSize; j++)
            {
                //Vector3Int currentCell = highlightMap.WorldToCell(positionToSpawn 
                //                                            - new Vector3((boxSpawnSize / 2) * grid.cellSize.x, (boxSpawnSize / 2) * grid.cellSize.y, 0f)
                //                                            + new Vector3(i * grid.cellSize.x, j * grid.cellSize.y, 0f));
                Vector3 point = positionToSpawn - new Vector3((boxSpawnSize / 2) * grid.cellSize.x, (boxSpawnSize / 2) * grid.cellSize.y, 0f)
                                                + new Vector3(i * grid.cellSize.x, j * grid.cellSize.y, 0f);

                if (networkObject == null) RemoveTileAtPosition(point);
                else networkObject.SendRpc(RPC_REMOVE_TILE_AT_POSITION, Receivers.All, point);

                //highlightMap.SetTile(currentCell, null);
            }
        }


        Vector2[] mapBorders = new Vector2[4];
        mapBorders[0] = transform.position + new Vector3(0f, grid.cellSize.y * mapGenerator.map.GetLength(1), 0f);
        mapBorders[1] = transform.position; 
        mapBorders[2] = transform.position + new Vector3(grid.cellSize.x * mapGenerator.map.GetLength(0), 0f, 0f); 
        mapBorders[3] = transform.position + new Vector3(grid.cellSize.x * mapGenerator.map.GetLength(0), grid.cellSize.y * mapGenerator.map.GetLength(1), 0f); 
        cinemachineConfinner.SetPath(0, mapBorders);

        //place floor for player to fall onto
        Vector3 floorPoint;

        for (int i = 0; i < 4; i++)
        {
            floorPoint = positionToSpawn + new Vector3((-2.2f + (float)i) * grid.cellSize.x, (-1.7f) * grid.cellSize.y, 0f);
            if (networkObject == null) PlaceTileAtPosition(floorPoint, 0);
            else networkObject.SendRpc(RPC_PLACE_TILE_AT_POSITION, Receivers.All, floorPoint, 0);
        }

        //floorPoint = positionToSpawn + new Vector3((-0.4f) * grid.cellSize.x, (-1.6f) * grid.cellSize.y, 0f);
        //if (networkObject == null) PlaceTileAtPosition(floorPoint, 0);
        //else networkObject.SendRpc(RPC_PLACE_TILE_AT_POSITION, Receivers.All, floorPoint, 0);

        //floorPoint = positionToSpawn + new Vector3((0.6f) * grid.cellSize.x, (-1.6f) * grid.cellSize.y, 0f);
        //if (networkObject == null) PlaceTileAtPosition(floorPoint, 0);
        //else networkObject.SendRpc(RPC_PLACE_TILE_AT_POSITION, Receivers.All, floorPoint, 0);


        GameManagerNetwork.gameManagerNetwork.SpawnPlayer(positionToSpawn);

        FillTilesWithCondition();
    }

    public BlockInfo GetBlockInfoAtPosition(Vector3 position)
    {
        TileBase tileAtPosition = highlightMap.GetTile(highlightMap.WorldToCell(position));

        var index = -1;
        var isResourceBlock = false;

        for (var i = 0; i < blocksInfo.Length; i++)
        {
            if(tileAtPosition == blocksInfo[i].tile)
            {
                index = i;
                break;
            }
        }

        if(index == -1)
        {
            for (var i = 0; i < resourcesInfo.Length; i++)
            {
                if (tileAtPosition != resourcesInfo[i].tile) continue;
                index = i;
                isResourceBlock = true;
                break;
            }
        }

        if (index != -1) return isResourceBlock ? resourcesInfo[index] : blocksInfo[index];
        else return null;
    }

    public TileBase GetTileAtPosition(Vector3 position)
    {
        TileBase tileAtPosition = highlightMap.GetTile(highlightMap.WorldToCell(position));

        return tileAtPosition;
    }

    void SetMinimapSettings(int mapWidth, int mapHeight)
    {
        minimapCamera.orthographicSize = mapWidth * 0.5f;
        minimapCamera.aspect = ((float)mapWidth) / ((float)mapHeight);
        minimapCamera.transform.position = new Vector3(mapWidth * grid.cellSize.x * 0.5f, 
                                                        mapHeight * grid.cellSize.y * 0.5f, -1);

        minimapBordersRectTransform.sizeDelta = new Vector2(
                                            minimapBordersRectTransform.sizeDelta.x, 
                                            minimapBordersRectTransform.sizeDelta.x * ((float)mapHeight) / ((float)mapWidth));

        minimapImageRectTransform.sizeDelta = new Vector2(
                                            minimapImageRectTransform.sizeDelta.x,
                                            minimapImageRectTransform.sizeDelta.x /** ((float)mapHeight) / ((float)mapWidth)*/);

    }
}

[System.Serializable]
public class BlockInfo
{
    [Space]
    [Header("BlockInfo")]
    public TileBase tile;
    public int durabilityStrength = 50;
    [Range(0, 1)] public float probabilityToPlaceBackgroundBlock = 0.1f;
    public TileBase[] backgroundBlocks;
    public GameObject[] backgroundGameObjects;
    [Range(0, 1)] public float probabilityToPlaceFrontgroundBlock = 0.1f;
    public GameObject[] frontgroundGameObjects;
    [Space]
    //[Header("Tiles with condition")]
    [Range(0, 1)] public float probabilityToPlaceCeilingBlock = 0.1f;
    public GameObject[] ceilingGameObjects;
    public int dropIndex = 0;   //index used to know which of the elements in NetworkManager
                                //will be spawned on the network
}

[System.Serializable]
public class GlobalFrontgroundTile
{
    [Space]
    public TileBase tile;
    public GameObject gameObjectPrefab;
    [Range(0f, 1f)] public float probabilityToPlace = 0.01f;
}

[System.Serializable]
public class GlobalBackgroundTile
{
    [Space]
    public TileBase tile;
    public GameObject gameObjectPrefab;
    [Range(0f, 1f)] public float probabilityToPlace = 0.01f;
}
