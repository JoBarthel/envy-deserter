﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace EnvyDeserter
{
    public class Input
    {
        private static Input instance;

        public static Input Instance
        {
            get
            {
                if (instance != null) return instance;
                instance = new Input();
                return instance;
            }
        }

        public InputMaster InputMaster { get; private set; }

        public Input()
        {
            InputMaster = new InputMaster();
        }

        public void GetTargetedPoint()
        {
            if( Gamepad.current != null && Gamepad.current.enabled )
                Debug.Log("gamepad");
            else if(Keyboard.current.enabled)
                Debug.LogError("mouse");
        }
    }
}