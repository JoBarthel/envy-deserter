﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [SerializeField] Transform target;
    [Range(0f, 1f)] [SerializeField] float interpolationFactor = 0.5f;


    float x, y;
    private void Start()
    {
        transform.SetParent(null);
    }

    private void Update()
    {
        x = Mathf.Lerp(transform.position.x, target.position.x, interpolationFactor);
        y = Mathf.Lerp(transform.position.y, target.position.y, interpolationFactor);

        transform.position = new Vector3(x, y, transform.position.z);
    }
}
