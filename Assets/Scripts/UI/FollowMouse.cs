﻿using System;
using Assets.Scripts.Character;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    private bool _controllerIsSet;
    private Controller _controller;
    public void SetController(Controller controller)
    {
        _controllerIsSet = true;
        _controller = controller;
    }

    private void Update()
    {
        if(!_controllerIsSet) return;

        transform.position = _controller.GetCursorPositionInScreenSpace();
    }
}
