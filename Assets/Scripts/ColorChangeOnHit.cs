﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChangeOnHit : MonoBehaviour, IDamageable
{
    protected new SpriteRenderer renderer;
    protected new Image image;

    protected Color originalColor;
    [SerializeField] protected Color hurtColor = Color.red;
    [SerializeField] protected float colorChangeDuration = 0.5f;

    [SerializeField] bool isUiElement = false;

    bool hasSpriteOrImage = false;

    private Coroutine currentCoroutine;

    private void Awake()
    {
        if (!isUiElement)
        {
            renderer = GetComponent<SpriteRenderer>();
        }
        else
        {
            image = GetComponent<Image>();
        }

        hasSpriteOrImage = (!isUiElement) ? (renderer != null) : (image != null);

        if (!hasSpriteOrImage)
        {
            if (!isUiElement)
            {
                Debug.LogWarning(name + " has a ColorChangeOnHit component but no SpriteRenderer and isn't marked as Ui element.");
            }
            else
            {
                Debug.LogWarning(name + " has a ColorChangeOnHit component but no Image and is marked as Ui element.");
            }
        }            
        else
            originalColor = (!isUiElement) ? renderer.color : image.color;
    }

    public void TakeDamage(int damage)
    {
        if (currentCoroutine != null) StopCoroutine(currentCoroutine);
        currentCoroutine = StartCoroutine(ColorFadeCoroutine(hurtColor, colorChangeDuration));
    }

    public void Die() {}

    protected IEnumerator ColorFadeCoroutine(Color color, float duration)
    {
        var time = 0.0f;

        while (time < duration)
        {
            var newColor = color * (1.0f - time / duration) + originalColor * time / duration;

            if (!isUiElement) renderer.color = new Color(newColor.r, newColor.g, newColor.b, renderer.color.a);
            else image.color = new Color(newColor.r, newColor.g, newColor.b, image.color.a);

            yield return new WaitForSeconds(Time.deltaTime);
            time += Time.deltaTime;
        }

        if(!isUiElement) renderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, renderer.color.a);
        else image.color = new Color(originalColor.r, originalColor.g, originalColor.b, image.color.a);
    }
}
