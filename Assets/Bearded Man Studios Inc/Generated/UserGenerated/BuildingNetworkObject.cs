using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0,0,0]")]
	public partial class BuildingNetworkObject : NetworkObject
	{
		public const int IDENTITY = 2;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector2 _position;
		public event FieldEvent<Vector2> positionChanged;
		public InterpolateVector2 positionInterpolation = new InterpolateVector2() { LerpT = 0f, Enabled = false };
		public Vector2 position
		{
			get { return _position; }
			set
			{
				// Don't do anything if the value is the same
				if (_position == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_position = value;
				hasDirtyFields = true;
			}
		}

		public void SetpositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_position(ulong timestep)
		{
			if (positionChanged != null) positionChanged(_position, timestep);
			if (fieldAltered != null) fieldAltered("position", _position, timestep);
		}
		[ForgeGeneratedField]
		private byte _team;
		public event FieldEvent<byte> teamChanged;
		public Interpolated<byte> teamInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte team
		{
			get { return _team; }
			set
			{
				// Don't do anything if the value is the same
				if (_team == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_team = value;
				hasDirtyFields = true;
			}
		}

		public void SetteamDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_team(ulong timestep)
		{
			if (teamChanged != null) teamChanged(_team, timestep);
			if (fieldAltered != null) fieldAltered("team", _team, timestep);
		}
		[ForgeGeneratedField]
		private bool _activated;
		public event FieldEvent<bool> activatedChanged;
		public Interpolated<bool> activatedInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool activated
		{
			get { return _activated; }
			set
			{
				// Don't do anything if the value is the same
				if (_activated == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_activated = value;
				hasDirtyFields = true;
			}
		}

		public void SetactivatedDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_activated(ulong timestep)
		{
			if (activatedChanged != null) activatedChanged(_activated, timestep);
			if (fieldAltered != null) fieldAltered("activated", _activated, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			positionInterpolation.current = positionInterpolation.target;
			teamInterpolation.current = teamInterpolation.target;
			activatedInterpolation.current = activatedInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _position);
			UnityObjectMapper.Instance.MapBytes(data, _team);
			UnityObjectMapper.Instance.MapBytes(data, _activated);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_position = UnityObjectMapper.Instance.Map<Vector2>(payload);
			positionInterpolation.current = _position;
			positionInterpolation.target = _position;
			RunChange_position(timestep);
			_team = UnityObjectMapper.Instance.Map<byte>(payload);
			teamInterpolation.current = _team;
			teamInterpolation.target = _team;
			RunChange_team(timestep);
			_activated = UnityObjectMapper.Instance.Map<bool>(payload);
			activatedInterpolation.current = _activated;
			activatedInterpolation.target = _activated;
			RunChange_activated(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _position);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _team);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _activated);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (positionInterpolation.Enabled)
				{
					positionInterpolation.target = UnityObjectMapper.Instance.Map<Vector2>(data);
					positionInterpolation.Timestep = timestep;
				}
				else
				{
					_position = UnityObjectMapper.Instance.Map<Vector2>(data);
					RunChange_position(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (teamInterpolation.Enabled)
				{
					teamInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					teamInterpolation.Timestep = timestep;
				}
				else
				{
					_team = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_team(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (activatedInterpolation.Enabled)
				{
					activatedInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					activatedInterpolation.Timestep = timestep;
				}
				else
				{
					_activated = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_activated(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (positionInterpolation.Enabled && !positionInterpolation.current.UnityNear(positionInterpolation.target, 0.0015f))
			{
				_position = (Vector2)positionInterpolation.Interpolate();
				//RunChange_position(positionInterpolation.Timestep);
			}
			if (teamInterpolation.Enabled && !teamInterpolation.current.UnityNear(teamInterpolation.target, 0.0015f))
			{
				_team = (byte)teamInterpolation.Interpolate();
				//RunChange_team(teamInterpolation.Timestep);
			}
			if (activatedInterpolation.Enabled && !activatedInterpolation.current.UnityNear(activatedInterpolation.target, 0.0015f))
			{
				_activated = (bool)activatedInterpolation.Interpolate();
				//RunChange_activated(activatedInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public BuildingNetworkObject() : base() { Initialize(); }
		public BuildingNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public BuildingNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
