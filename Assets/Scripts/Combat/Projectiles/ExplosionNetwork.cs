﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class ExplosionNetwork : ExplosionBehavior
{
    public override void Explode(RpcArgs args)
    {
        var position = args.GetNext<Vector2>();
        var radius = args.GetNext<int>();

        for (var i = -radius; i <= radius; i++)
            for (var j = -radius; j <= radius; j++)
                if (i * i + j * j <= radius * radius)
                    MapManager.Instance.RemoveTileAtPosition(position + i * Vector2.up + j * Vector2.down);
    }
}
