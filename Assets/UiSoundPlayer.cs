﻿using UnityEngine;

public class UiSoundPlayer : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip errorSound;
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayErrorSound()
    {
        _audioSource.PlayOneShot(errorSound);
    }
}
