﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform), typeof(LineRenderer))]
public class PieMenu : MonoBehaviour
{
    [SerializeField] private KeyCode activationKey;
    public KeyCode ActivationKey => activationKey;

    [SerializeField] private AnimationCurve popCurve;
    private float currentPopTime;
    private bool activated;

    private float diameter;

    [SerializeField] [Tooltip("Space between buttons (degrees)")] private float intersectionAngle = 10.0f;
    private float buttonAngle;

    private LineRenderer lineRenderer;

    private PieController pieController;
    private PieButton[] buttons; // 0 is the center button
    private PieButton centerButton;

    private int selection;

    private RectTransform rectTransform;
    private Vector2 centerPosition;

    private const float ICON_OFFSET_FROM_CENTER = .75f;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        pieController = GetComponentInParent<PieController>();
        lineRenderer = GetComponent<LineRenderer>();
        buttons = GetComponentsInChildren<PieButton>();

        lineRenderer.positionCount = 2;
        buttonAngle = 360.0f / buttons.Length;
        diameter = rectTransform.sizeDelta.y;

        currentPopTime = popCurve[0].time;
        rectTransform.localScale =  popCurve.Evaluate(currentPopTime) * Vector3.one;

        SelectAndHide();
    }

    private void Start()
    {
        for (var i = 0; i < buttons.Length; i++)
        {
            buttons[i].selected = false;

            var bTransform = buttons[i].RectTransform;
            bTransform.position -= bTransform.position.z * Vector3.forward;
            bTransform.sizeDelta = diameter * Vector2.one;
            bTransform.rotation = Quaternion.Euler(0.0f, 0.0f, (i + 0.5f) * buttonAngle - intersectionAngle / 2.0f);

            var bImage = buttons[i].ButtonImage;
            bImage.type = Image.Type.Filled;
            bImage.fillMethod = Image.FillMethod.Radial360;
            bImage.fillOrigin = 2; // Top
            bImage.fillAmount = 1.0f / buttons.Length - intersectionAngle / 360.0f;
            bImage.fillClockwise = true;

            if (buttons[i].Child == null) continue;

            var position = Quaternion.Euler(0.0f, 0.0f, (intersectionAngle - buttonAngle) / 2.0f)
                       * (ICON_OFFSET_FROM_CENTER * buttons[i].RectTransform.sizeDelta.y * Vector2.up);
            buttons[i].Child.anchoredPosition = position; 
            buttons[i].Child.rotation = Quaternion.identity;
        }

        gameObject.SetActive(false);
    }

    private void Update()
    {
        currentPopTime += activated ? Time.deltaTime : -Time.deltaTime;
        if (currentPopTime > popCurve[popCurve.length - 1].time) currentPopTime = popCurve[popCurve.length - 1].time;
        if (currentPopTime < popCurve[0].time) currentPopTime = popCurve[0].time;

        rectTransform.localScale = popCurve.Evaluate(currentPopTime) * Vector3.one;
        
        if (activated) GetSelection();
        else if (currentPopTime <= popCurve[0].time) gameObject.SetActive(false);
    }

    public void Show(State interactionMode)
    {
        gameObject.SetActive(true);
        
        centerPosition = pieController.IsUsingController
            ? pieController.ScreenWorldCenter
            : pieController.CursorPosition;

        rectTransform.position = new Vector3(centerPosition.x, centerPosition.y, rectTransform.position.z);

        lineRenderer.SetPosition(0, centerPosition);
        lineRenderer.SetPosition(1, centerPosition);

        lineRenderer.enabled = true;
        activated = true;
    }

    public void SelectAndHide()
    {
        if (selection >= 0 && selection < buttons.Length) buttons[selection].Activate();
        
        lineRenderer.enabled = false;
        activated = false;
    }

    private void GetSelection()
    {
        var worldRadius = diameter/2.0f;
        
        var cursor = pieController.CursorPosition;
        if (pieController.IsUsingController) cursor *= worldRadius;
        else cursor -= centerPosition;

        lineRenderer.SetPosition(1, centerPosition + cursor);

        if (selection >= 0 && selection < buttons.Length) buttons[selection].selected = false;

        if (cursor.magnitude < worldRadius * 0.5f)
        {
            selection = -1;
            return;
        }

        var angle = Vector2.SignedAngle(Vector2.up, cursor) + buttonAngle / 2.0f;
        if (angle < 0.0f) angle += 360.0f;
        if (angle > 360.0f) angle -= 360.0f;

        selection = Mathf.FloorToInt(angle / 360.0f * buttons.Length);

        buttons[selection].selected = true;
    }
}