﻿using System.Collections;
using UnityEngine;

internal interface IDamageable
{
    void TakeDamage(int damage);

    void Die();
}