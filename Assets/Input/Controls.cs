// GENERATED AUTOMATICALLY FROM 'Assets/Input/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Basic Controls"",
            ""id"": ""7cd09f39-5a1c-4781-a3a0-e1f74aaa1a23"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""e3439771-ae83-48ca-ad8b-1ae261c883af"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""311e15ef-3dc8-4aca-a751-7a81d6cacf8e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Climb"",
                    ""type"": ""Value"",
                    ""id"": ""6d6830af-b1df-4636-8d6b-bae85959d166"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""8af269ca-eae0-4a54-aaae-cfeb4c01350d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StopFire"",
                    ""type"": ""Button"",
                    ""id"": ""ef315b6b-dfee-4d45-bab0-8e86ab850e6b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AltFire"",
                    ""type"": ""Button"",
                    ""id"": ""792653bc-fac5-4aa0-9f78-0c0b7e2d8008"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StartGrapplingHook"",
                    ""type"": ""Button"",
                    ""id"": ""0d076167-f0ef-4c02-9eee-5e5983a242cc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StopGrapplingHook"",
                    ""type"": ""Button"",
                    ""id"": ""b961e67b-19ec-468a-b24c-89f4f59e7a28"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OpenMenu"",
                    ""type"": ""Button"",
                    ""id"": ""90ac890f-b7f5-4b8f-b3ff-cb6074ea6896"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectDig"",
                    ""type"": ""Button"",
                    ""id"": ""5972f0cd-2115-4d2d-b036-4a018e637445"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectFire"",
                    ""type"": ""Button"",
                    ""id"": ""82b00945-aa31-403d-9f4a-5d37c8db9b3f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SelectConstruction"",
                    ""type"": ""Button"",
                    ""id"": ""131b027f-08e9-4c7e-803d-c5d71f79b6f7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ScrollMode"",
                    ""type"": ""Value"",
                    ""id"": ""7efdfc0e-074d-4eec-8d86-b5b37ef0f6df"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CursorPosition"",
                    ""type"": ""Value"",
                    ""id"": ""ea4f1b32-afc9-40bb-b3da-e153e33ed750"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TargetingDirection"",
                    ""type"": ""Value"",
                    ""id"": ""ca3b44ed-5b5d-4d78-aaa0-530139442182"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""6313db86-b870-4d51-98d7-f9970aed06f4"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""e7eb9102-6ae2-4a42-9750-9ee7cf1b7ce9"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a1233f9d-101b-4cda-bb62-d6301466f63a"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""aa496a2c-8036-4331-a623-f147d7de6d64"",
                    ""path"": ""<Gamepad>/leftStick/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cb074c8f-9d7a-4903-8e60-812b4e26426d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f922a11f-ceb4-41f4-8add-35f760eaa494"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""c46a3089-4468-425c-b72b-bb026c266f79"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Climb"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""60ffbb68-f89d-4c6d-a71b-2dded5d67716"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Climb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""24be3f05-3f96-4faf-b4ff-072609e3ae61"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Climb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""46935ab2-a292-488f-99ca-216bac52ac28"",
                    ""path"": ""<Gamepad>/leftStick/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Climb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b520653b-e77b-4536-9cee-24e2e962ad75"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf893dc0-7bad-44c2-8ad1-b09345f6643d"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1419d8d8-8b37-4d4e-82f5-24a8d564fec4"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ec9c7f30-087f-4313-ada3-d1acd5276023"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""AltFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1c852914-93fb-4986-94a5-a53db5a885de"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""StartGrapplingHook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f66409ec-fbbb-40b1-85d2-d83b883458ac"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""StartGrapplingHook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9141b16a-573e-4516-a918-dcd6d6ff16a2"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""OpenMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d9f30ab-4494-42b6-8255-4688eab26896"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""SelectDig"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""005ed92a-2e89-40b0-9aa8-6a7ca986b2e4"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""SelectConstruction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bd2f230b-fa20-4679-a0b6-1a136b948f71"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""ScrollMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""1d07e82e-0ccf-4828-9917-f5da41bd089d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScrollMode"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""856deaa3-92d8-4d0a-a262-894c1f6241c8"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=2.3)"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ScrollMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""08ef1c08-d57e-4609-9c4a-8b6a7139bc3c"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": ""Scale(factor=2.3)"",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ScrollMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""57495cd8-b6db-4c81-a46f-83a354ccb352"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""StopGrapplingHook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c2813d46-6ee6-49ca-88f3-103846bc43ca"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""StopGrapplingHook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d205e340-b1d5-4d4b-970a-b96bac52d8a2"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""CursorPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""342d48a1-2a19-4fe1-8bf3-94e0a2956bbb"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""StopFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4168d8f6-aaed-40cc-9029-531075ed8f22"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""StopFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd3119b0-aff3-4f39-952e-85680463652a"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""SelectFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f0c155e0-2842-43ae-9b07-f422383747c6"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""TargetingDirection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI"",
            ""id"": ""8c856449-d459-4eed-8ac2-c6d1563f31b7"",
            ""actions"": [
                {
                    ""name"": ""Pointing"",
                    ""type"": ""PassThrough"",
                    ""id"": ""2e524a44-862b-47c6-b670-dd2bb93026d4"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Clicking"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5104667c-058b-4f97-918e-07c2f2e1329e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""fdb53506-cba7-4a44-95b0-c9ffc93b536d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5853befc-203f-4ce4-8e0a-19b86c628e47"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard+Mouse"",
                    ""action"": ""Pointing"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2061f434-e1bb-46c5-9d50-1ed82bd4cb1f"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Clicking"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d327f4a9-3829-491e-b9b2-6e657a4cf5f3"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard+Mouse"",
            ""bindingGroup"": ""Keyboard+Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Basic Controls
        m_BasicControls = asset.FindActionMap("Basic Controls", throwIfNotFound: true);
        m_BasicControls_Move = m_BasicControls.FindAction("Move", throwIfNotFound: true);
        m_BasicControls_Jump = m_BasicControls.FindAction("Jump", throwIfNotFound: true);
        m_BasicControls_Climb = m_BasicControls.FindAction("Climb", throwIfNotFound: true);
        m_BasicControls_Fire = m_BasicControls.FindAction("Fire", throwIfNotFound: true);
        m_BasicControls_StopFire = m_BasicControls.FindAction("StopFire", throwIfNotFound: true);
        m_BasicControls_AltFire = m_BasicControls.FindAction("AltFire", throwIfNotFound: true);
        m_BasicControls_StartGrapplingHook = m_BasicControls.FindAction("StartGrapplingHook", throwIfNotFound: true);
        m_BasicControls_StopGrapplingHook = m_BasicControls.FindAction("StopGrapplingHook", throwIfNotFound: true);
        m_BasicControls_OpenMenu = m_BasicControls.FindAction("OpenMenu", throwIfNotFound: true);
        m_BasicControls_SelectDig = m_BasicControls.FindAction("SelectDig", throwIfNotFound: true);
        m_BasicControls_SelectFire = m_BasicControls.FindAction("SelectFire", throwIfNotFound: true);
        m_BasicControls_SelectConstruction = m_BasicControls.FindAction("SelectConstruction", throwIfNotFound: true);
        m_BasicControls_ScrollMode = m_BasicControls.FindAction("ScrollMode", throwIfNotFound: true);
        m_BasicControls_CursorPosition = m_BasicControls.FindAction("CursorPosition", throwIfNotFound: true);
        m_BasicControls_TargetingDirection = m_BasicControls.FindAction("TargetingDirection", throwIfNotFound: true);
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_Pointing = m_UI.FindAction("Pointing", throwIfNotFound: true);
        m_UI_Clicking = m_UI.FindAction("Clicking", throwIfNotFound: true);
        m_UI_Movement = m_UI.FindAction("Movement", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Basic Controls
    private readonly InputActionMap m_BasicControls;
    private IBasicControlsActions m_BasicControlsActionsCallbackInterface;
    private readonly InputAction m_BasicControls_Move;
    private readonly InputAction m_BasicControls_Jump;
    private readonly InputAction m_BasicControls_Climb;
    private readonly InputAction m_BasicControls_Fire;
    private readonly InputAction m_BasicControls_StopFire;
    private readonly InputAction m_BasicControls_AltFire;
    private readonly InputAction m_BasicControls_StartGrapplingHook;
    private readonly InputAction m_BasicControls_StopGrapplingHook;
    private readonly InputAction m_BasicControls_OpenMenu;
    private readonly InputAction m_BasicControls_SelectDig;
    private readonly InputAction m_BasicControls_SelectFire;
    private readonly InputAction m_BasicControls_SelectConstruction;
    private readonly InputAction m_BasicControls_ScrollMode;
    private readonly InputAction m_BasicControls_CursorPosition;
    private readonly InputAction m_BasicControls_TargetingDirection;
    public struct BasicControlsActions
    {
        private @InputMaster m_Wrapper;
        public BasicControlsActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_BasicControls_Move;
        public InputAction @Jump => m_Wrapper.m_BasicControls_Jump;
        public InputAction @Climb => m_Wrapper.m_BasicControls_Climb;
        public InputAction @Fire => m_Wrapper.m_BasicControls_Fire;
        public InputAction @StopFire => m_Wrapper.m_BasicControls_StopFire;
        public InputAction @AltFire => m_Wrapper.m_BasicControls_AltFire;
        public InputAction @StartGrapplingHook => m_Wrapper.m_BasicControls_StartGrapplingHook;
        public InputAction @StopGrapplingHook => m_Wrapper.m_BasicControls_StopGrapplingHook;
        public InputAction @OpenMenu => m_Wrapper.m_BasicControls_OpenMenu;
        public InputAction @SelectDig => m_Wrapper.m_BasicControls_SelectDig;
        public InputAction @SelectFire => m_Wrapper.m_BasicControls_SelectFire;
        public InputAction @SelectConstruction => m_Wrapper.m_BasicControls_SelectConstruction;
        public InputAction @ScrollMode => m_Wrapper.m_BasicControls_ScrollMode;
        public InputAction @CursorPosition => m_Wrapper.m_BasicControls_CursorPosition;
        public InputAction @TargetingDirection => m_Wrapper.m_BasicControls_TargetingDirection;
        public InputActionMap Get() { return m_Wrapper.m_BasicControls; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BasicControlsActions set) { return set.Get(); }
        public void SetCallbacks(IBasicControlsActions instance)
        {
            if (m_Wrapper.m_BasicControlsActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnMove;
                @Jump.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnJump;
                @Climb.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnClimb;
                @Climb.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnClimb;
                @Climb.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnClimb;
                @Fire.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnFire;
                @StopFire.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopFire;
                @StopFire.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopFire;
                @StopFire.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopFire;
                @AltFire.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnAltFire;
                @AltFire.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnAltFire;
                @AltFire.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnAltFire;
                @StartGrapplingHook.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStartGrapplingHook;
                @StartGrapplingHook.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStartGrapplingHook;
                @StartGrapplingHook.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStartGrapplingHook;
                @StopGrapplingHook.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopGrapplingHook;
                @StopGrapplingHook.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopGrapplingHook;
                @StopGrapplingHook.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnStopGrapplingHook;
                @OpenMenu.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnOpenMenu;
                @OpenMenu.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnOpenMenu;
                @OpenMenu.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnOpenMenu;
                @SelectDig.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectDig;
                @SelectDig.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectDig;
                @SelectDig.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectDig;
                @SelectFire.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectFire;
                @SelectFire.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectFire;
                @SelectFire.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectFire;
                @SelectConstruction.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectConstruction;
                @SelectConstruction.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectConstruction;
                @SelectConstruction.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnSelectConstruction;
                @ScrollMode.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnScrollMode;
                @ScrollMode.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnScrollMode;
                @ScrollMode.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnScrollMode;
                @CursorPosition.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnCursorPosition;
                @CursorPosition.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnCursorPosition;
                @CursorPosition.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnCursorPosition;
                @TargetingDirection.started -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnTargetingDirection;
                @TargetingDirection.performed -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnTargetingDirection;
                @TargetingDirection.canceled -= m_Wrapper.m_BasicControlsActionsCallbackInterface.OnTargetingDirection;
            }
            m_Wrapper.m_BasicControlsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Climb.started += instance.OnClimb;
                @Climb.performed += instance.OnClimb;
                @Climb.canceled += instance.OnClimb;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @StopFire.started += instance.OnStopFire;
                @StopFire.performed += instance.OnStopFire;
                @StopFire.canceled += instance.OnStopFire;
                @AltFire.started += instance.OnAltFire;
                @AltFire.performed += instance.OnAltFire;
                @AltFire.canceled += instance.OnAltFire;
                @StartGrapplingHook.started += instance.OnStartGrapplingHook;
                @StartGrapplingHook.performed += instance.OnStartGrapplingHook;
                @StartGrapplingHook.canceled += instance.OnStartGrapplingHook;
                @StopGrapplingHook.started += instance.OnStopGrapplingHook;
                @StopGrapplingHook.performed += instance.OnStopGrapplingHook;
                @StopGrapplingHook.canceled += instance.OnStopGrapplingHook;
                @OpenMenu.started += instance.OnOpenMenu;
                @OpenMenu.performed += instance.OnOpenMenu;
                @OpenMenu.canceled += instance.OnOpenMenu;
                @SelectDig.started += instance.OnSelectDig;
                @SelectDig.performed += instance.OnSelectDig;
                @SelectDig.canceled += instance.OnSelectDig;
                @SelectFire.started += instance.OnSelectFire;
                @SelectFire.performed += instance.OnSelectFire;
                @SelectFire.canceled += instance.OnSelectFire;
                @SelectConstruction.started += instance.OnSelectConstruction;
                @SelectConstruction.performed += instance.OnSelectConstruction;
                @SelectConstruction.canceled += instance.OnSelectConstruction;
                @ScrollMode.started += instance.OnScrollMode;
                @ScrollMode.performed += instance.OnScrollMode;
                @ScrollMode.canceled += instance.OnScrollMode;
                @CursorPosition.started += instance.OnCursorPosition;
                @CursorPosition.performed += instance.OnCursorPosition;
                @CursorPosition.canceled += instance.OnCursorPosition;
                @TargetingDirection.started += instance.OnTargetingDirection;
                @TargetingDirection.performed += instance.OnTargetingDirection;
                @TargetingDirection.canceled += instance.OnTargetingDirection;
            }
        }
    }
    public BasicControlsActions @BasicControls => new BasicControlsActions(this);

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_Pointing;
    private readonly InputAction m_UI_Clicking;
    private readonly InputAction m_UI_Movement;
    public struct UIActions
    {
        private @InputMaster m_Wrapper;
        public UIActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Pointing => m_Wrapper.m_UI_Pointing;
        public InputAction @Clicking => m_Wrapper.m_UI_Clicking;
        public InputAction @Movement => m_Wrapper.m_UI_Movement;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @Pointing.started -= m_Wrapper.m_UIActionsCallbackInterface.OnPointing;
                @Pointing.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnPointing;
                @Pointing.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnPointing;
                @Clicking.started -= m_Wrapper.m_UIActionsCallbackInterface.OnClicking;
                @Clicking.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnClicking;
                @Clicking.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnClicking;
                @Movement.started -= m_Wrapper.m_UIActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnMovement;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Pointing.started += instance.OnPointing;
                @Pointing.performed += instance.OnPointing;
                @Pointing.canceled += instance.OnPointing;
                @Clicking.started += instance.OnClicking;
                @Clicking.performed += instance.OnClicking;
                @Clicking.canceled += instance.OnClicking;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
            }
        }
    }
    public UIActions @UI => new UIActions(this);
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard+Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IBasicControlsActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnClimb(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnStopFire(InputAction.CallbackContext context);
        void OnAltFire(InputAction.CallbackContext context);
        void OnStartGrapplingHook(InputAction.CallbackContext context);
        void OnStopGrapplingHook(InputAction.CallbackContext context);
        void OnOpenMenu(InputAction.CallbackContext context);
        void OnSelectDig(InputAction.CallbackContext context);
        void OnSelectFire(InputAction.CallbackContext context);
        void OnSelectConstruction(InputAction.CallbackContext context);
        void OnScrollMode(InputAction.CallbackContext context);
        void OnCursorPosition(InputAction.CallbackContext context);
        void OnTargetingDirection(InputAction.CallbackContext context);
    }
    public interface IUIActions
    {
        void OnPointing(InputAction.CallbackContext context);
        void OnClicking(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
    }
}
