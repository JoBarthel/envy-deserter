﻿
using System;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

[RequireComponent(typeof(NormalProjectile))]
public class ProjectileNetwork : ProjectileBehavior
{
    public NormalProjectile NormalProjectile;

    private void Awake()
    {
        if (networkObject == null)
        {
            Debug.LogWarning("ProjectileNetwork.cs, Awake : networkObject is null.");
            Destroy(this);

            return;
        }

        NormalProjectile = GetComponent<NormalProjectile>();
    }

    public override void OnTargetReached(RpcArgs args)
    {
        throw new NotImplementedException();
    }

    private void Update()
    {
        if (networkObject.IsOwner)
        {
            //Vector2 move = normalProjectile.direction * normalProjectile.speed * Time.deltaTime;
            //transform.position += (Vector3)move;

            networkObject.position = transform.position;
            networkObject.rotation = transform.rotation;
        }
        else
        {
            transform.position = networkObject.position;
            transform.rotation = networkObject.rotation;
        }
    }
}
