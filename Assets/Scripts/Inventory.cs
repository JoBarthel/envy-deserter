﻿using System;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using TMPro;
using UnityEngine;

public struct InventoryState
{
    public int numberOfCrystals;
    public int numberOfBlocks;

    public InventoryState(int numberOfCrystals, int numberOfBlocks)
    {
        this.numberOfCrystals = numberOfCrystals;
        this.numberOfBlocks = numberOfBlocks;
    }
}
/**
 * Holds the inventory for the player
 * Notify observers of state change in the inventory
 * The inventory state is a tuple with the number of blocks and the numbers of crystals
 * in player inventory, in this order
 */
public class Inventory : MonoBehaviour, IObservable<InventoryState>
{
    [SerializeField] private int maxCarriedBlocks = 20;
    public int MaxCarriedBlocks => maxCarriedBlocks;

    [SerializeField] private int maxCarriedCrystals = 20;
    public int MaxCarriedCrystals => maxCarriedCrystals;

    public int NumberOfCrystals { get; private set; }

    public int NumberOfDirtBlocks { get; private set; }

    [SerializeField] int dirtBlockIndexToNetworkInstantiate = 4;
    [SerializeField] int crystalIndexToNetworkInstantiate = 5;

    [SerializeField] int dirtBlockIndexToNetworkInstantiateWhenDroppingIndividualItems = 6;
    [SerializeField] int crystalIndexToNetworkInstantiateWhenDroppingIndividualItems = 7;

    [SerializeField] int dirtBlockIndexToNetworkInstantiateWhenDroppingPacks = 6;
    [SerializeField] int crystalIndexToNetworkInstantiateWhenDroppingPacks = 7;


    public int DirtBlockIndexToNetworkInstantiate { get { return dirtBlockIndexToNetworkInstantiate; } }
    public int CrystalIndexToNetworkInstantiate { get { return crystalIndexToNetworkInstantiate; } }

    [SerializeField] float speedToFlyDropItems = 7f;
    [SerializeField] float minAngleAboveTheHorizontalToDropItems = 60f;

    public void UpdateInventory(DropType dropType, int amountToAdd)
    {
        switch (dropType)
        {
            case DropType.Block:
                NumberOfDirtBlocks += amountToAdd;
                break;

            case DropType.Crystal:
                NumberOfCrystals += amountToAdd;
                break;

        }
        NotifyObservers();
    }


    public int MaxCarriedBlocksByType(DropType dropType)
    {
        switch (dropType)
        {
            case DropType.Block:
                return maxCarriedBlocks;

            case DropType.Crystal:
                return maxCarriedCrystals;
            
            default:
                return maxCarriedBlocks;
        }
    }

    public int NumberCarriedBlocksByType(DropType dropType)
    {
        switch (dropType)
        {
            case DropType.Block:
                return NumberOfDirtBlocks;

            case DropType.Crystal:
                return NumberOfCrystals;

            default:
                return NumberOfDirtBlocks;
        }

    }

    public void DropPack(DropType dropType, int amountToDrop, bool dropIndividualItems = false)
    {
        if (amountToDrop <= 0) return;

        PlayerNetwork playerNetwork = GetComponent<PlayerNetwork>();
        ChestNetwork chestNetwork = GetComponent<ChestNetwork>();

        if (playerNetwork != null && playerNetwork.networkObject.IsOwner)
        {
            playerNetwork.networkObject.SendRpc(PlayerNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, BeardedManStudios.Forge.Networking.Receivers.All,
                                                -amountToDrop, playerNetwork.ownerID, (int)dropType);
        }
        else if (chestNetwork != null && chestNetwork.networkObject.IsOwner)
        {
            chestNetwork.networkObject.SendRpc(ChestNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, BeardedManStudios.Forge.Networking.Receivers.All,
                                                -amountToDrop, chestNetwork.playerOwnerId, (int)dropType);
        }

        DropItemBehavior dropItemBehavior = null;
        DropItem dropItem = null;
        if ((playerNetwork != null && playerNetwork.networkObject.IsOwner) ||
            (chestNetwork != null && chestNetwork.networkObject.IsOwner))
        {
            int indexToNetworkInstantiate = 0;
            switch (dropType)
            {
                case DropType.Block:
                    indexToNetworkInstantiate = dirtBlockIndexToNetworkInstantiateWhenDroppingPacks;
                    break;

                case DropType.Crystal:
                    indexToNetworkInstantiate = crystalIndexToNetworkInstantiateWhenDroppingPacks;
                    break;

                default:
                    indexToNetworkInstantiate = dirtBlockIndexToNetworkInstantiate;
                    break;
            }


            if (!dropIndividualItems)
            {
                dropItemBehavior = NetworkManager.Instance.InstantiateDropItem(indexToNetworkInstantiate, transform.position, Quaternion.identity, true);
                dropItem = dropItemBehavior.GetComponent<DropItem>();

                if (dropItem.networkHasStarted)
                {
                    dropItem.networkObject.SendRpc(DropItem.RPC_UPDATE_AMOUNT_OF_ITEMS_HELD, BeardedManStudios.Forge.Networking.Receivers.All, amountToDrop);
                }
                else
                {
                    dropItem.amountOfItemsHeld = amountToDrop;
                    dropItem.networkStarted += dropItem.TransmitMyAmountOfItemsHeldToOtherInstances;
                }


                if (dropItemBehavior != null)
                {
                    Rigidbody2D rigidbody2D = dropItemBehavior.GetComponent<Rigidbody2D>();

                    if (rigidbody2D != null && dropItem != null && dropItem.networkObject.IsOwner)
                    {
                        float angle = UnityEngine.Random.Range(minAngleAboveTheHorizontalToDropItems * Mathf.Deg2Rad,
                                                                Mathf.PI - minAngleAboveTheHorizontalToDropItems * Mathf.Deg2Rad);

                        Vector3 direction = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f);
                        rigidbody2D.velocity = direction * speedToFlyDropItems;
                    }
                }
            }
            else
            {
                switch (dropType)
                {
                    case DropType.Block:
                        indexToNetworkInstantiate = dirtBlockIndexToNetworkInstantiateWhenDroppingIndividualItems;
                        break;

                    case DropType.Crystal:
                        indexToNetworkInstantiate = crystalIndexToNetworkInstantiateWhenDroppingIndividualItems;
                        break;

                    default:
                        indexToNetworkInstantiate = dirtBlockIndexToNetworkInstantiate;
                        break;
                }

                for (int i = 0; i < amountToDrop; i++)
                {
                    dropItemBehavior = NetworkManager.Instance.InstantiateDropItem(indexToNetworkInstantiate, transform.position, Quaternion.identity, true);
                    dropItem = dropItemBehavior.GetComponent<DropItem>();

                    //if (dropItem.networkHasStarted)
                    //{
                    //    dropItem.networkObject.SendRpc(DropItem.RPC_UPDATE_AMOUNT_OF_ITEMS_HELD, BeardedManStudios.Forge.Networking.Receivers.All, 1);
                    //}
                    //else
                    //{
                    //    dropItem.amountOfItemsHeld = 1;
                    //    dropItem.networkStarted += dropItem.TransmitMyAmountOfItemsHeldToOtherInstances;
                    //}


                    if (dropItemBehavior != null)
                    {
                        Rigidbody2D rigidbody2D = dropItemBehavior.GetComponent<Rigidbody2D>();

                        if (rigidbody2D != null && dropItem != null && dropItem.networkObject.IsOwner)
                        {
                            float angle = UnityEngine.Random.Range(minAngleAboveTheHorizontalToDropItems * Mathf.Deg2Rad,
                                                                    Mathf.PI - minAngleAboveTheHorizontalToDropItems * Mathf.Deg2Rad);

                            Vector3 direction = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f);
                            rigidbody2D.velocity = direction * speedToFlyDropItems;
                        }
                    }
                }
            }
        }

        
    }

    private List<IObserver<InventoryState>> _observers = new List<IObserver<InventoryState>>();
    public IDisposable Subscribe(IObserver<InventoryState> observer)
    {
        _observers.Add(observer);
        return new Unsubscriber(_observers, observer);
    }
    private void NotifyObservers()
    {
        foreach (IObserver<InventoryState> observer in _observers)
            observer.OnNext(new InventoryState(NumberOfCrystals, NumberOfDirtBlocks));
    }

    private class Unsubscriber : IDisposable
    {
        private List<IObserver<InventoryState>> _observers;
        private IObserver<InventoryState> _observer;

        public Unsubscriber(List<IObserver<InventoryState>> observers, IObserver<InventoryState> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observer != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    [ContextMenu("Show my inventory")]
    public void ShowInventory()
    {
        Debug.Log("Dirt Blocks: " + NumberOfDirtBlocks.ToString());
        Debug.Log("Crystals: " + NumberOfCrystals.ToString());
    }
}
