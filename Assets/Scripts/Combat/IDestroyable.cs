﻿using UnityEngine.Events;

namespace EnvyDeserter.Combat
{
    public interface IDestroyable
    {
        void AddOnHealthChanged(UnityAction<float> callback);
    }
}