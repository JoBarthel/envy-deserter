﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ManageFrontgroundItem : MonoBehaviour
{
    [SerializeField] float timeBetweenChecksOnTileMap = 0.5f;
    float timeSinceLastCheckOnTileMap;

    SpriteRenderer spriteRenderer;

    [SerializeField] float timeToDisappear = 1f;

    bool canCheckTerrain = true;

    MapManager mapManager;

    private void Start()
    {
        timeSinceLastCheckOnTileMap = timeBetweenChecksOnTileMap;

        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        mapManager = GameObject.Find("MapManager")?.GetComponent<MapManager>();
    }

    private void Update()
    {
        timeSinceLastCheckOnTileMap += Time.deltaTime;

        if(timeSinceLastCheckOnTileMap > timeBetweenChecksOnTileMap && canCheckTerrain)
        {
            timeSinceLastCheckOnTileMap = 0f;

            CheckTerrain();
        }
    }

    void CheckTerrain()
    {
        if(mapManager != null)
        {
            TileBase tileAtPosition = mapManager.GetTileAtPosition(transform.position);
            if(tileAtPosition == null)
            {
                canCheckTerrain = false;

                //Disappear
                IEnumerator disappearCoroutine = Disappear();
                StartCoroutine(disappearCoroutine);
            }
        }
    }

    IEnumerator Disappear()
    {
        float disappearingTime = 0f;

        while(disappearingTime < timeToDisappear)
        {
            disappearingTime += Time.deltaTime;
            Color spriteColor = spriteRenderer.color;
            spriteColor.a = (1f - (disappearingTime / timeToDisappear));
            spriteRenderer.color = spriteColor;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        Destroy(gameObject);
    }

}


