﻿using BeardedManStudios.Forge.Networking.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestHealth : ColorChangeOnHit, IDamageable
{
    [SerializeField] private int maxHealth;
    public int CurrentHealth { get { return currentHealth; } private set { currentHealth = value; } }
    [SerializeField] private int currentHealth;

    [SerializeField] ChestNetwork chestNetwork;

    Inventory chestInventory;

    bool canTakeDamage = true;

    [SerializeField] public Slider healthBarSlider;  //UI slider used to represent the current HP of a chest

    bool networkHasStarted = false;

    Animator chestAnimator;

    [HideInInspector] public ColorChangeOnHit minimapChestMarkerColorChangeOnHit;

    [SerializeField] InventoryObserver chestInventoryObserver;


    private void Start()
    {
        currentHealth = maxHealth;
        if (healthBarSlider != null)
        {
            healthBarSlider.maxValue = maxHealth;
            healthBarSlider.value = currentHealth;
        }

        chestInventory = GetComponent<Inventory>();

        if(chestInventory != null && chestInventoryObserver != null) chestInventoryObserver.Subscribe(chestInventory);

        chestAnimator = GetComponent<Animator>();


        chestNetwork = GetComponent<ChestNetwork>();
        if (chestNetwork != null) chestNetwork.networkStarted += InitAfterNetworkStart;
    }

    void InitAfterNetworkStart(NetworkBehavior networkBehavior)
    {
        networkHasStarted = true;
    }

    [ContextMenu("Die")]
    public new void Die()
    {
        base.Die();

        if(chestAnimator != null)
        {
            chestAnimator?.SetBool("dead", true);
        }

        Inventory myInventory = GetComponent<Inventory>();

        if (myInventory != null)
        {
            if (myInventory.NumberOfDirtBlocks > 0)
                myInventory.DropPack(DropType.Block, myInventory.NumberOfDirtBlocks, false);

            if (myInventory.NumberOfCrystals > 0)
                myInventory.DropPack(DropType.Crystal, myInventory.NumberOfCrystals, false);
        }

        Inventory chestInventory = GetComponent<Inventory>();
        /*if(chestInventory != null)
        {
            for (int i = 0; i < Enum.GetNames(typeof(DropItem)).Length; i++)
            {
                
            }
        }*/


        chestNetwork.networkObject.Destroy();
    }

    public new void TakeDamage(int damage)
    {
        if (canTakeDamage)
        {
            base.TakeDamage(damage);

            currentHealth -= damage;
            if (chestAnimator != null) chestAnimator?.SetTrigger("hurt");
            if (minimapChestMarkerColorChangeOnHit != null && damage > 0) minimapChestMarkerColorChangeOnHit.TakeDamage(damage);

            if (healthBarSlider != null)
            {
                healthBarSlider.value = currentHealth;
            }
        }

        if (currentHealth <= 0) Die();
    }

    [ContextMenu("Take 40 Damage")]
    public void Take40Damage() => TakeDamage(40);
}
