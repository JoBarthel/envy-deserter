﻿using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndPanel : MonoBehaviour
{
    public TextMeshProUGUI Text;

    public void BackToMenu()
    {
        //NetworkManager.Instance.Disconnect();
        GameManagerNetwork.gameManagerNetwork.networkObject.SendRpc(GameManagerNetwork.RPC_DISCONNECT_FROM_SERVER, BeardedManStudios.Forge.Networking.Receivers.Server);

        // Load MultiplayerMenuStartScene
        SceneManager.LoadScene(0);

        // TODO check if we need to change things on the network
        // Manage disconnections, etc...
    }
}
