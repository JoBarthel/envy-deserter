﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MinimapUncover : MonoBehaviour
{
    [SerializeField] MapManager mapManager;

    [Space]

    [SerializeField] float timeBetweenMinimapUncoverUpdates = 0.5f;
    float timeSinceLastMinimapUncoverUpdate;

    [SerializeField] Transform playerPosition;

    Grid coverMinimapGrid;
    [SerializeField] TileBase minimapCoverTile;
    [SerializeField] Tilemap coverMinimapTilemap;

    [Space]

    [SerializeField] float horizontalDistanceFromPlayerToUncover = 15f;
    [SerializeField] float verticalDistanceFromPlayerToUncover = 5f;

    [SerializeField] bool useCircularUncoverZone = false;
    [SerializeField] float circularUncoverZoneRadius = 5f;

    [Space]
    [SerializeField] Camera minimapCamera;

    bool alreadyCoveredMinimap = false;

    private void Start()
    {
        timeSinceLastMinimapUncoverUpdate = timeBetweenMinimapUncoverUpdates;

        coverMinimapGrid = GetComponent<Grid>();

        //CoverMinimap();
    }

    public void CoverMinimap()
    {
        coverMinimapTilemap.ClearAllTiles();

        //Set minimap camera size settings to adjust to the size of the generated map
        minimapCamera.orthographicSize = mapManager.mapGenerator.map.GetLength(1) * mapManager.grid.cellSize.y / 2f;
        float value = (float) mapManager.mapGenerator.map.GetLength(1) / mapManager.mapGenerator.map.GetLength(0);
        minimapCamera.rect = new Rect(0f, 0f, 1f, value);
        //Debug.Log("value: " + value.ToString());
        //Debug.Log("minimapCamera.rect.height: " + minimapCamera.rect.height.ToString());
        minimapCamera.transform.position = new Vector3(mapManager.transform.position.x + (mapManager.mapGenerator.map.GetLength(0) * mapManager.grid.cellSize.x / 2f), 
                                                        mapManager.transform.position.y + (mapManager.mapGenerator.map.GetLength(1) * mapManager.grid.cellSize.y / 2f), 
                                                        -1f);

        Vector3 currentPosition = Vector3.zero;
        for(float positionX = mapManager.transform.position.x; positionX <= mapManager.transform.position.x + mapManager.mapGenerator.map.GetLength(0) * mapManager.grid.cellSize.x; positionX += mapManager.grid.cellSize.x)
        {
            for (float positionY = mapManager.transform.position.y; positionY <= mapManager.transform.position.y + mapManager.mapGenerator.map.GetLength(1) * mapManager.grid.cellSize.y; positionY += mapManager.grid.cellSize.y)
            {
                currentPosition.x = positionX;
                currentPosition.y = positionY;

                Vector3Int currentCell = coverMinimapTilemap.WorldToCell(currentPosition);

                coverMinimapTilemap.SetTileFlags(currentCell, TileFlags.None);
                coverMinimapTilemap.SetTile(currentCell, minimapCoverTile);     
                coverMinimapTilemap.SetColor(currentCell, Color.black);
            }
        }

        alreadyCoveredMinimap = true;
    }

    void UncoverRegion(Vector3 position)
    {
        Vector3 currentPosition = Vector3.zero;

        for (float positionX = position.x - horizontalDistanceFromPlayerToUncover; positionX <= position.x + horizontalDistanceFromPlayerToUncover; positionX += coverMinimapGrid.cellSize.x)
        {
            for(float positionY = position.y - verticalDistanceFromPlayerToUncover; positionY <= position.y + verticalDistanceFromPlayerToUncover; positionY += coverMinimapGrid.cellSize.y)
            {
                currentPosition.x = positionX;
                currentPosition.y = positionY;
                Vector3Int currentCell = coverMinimapTilemap.WorldToCell(currentPosition);

                if (!useCircularUncoverZone || (useCircularUncoverZone && (currentPosition - position).sqrMagnitude <= circularUncoverZoneRadius * circularUncoverZoneRadius))
                {
                    coverMinimapTilemap.SetTileFlags(currentCell, TileFlags.None);
                    coverMinimapTilemap.SetTile(currentCell, null);
                    coverMinimapTilemap.SetColor(currentCell, Color.clear);
                }
                

            }
        }
    }


    private void Update()
    {
        timeSinceLastMinimapUncoverUpdate += Time.deltaTime;

        if(timeSinceLastMinimapUncoverUpdate >= timeBetweenMinimapUncoverUpdates && alreadyCoveredMinimap)
        {
            timeSinceLastMinimapUncoverUpdate = 0f;
            UncoverRegion(playerPosition.position);
        }
    }

}
