﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Character;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class GrapplingHook : GrapplingHookBehavior, IObservable<bool>
{
    private struct Hinge
    {
        public Vector2 position;
        public Vector2 breakDirection;

        public Hinge(Vector2 pos, Vector2 breakDir)
        {
            position = pos;
            breakDirection = breakDir;
        }
    }

    public const float END_OF_ROPE_TOLERANCE = 0.5f;

    [SerializeField] private float maxLength = 20.0f;
    public float MaxLength => maxLength;

    private float _wantedLength;
    public float WantedLength => _wantedLength;
    [SerializeField] private float lengthChangeSpeed = 8.0f;

    private readonly List<Hinge> _hinges = new List<Hinge>();
    private LineRenderer _rope;

    private Player _player;
    [SerializeField] private PlayerNetwork playerNetwork;
    [SerializeField] private ContactFilter2D contactFilter;
    private readonly RaycastHit2D[] _hits = new RaycastHit2D[8];
    
    public Vector2 PosToHinge => _hinges.Count > 0
        ? _hinges[_hinges.Count - 1].position - _player.Rigidbody.position
        : (Vector2) Hook.transform.position - _player.Rigidbody.position;

    public Vector2 LastHingeDirection => _hinges.Count > 0
        ? _hinges[_hinges.Count - 1].breakDirection
        : Vector2.zero;

    public float RopeLength { get; private set; }

    public Hook Hook { get; private set; }

    public bool Fired { get; private set; }
    public bool Released { get; private set; }

    public int HingeCount => _hinges.Count;

    public bool EndOfRope => RopeLength > WantedLength - END_OF_ROPE_TOLERANCE;

    private void Awake()
    {
        _wantedLength = maxLength;
        if (playerNetwork != null)
            playerNetwork.networkStarted += InitAfterNetworkStarted;
        
        Hook = GetComponentInChildren<Hook>();
        if (Hook == null) Debug.LogWarning(name + " has a GrapplingHook component but no Hook in its children.");
        else Hook.gameObject.SetActive(false);

        _rope = GetComponent<LineRenderer>();
        if (_rope == null) Debug.LogWarning(name + " has a GrapplingHook component but no LineRenderer.");
        else _rope.enabled = false;

        _player = GetComponentInParent<Player>();
        if (_player == null) Debug.LogWarning(name + " has a GrapplingHook component but no Player in its parent.");
    }

    private void InitAfterNetworkStarted(NetworkBehavior behavior)
    {
        if (!networkObject.IsOwner) return;
        
        _inputMaster = controller.GetInputMaster();

        _inputMaster.BasicControls.StartGrapplingHook.performed += delegate
        {
            var direction
                = controller.GetLookPosition()
                - _player.Rigidbody.position;
            direction.Normalize();

            Launch(direction);
            networkObject?.SendRpc(RPC_SHOOT_HOOK, Receivers.Others, networkObject.NetworkId, direction);
        };

        _inputMaster.BasicControls.StopGrapplingHook.performed += delegate
        {
            Retract();
            networkObject?.SendRpc(RPC_RETRACT_HOOK, Receivers.Others, networkObject.NetworkId);
        };
    }

    private InputMaster _inputMaster;
    [SerializeField] private Controller controller;

    private bool _playerIsClimbing;
    private void Update()
    {
        if (networkObject != null)
        {
            if (!networkObject.IsOwner)
            {
                Hook.transform.position = networkObject.hookPosition;

                if (Fired) UpdateRope();

                return;
            }

            networkObject.hookPosition = Hook.transform.position;
        }

        Released = false;
        if (_wantedLength <= END_OF_ROPE_TOLERANCE)
        {
            if (EndOfRope) Released = true;

            Retract();
            networkObject?.SendRpc(RPC_RETRACT_HOOK, Receivers.Others, networkObject.NetworkId);

            return;
        }

        if (Fired)
        {
            if (Hook.JustHooked
                || _player.Controller.IsOnGround && !_playerIsClimbing)
            {
                _wantedLength = RopeLength + 1.0f;
                if (_wantedLength > maxLength) _wantedLength = maxLength;
            }
            
            UpdateHinges();
            UpdateRope();

            if (EndOfRope && !Hook.IsHooked)
            {
                Retract();
                networkObject?.SendRpc(RPC_RETRACT_HOOK, Receivers.Others, networkObject.NetworkId);
            }
        }

        if (Hook.JustHooked) NotifyObservers(true);

        if (Hook.IsHooked)
        {
            float climbDirection = _inputMaster.BasicControls.Climb.ReadValue<float>();
            if (climbDirection > 0) ClimbUp();
            else if (climbDirection < 0) ClimbDown();
            else _playerIsClimbing = false;
        }
        else
            _playerIsClimbing = false;
        
        if (_hinges.Count <= 0) RopeLength = ((Vector2) Hook.transform.position - _player.Rigidbody.position).magnitude;
        else
        {
            RopeLength = ((Vector2) Hook.transform.position - _hinges[0].position).magnitude;
            for (var i = 0; i < _hinges.Count - 1; i++)
                RopeLength += (_hinges[i].position - _hinges[i + 1].position).magnitude;
            RopeLength += (_player.Rigidbody.position - _hinges[_hinges.Count - 1].position).magnitude;
        }
    }
    
    private void ClimbUp()
    {
        _playerIsClimbing = true;

        if (_wantedLength < RopeLength - END_OF_ROPE_TOLERANCE) return;

        if (_wantedLength > 0.0f)
            _wantedLength = _wantedLength - lengthChangeSpeed * Time.deltaTime >= 0.0f
                ? _wantedLength - lengthChangeSpeed * Time.deltaTime
                : 0.0f;
    }

    private void ClimbDown()
    {
        _playerIsClimbing = false;

        if (_wantedLength < maxLength)
            _wantedLength = _wantedLength + lengthChangeSpeed * Time.deltaTime <= maxLength
                ? _wantedLength + lengthChangeSpeed * Time.deltaTime
                : maxLength;
    }

    private void UpdateRope()
    {
        _rope.positionCount = _hinges.Count + 2;
        _rope.SetPosition(0, Hook.transform.position);
        for (var i = 0; i < _hinges.Count; i++)
            _rope.SetPosition(i + 1, _hinges[i].position);
        _rope.SetPosition(_hinges.Count + 1, _player.Rigidbody.position);
    }

    private void UpdateHinges()
    {
        var direction = _hinges.Count > 0
            ? _hinges[_hinges.Count - 1].position - _player.Rigidbody.position
            : (Vector2) Hook.transform.position - _player.Rigidbody.position;
        direction.Normalize();

        var ropeLength = _hinges.Count <= 0
            ? ((Vector2) Hook.transform.position - _player.Rigidbody.position).magnitude
            : (_hinges[_hinges.Count - 1].position - _player.Rigidbody.position).magnitude;

        var count = Physics2D.Raycast(_player.Rigidbody.position, direction, contactFilter, _hits, ropeLength);

        if (count <= 0)
        {
            if (_hinges.Count <= 0 || Vector3.Dot(_hinges[_hinges.Count - 1].breakDirection,
                    (_player.Rigidbody.position - _hinges[_hinges.Count - 1].position).normalized) <= 0.01f)
                return;
            
            _hinges.RemoveAt(_hinges.Count - 1);
            networkObject?.SendRpc(RPC_REMOVE_HINGE, Receivers.Others, networkObject.NetworkId);

            return;
        }

        var snappedPoint = SnapToGrid(_hits[0].point);

        if (_hinges.Count > 0 && (snappedPoint - _hinges[_hinges.Count - 1].position).sqrMagnitude <= 0.01f)
        {
            // Break the last hinge if the angle is too high
            if (Vector3.Dot(_hinges[_hinges.Count - 1].breakDirection,
                    (_player.Rigidbody.position - _hinges[_hinges.Count - 1].position).normalized) <= 0.01f)
                return;

            _hinges.RemoveAt(_hinges.Count - 1);
            networkObject?.SendRpc(RPC_REMOVE_HINGE, Receivers.Others, networkObject.NetworkId);

            return;
        }

        var collisionNormal = snappedPoint - _hits[0].point;
        var ropeNormal = _hinges.Count > 0
            ? Vector2.Perpendicular(_hinges[_hinges.Count - 1].position - snappedPoint)
            : Vector2.Perpendicular((Vector2) Hook.transform.position - snappedPoint);
        var breakDirection = Vector3.Project(collisionNormal, ropeNormal).normalized;

        // Add a hinge point if the rope collides with a block
        _hinges.Add(new Hinge(snappedPoint, breakDirection));
        networkObject?.SendRpc(RPC_ADD_HINGE, Receivers.Others, networkObject.NetworkId, snappedPoint);
    }

    private void Launch(Vector2 direction)
    {
        Hook.gameObject.SetActive(true);
        Hook.transform.parent = Game.Instance.Projectiles;
        Hook.transform.position = _player.Rigidbody.position;
        Hook.Launch(direction);

        _rope.enabled = true;
        Fired = true;
    }

    private void Retract()
    {
        Hook.transform.parent = transform;
        Hook.transform.localPosition = Vector3.zero;
        Hook.gameObject.SetActive(false);
        NotifyObservers(false);
        _rope.positionCount = 2;
        _rope.SetPosition(0, _player.Rigidbody.position);
        _rope.SetPosition(1, _player.Rigidbody.position);

        _hinges.Clear();

        _wantedLength = maxLength;
        _rope.enabled = false;
        Fired = false;
    }

    private static Vector2 SnapToGrid(Vector2 position)
        => new Vector2(Mathf.Round(position.x), Mathf.Round(position.y));

    #region Remote Procedure Calls
    
    public override void AddHinge(RpcArgs args)
    {
        if (args.GetNext<uint>() != networkObject.NetworkId) return;

        _hinges.Add(new Hinge(args.GetNext<Vector2>(), Vector2.zero));
    }

    public override void RemoveHinge(RpcArgs args)
    {
        if (args.GetNext<uint>() != networkObject.NetworkId) return;

        _hinges.RemoveAt(_hinges.Count - 1);
    }

    public override void ShootHook(RpcArgs args)
    {
        if (args.GetNext<uint>() != networkObject.NetworkId) return;

        Launch(args.GetNext<Vector2>());
    }

    public override void RetractHook(RpcArgs args)
    {
        if (args.GetNext<uint>() != networkObject.NetworkId) return;

        Retract();
    }

    #endregion

    #region Observable Pattern

    private readonly List<IObserver<bool>> _observers = new List<IObserver<bool>>();

    public IDisposable Subscribe(IObserver<bool> observer)
    {
        _observers.Add(observer);
        return new Unsubscriber(_observers, observer);
    }
    
    private void NotifyObservers(bool status)
    {
        foreach (IObserver<bool> observer in _observers)
        {
            observer.OnNext(status);
        }
    }

    private class Unsubscriber : IDisposable
    {
        private readonly List<IObserver<bool>> _observers;
        private readonly IObserver<bool> _observer;

        public Unsubscriber(List<IObserver<bool>> observers, IObserver<bool> observer)
        {
            _observers = observers;
            _observer = observer;
        }

        public void Dispose()
        {
            if (_observers != null && _observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }

    #endregion
}