﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class DoubleProjectileWeapon : Weapon
{
    [SerializeField] protected Projectile projectilePrefab;
    [SerializeField] protected Projectile altProjectilePrefab;

    [SerializeField] protected float altShootCooldown = 1.0f;
    protected float altShootTimer;

    private DoubleProjectileWeaponNetwork network;

    protected new void Awake()
    {
        base.Awake();

        network = GetComponent<DoubleProjectileWeaponNetwork>();
    }

    public new void NetworkStart()
    {
        base.NetworkStart();

        if (Player != null && !PlayerNetwork.networkObject.IsOwner || Controller == null) return;

        inputMaster.BasicControls.AltFire.performed += delegate
        {
            if (!isActiveAndEnabled || altShootTimer > 0.0f) return;

            altShootTimer = altShootCooldown;

            var target = Controller.GetCursorPositionInWorldSpace();
            AltShoot((target - (Vector2) transform.position).normalized);
        };
    }

    protected new void Update()
    {
        base.Update();

        if (altShootTimer >= 0.0f)
            altShootTimer -= Time.deltaTime;
    }

    public override void Shoot(Vector2 direction)
    {
        if (network.networkObject == null) InitProjectile(0, transform.position, direction);
        else
        {
            if (network.networkObject.IsOwner && isActiveAndEnabled && PlayerNetwork != null)
                network.networkObject.SendRpc(DoubleProjectileWeaponBehavior.RPC_SHOOT, Receivers.All,
                    PlayerNetwork.ownerID, (Vector2) transform.position, direction, false);
        }
    }

    public void AltShoot(Vector2 direction)
    {
        if (network.networkObject == null) InitAltProjectile(0, transform.position, direction);
        else
        {
            if (network.networkObject.IsOwner && isActiveAndEnabled && PlayerNetwork != null)
                network.networkObject.SendRpc(DoubleProjectileWeaponBehavior.RPC_SHOOT, Receivers.All,
                    PlayerNetwork.ownerID, (Vector2) transform.position, direction, true);
        }
    }

    public void InitProjectile(uint id, Vector2 position, Vector2 direction)
    {
        Projectile projectile = Pooling.Instance.GetProjectile(projectilePrefab);
        projectile.transform.position = position;
        projectile.Init(direction, id);
    }

    public void InitAltProjectile(uint id, Vector2 position, Vector2 direction)
    {
        Projectile projectile = Pooling.Instance.GetProjectile(altProjectilePrefab);
        projectile.transform.position = position;
        projectile.Init(direction, id);
    }
}