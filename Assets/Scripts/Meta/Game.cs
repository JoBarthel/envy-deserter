﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Game : MonoBehaviour
{
    [SerializeField] private Tilemap terrain;
    public Tilemap Terrain => terrain;

    [SerializeField] private Transform buildings;
    public Transform Buildings => buildings;

    [SerializeField] private Transform projectiles;
    public Transform Projectiles => projectiles;

    [SerializeField] private BuildingData buildingData;
    public BuildingData BuildingData => buildingData;

    public static Game Instance { get; private set; }

    [SerializeField] private Grid grid;
    public Grid Grid => grid;

    public readonly List<Player> Players = new List<Player>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    // Teams go from one to the number of players
    // Team 0 is neutral
    public void RegisterPlayer(Player player)
    {
        Players.Add(player);
        player.Stats.Team = Players.Count;
    }
}
