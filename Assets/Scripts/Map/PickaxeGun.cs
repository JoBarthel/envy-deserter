﻿using BeardedManStudios.Forge.Networking.Unity;
using Assets.Scripts.Character;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using UnityEngine;

public class PickaxeGun : MonoBehaviour
{
    [SerializeField] private PlayerNetwork playerNetwork;
    [SerializeField] private Player player;
    [SerializeField] private Controller controller;
    public Player Player => player;
    private MapManager _mapManager;
    public MapManager MapManager => _mapManager;

    private bool _networkHasStarted = false;

    private Vector2 _targetPosition;

    [SerializeField] private float minimumTimeBetweenPlacingBlocks = 0.2f;
    private float _timeSinceLastPlacingBlocks;
    
    [SerializeField] private float range = 5f;

    public int currentSizeOfSquareSelectionTiles = 2;
    
    private InputMaster _inputMaster;

    private PickaxeAnimator pickaxeAnimator;

    private void Awake()
    {
        if(playerNetwork != null)
            playerNetwork.networkStarted += InitAfterNetworkStarts;
        _mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        pickaxeAnimator = GetComponent<PickaxeAnimator>();
        if (pickaxeAnimator == null)
            Debug.LogWarning(name + " has a PickaxeGun component but no PickaxeAnimator.");

        _timeSinceLastPlacingBlocks = minimumTimeBetweenPlacingBlocks;
    }

    private bool _isMining;
    
    private void PlaceBlock()
    {
        if (!(_timeSinceLastPlacingBlocks >= minimumTimeBetweenPlacingBlocks)) return;
        if (!isActiveAndEnabled) return;
        _timeSinceLastPlacingBlocks = 0f;

        if (playerNetwork == null || playerNetwork.networkObject == null)
            pickaxeAnimator.PlaceBlockAnimation(_targetPosition);
        else
            playerNetwork.SendRpcPlaceBlock(_targetPosition);
    }

    private void InitAfterNetworkStarts(NetworkBehavior behavior)
    {
        if (playerNetwork.networkObject != null)
            DestroyIfNotOwned();

        if (playerNetwork.networkObject != null && playerNetwork.networkObject.IsOwner)
        {
            _inputMaster = controller.GetInputMaster();
            _inputMaster.BasicControls.AltFire.performed += delegate { PlaceBlock(); };
            _inputMaster.BasicControls.Fire.performed += delegate { _isMining = true; };
            _inputMaster.BasicControls.StopFire.performed += delegate
            {
                _mapManager.ResetTilesBeingMined();
                _isMining = false;
                if (playerNetwork == null || playerNetwork.networkObject == null)
                    pickaxeAnimator.MineAnimation(_targetPosition, false);
                else
                    playerNetwork.SendRpcMine(_targetPosition, false);
            };
        }
    }

    private void DestroyIfNotOwned()
    {
        if (!playerNetwork.networkObject.IsOwner)
            Destroy(this);
    }

    private void Update()
    {
         _targetPosition = ClampInRange(controller.GetCursorPositionInWorldSpace());
         _mapManager.mouseHover = _targetPosition;
         if (_isMining)
         {
             _mapManager.ManageTilesBeingMined();
             if (playerNetwork == null || playerNetwork.networkObject == null)
                 pickaxeAnimator.MineAnimation(_targetPosition, true);
             else
                 playerNetwork.SendRpcMine(_targetPosition, true);
         }
         _timeSinceLastPlacingBlocks += Time.deltaTime;
    }
    
    private Vector2 ClampInRange(Vector2 position)
    {
        var playerToPos = position - (Vector2) transform.position;
        var inRange = playerToPos.magnitude > range
            ? range * playerToPos.normalized
            : playerToPos;

        return (Vector2) transform.position + inRange;
    }

    private void ChangeSizeOfHoverTiles(int newSize)
    {
        _mapManager.ChangeSizeOfHoverTiles(newSize);
    }

    [ContextMenu("Change Size Of Hover Tiles To 2")]
    private void ChangeSizeOfHoverTilesTo2()
    {
        ChangeSizeOfHoverTiles(2);
    }

    private void OnDisable()
    {
        ChangeSizeOfHoverTiles(0);
    }

    private void OnEnable()
    {
        ChangeSizeOfHoverTiles(currentSizeOfSquareSelectionTiles);
    }
}