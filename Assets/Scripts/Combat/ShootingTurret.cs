﻿using BeardedManStudios.Forge.Networking;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Generated;
using EnvyDeserter.Combat;
using UnityEngine;
using UnityEngine.Events;

public class ShootingTurret : MonoBehaviour
{
    private bool isEnabled;
    public uint ownerID;
    [HideInInspector] public PlayerNetwork playerNetwork;
    [SerializeField] Transform shootingPosition;
    private ParticleSystem _particleSystem;

    private ProjectileWeapon projectileWeapon;

    private Building building;

    Transform currentTarget;

    List<Player> playersNearby = new List<Player>();

    [SerializeField] float minimumTimeBetweenShots = 2f;
    float timeSinceLastShot;

    Vector2 firingDirection = Vector2.right;

    [SerializeField] CircleCollider2D circleCollider2D;

    [SerializeField] float shootingRange = 5f;

    [SerializeField] private Color idleRangeColor = Color.green;
    [SerializeField] private Color shootingRangeColor = Color.red;
    private Color _currentColor = Color.clear;
    private ParticleSystem rangeIndicator;
    [SerializeField] private ItemHealthDisplay _display;
    private void Awake()
    {
        timeSinceLastShot = minimumTimeBetweenShots;
        _particleSystem = GetComponent<ParticleSystem>();
        ChangeRange(shootingRange);
        // Will probably not work, could not manage to change it as it was placed
        // To change the *real* starting color of the range, modify the particle system
        ChangeRangeColor(idleRangeColor, true);

        projectileWeapon = GetComponentInChildren<ProjectileWeapon>();
        if (projectileWeapon == null)
            Debug.LogWarning(name + " has a ShootingTurret component but no ProjectileWeapon in its children.");

        building = GetComponent<Building>();
        if (building == null)
            Debug.LogWarning(name + " has a ShootingTurret component but no Building.");
        
        _particleSystem.Stop();
    }

    private void Start()
    {
        projectileWeapon.Player = building.Owner;
        SetEnabled(true);
    }

    public void SetEnabled(bool value)
    {
        isEnabled = value;
        if (!isEnabled) _particleSystem.Stop();
        else _particleSystem.Play();
    }
    private void Update()
    {
        timeSinceLastShot += Time.deltaTime;

        if (playerNetwork == null || !playerNetwork.networkObject.IsOwner) return;

        if (playersNearby.Count <= 0)
        {
            ChangeRangeColor(idleRangeColor, true);
            return;
        }
        ChangeRangeColor(shootingRangeColor, true);

        if (timeSinceLastShot < minimumTimeBetweenShots) return;

        var closestSquaredDistance = Mathf.Infinity;
        var chosenIndex = 0;

        for (var i = 0; i < playersNearby.Count; i++)
        {
            if ((playersNearby[i].transform.position - shootingPosition.position).sqrMagnitude >= closestSquaredDistance)
                continue;

            closestSquaredDistance = (playersNearby[i].transform.position - shootingPosition.position).sqrMagnitude;
            chosenIndex = i;
        }

        currentTarget = playersNearby[chosenIndex].transform;
        timeSinceLastShot = 0f;
        firingDirection = currentTarget.position - shootingPosition.position;

        projectileWeapon?.Shoot(firingDirection.normalized);
    }

    public void AddPlayerToList(Collider2D collision)
    {
        if (playerNetwork == null
            || playerNetwork.networkObject == null
            || !playerNetwork.networkObject.IsOwner)
            return;

        Player newNearbyPlayer = collision.GetComponent<Player>();

        if (newNearbyPlayer == null) return;
        if (newNearbyPlayer.Network.ownerID == ownerID) return;

        playersNearby.Add(newNearbyPlayer);
    }

    public void RemovePlayerFromList(Collider2D collision)
    {
        if (playerNetwork == null || !playerNetwork.networkObject.IsOwner) return;
        Player newNearbyPlayer = collision.GetComponent<Player>();
        if (newNearbyPlayer != null)
        {
            if (newNearbyPlayer.Network.ownerID == ownerID)
            {
                return;
            }
            playersNearby.Remove(newNearbyPlayer);
        }
    }

    public void ChangeRange(float newRange)
    {
        shootingRange = newRange;
        circleCollider2D.radius = shootingRange;
        ParticleSystem.ShapeModule emissionShape = _particleSystem.shape;
        emissionShape.radius = shootingRange;
    }

    public void ActivateShooting()
    {
        circleCollider2D.enabled = true;
        circleCollider2D.radius = shootingRange;
    }

    public void DeactivateShooting()
    {
        circleCollider2D.enabled = false;
    }

    public void ChangeRangeColor(Color newColor, bool sendRPC = false)
    {
        if (newColor == _currentColor) return;
        _currentColor = newColor;

        if (sendRPC)
        {
            GetComponent<Building>()?.networkObject?.SendRpc(BuildingBehavior.RPC_CHANGE_TURRET_RANGE_COLOR,
                Receivers.All, newColor);
        }

        ParticleSystem.TrailModule particleSystemTrails = _particleSystem.trails;
        particleSystemTrails.colorOverLifetime = new ParticleSystem.MinMaxGradient(newColor);
    }

}
