﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayerEnteredRegion : MonoBehaviour
{

    [SerializeField] ShootingTurret shootingTurret;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        shootingTurret.AddPlayerToList(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        shootingTurret.RemovePlayerFromList(collision);
    }
}
