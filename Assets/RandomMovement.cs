﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Effects
{
    public class RandomMovement : MonoBehaviour
    {
        private float _random;

        [SerializeField] private float speed = 1f;
        [SerializeField] private float scale = 1f;

        [SerializeField] private float randomScale = 100f;
        
        private void Start()
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
            Random.InitState(GameManagerNetwork.gameManagerNetwork.numberOfConnectedPlayers);
            _random = Random.value * randomScale;
        }


        private void Update()
        {

                float x = scale * (Mathf.PerlinNoise(_random + 0 + (Time.time * speed * 2), _random + 1 + (Time.time * speed * 2)) - 0.5f);
                float y = scale * (Mathf.PerlinNoise(_random + 2 + (Time.time * speed * 2), _random + 3 + (Time.time * speed * 2)) - 0.5f);
                float z = scale * (Mathf.PerlinNoise(_random + 4 + (Time.time * speed * 2), _random + 5 + (Time.time * speed * 2)) - 0.5f);
                transform.localPosition = Vector3.up + new Vector3(x, y, z) * 1;

        }
    }
}
