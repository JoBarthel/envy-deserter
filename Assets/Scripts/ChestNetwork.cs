﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Inventory))]
public class ChestNetwork : Building
{
    Inventory chestInventory;
    public Inventory ChestInventory { get { return chestInventory; } }
    [SerializeField] ChestHealth chestHealth;

    Inventory playerInventory;
    


    public uint playerOwnerId;
    public PlayerNetwork playerNetworkOwner;

    bool networkAlreadyStarted = false;
    bool allInitializationsDone = false;

    [Space]
    [Header("Health regeneration to player")]
    [SerializeField] float timeBetweenDistanceChecksforPlayer = 1f;
    float timeSinceLastDistanceChecksforPlayer;

    [SerializeField] float timeToWaitToHealAfterPlayerIsHit = 5f;
    //[SerializeField] float timeSincePlayerWasLastHit;
    bool waitingDelayAfterHit = false;
    bool waitingDelayAfterHit2 = true;

    PlayerHealth playerHealth;

    bool canHealPlayer = false;

    [SerializeField] float distanceToAllowInteraction = 10f;

    [SerializeField] float timeBetweenHealInteractionsWhenHealing = 0.5f;
    float timeSinceLastHealInteraction;
    [SerializeField] int amountToHealPlayerPerInteraction = 3;

    [Space]
    [Header("Particles Systems")]
    [SerializeField] ParticleSystem healthParticlesSystem;
    [SerializeField] ParticleSystemForceField healthParticlesSystemForceField;

    [SerializeField] ParticleSystem[] areaOfEffectParticlesSystems;
    [SerializeField] ParticleSystemForceField[] areaOfEffectForceFields;

    [Space]
    [Header("Resources Transference to chest")]
    [SerializeField] float timeBetweenResourcesTransferInteractions = 0.5f;
    float timeSinceLastResourcesTransferInteraction;
    [SerializeField] int amountOfBlocksPerInteraction = 3;

    bool canTransferToPlayer = false;
    [SerializeField] bool transferIsOn = true;

    [Space]
    [Header("Minimap Marker")]
    [SerializeField] MinimapMarker minimapChestMarker;
    ColorChangeOnHit minimapChestMarkerColorChangeOnHit;

    protected override void NetworkStart()
    {
        base.NetworkStart();

        networkAlreadyStarted = true;
        
        for (int i = 0; i < areaOfEffectParticlesSystems.Length; i++)
        {
            ParticleSystem.ShapeModule shapeModule = areaOfEffectParticlesSystems[i].shape;
            shapeModule.radius = distanceToAllowInteraction - 0.2f;
        }

        for (int i = 0; i < areaOfEffectForceFields.Length; i++)
        {
            areaOfEffectForceFields[i].endRange = distanceToAllowInteraction + 1f;
        }

        if (networkObject.IsOwner)
        {
            if(minimapChestMarker == null)
            {
                minimapChestMarker = GameObject.Find("ChestMarker").GetComponent<MinimapMarker>();
            }
            minimapChestMarker.targetToFollow = transform;

            minimapChestMarkerColorChangeOnHit = minimapChestMarker.GetComponent<ColorChangeOnHit>();
            chestHealth.minimapChestMarkerColorChangeOnHit = minimapChestMarkerColorChangeOnHit;
        }

    }

    public override void TakeDamage(RpcArgs args)
    {
        chestHealth.TakeDamage(args.GetNext<int>());
        health = chestHealth.CurrentHealth;

    }

    public override void UpdateChestPosition(RpcArgs args)
    {
        Vector3 position = args.GetNext<Vector3>();
        transform.position = position;

        throw new System.NotImplementedException();
    }

    public override void UpdateChestInventory(RpcArgs args)
    {
        DropType dropType = (DropType)args.GetNext<int>();
        int amountAdded = args.GetNext<int>();
      
        chestInventory = GetComponent<Inventory>();
        if (chestInventory != null)
        {
            chestInventory.UpdateInventory(dropType, amountAdded);
        }
        
    }

    public override void AddDropItemToInventory(RpcArgs args)
    {
        int amountAdded = args.GetNext<int>();
        uint playerId = args.GetNext<uint>();
        DropType dropType = (DropType)args.GetNext<int>();


        if (playerOwnerId == playerId)
        {
            Inventory chestInventory = GetComponent<Inventory>();
            if (chestInventory != null)
            {
                chestInventory.UpdateInventory(dropType, amountAdded);
            }
        }
    }

    public void Initializations()
    {
        chestInventory = GetComponent<Inventory>();

        if (networkObject.IsOwner)
        {
            GameManagerNetwork.gameManagerNetwork.chestInventoryObserver.Subscribe(chestInventory);
        }

        playerOwnerId = ownerId;

        if (playerNetworkOwner == null)
            playerNetworkOwner = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(playerOwnerId);
        
        playerInventory = playerNetworkOwner.GetComponent<Inventory>();

        playerHealth = playerNetworkOwner.GetComponent<PlayerHealth>();

        timeSinceLastDistanceChecksforPlayer = timeBetweenDistanceChecksforPlayer;

        timeSinceLastHealInteraction = timeBetweenHealInteractionsWhenHealing;

        timeSinceLastResourcesTransferInteraction = timeBetweenResourcesTransferInteractions;

        healthParticlesSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);

        allInitializationsDone = true;
    }

    private void Update()
    {
        if (!networkAlreadyStarted || !allInitializationsDone) return;

        base.Update();

        timeSinceLastDistanceChecksforPlayer += Time.deltaTime;

        if(timeSinceLastDistanceChecksforPlayer >= timeBetweenDistanceChecksforPlayer)
        {
            timeSinceLastDistanceChecksforPlayer = 0f;
            if ((playerNetworkOwner.transform.position - transform.position).sqrMagnitude <= (distanceToAllowInteraction * distanceToAllowInteraction))
            {
                canHealPlayer = true;
                canTransferToPlayer = true;
            }
            else
            {
                canHealPlayer = false;
                canTransferToPlayer = false;
                healthParticlesSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }


        timeSinceLastHealInteraction += Time.deltaTime;
        if (canHealPlayer && !waitingDelayAfterHit2)
        {
            if (timeSinceLastHealInteraction >= timeBetweenHealInteractionsWhenHealing)
            {
                timeSinceLastHealInteraction = 0f;
                HealPlayer(amountToHealPlayerPerInteraction);
            }

            healthParticlesSystemForceField.transform.position = playerNetworkOwner.transform.position;
        }

        timeSinceLastResourcesTransferInteraction += Time.deltaTime;
        if (canTransferToPlayer && transferIsOn)
        {
            if(timeSinceLastResourcesTransferInteraction >= timeBetweenResourcesTransferInteractions)
            {
                timeSinceLastResourcesTransferInteraction = 0f;
                TransferResourcesToPlayer(amountOfBlocksPerInteraction);
            }
        }
        

    }

    IEnumerator playerWasHitCoroutine;
    public void PlayerWasHit()
    {
        if(playerWasHitCoroutine != null)
        {
            StopCoroutine(playerWasHitCoroutine);
        }
        playerWasHitCoroutine = WaitToHealPlayerAfterHit();
        StartCoroutine(playerWasHitCoroutine);
    }

    IEnumerator WaitToHealPlayerAfterHit()
    {
        float timeSinceHit = 0f;
        waitingDelayAfterHit2 = true;


        canHealPlayer = false;
        healthParticlesSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);

        while (timeSinceHit < timeToWaitToHealAfterPlayerIsHit)
        {
            timeSinceHit += Time.deltaTime;
            yield return new WaitForSeconds(0f);
        }

        if ((playerNetworkOwner.transform.position - transform.position).sqrMagnitude <= (distanceToAllowInteraction * distanceToAllowInteraction))
        {
            canHealPlayer = true;
        }
        waitingDelayAfterHit2 = false;

    }


    void HealPlayer(int amount)
    {
        if (playerHealth.CurrentHealth >= playerHealth.MaxHealth)
        {
            healthParticlesSystem.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            return;
        }

        healthParticlesSystem.Play(true);

        playerNetworkOwner?.networkObject.SendRpc(PlayerNetworkBehavior.RPC_TAKE_DAMAGE, Receivers.All, -amountToHealPlayerPerInteraction);
    }

    void TransferResourcesToPlayer(int amountOfBlocksToTransfer)
    {
        int currentAmountOfDirtBlocks = playerInventory.NumberOfDirtBlocks;
        int currentAmountOfCrystals = playerInventory.NumberOfCrystals;

        if(currentAmountOfDirtBlocks == 0 && currentAmountOfCrystals == 0)
        {
            return;
        }

        //Dirt Blocks
        for (int i = 0; i < amountOfBlocksToTransfer; i++)
        {
            if(currentAmountOfDirtBlocks <= 0)
            {
                break;
            }
            //instantiate dirt block drop item
            if (networkObject.IsOwner)
            {
                DropItemBehavior dropItemBehavior = NetworkManager.Instance.InstantiateDropItem(playerInventory.DirtBlockIndexToNetworkInstantiate, playerNetworkOwner.transform.position, Quaternion.identity);
                //set drop item to go towards chest
                DetectPlayerTouchedBlockDropItem detectPlayerTouchedBlockDropItem = dropItemBehavior.GetComponentInChildren<DetectPlayerTouchedBlockDropItem>();
                detectPlayerTouchedBlockDropItem.localAlreadySentRPC = true;

                DropItem dropItem = dropItemBehavior.GetComponent<DropItem>();
                dropItem.targetPlayerNetwork = playerNetworkOwner;

                if (dropItem.networkHasStarted)
                    dropItem.networkObject.SendRpc(DropItem.RPC_TRIGGER_DROP_ITEM,
                        Receivers.Owner,
                        dropItem.targetPlayerNetwork.networkObject.NetworkId,
                        transform.position,
                        (int)dropItem.dropType,
                        false);
                else
                    dropItem.networkStarted += dropItem.CallRpcAfterNetworkStartToGoToChest;

            }

            currentAmountOfDirtBlocks--;
        }
        //remove blocks from player's inventory
        if (networkObject.IsOwner)
        {
            playerNetworkOwner.networkObject.SendRpc(PlayerNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, Receivers.All, -(playerInventory.NumberOfDirtBlocks - currentAmountOfDirtBlocks), playerOwnerId, (int)DropType.Block);
        }
        
        //Crystal Blocks
        for (int i = 0; i < amountOfBlocksToTransfer; i++)
        {
            if (currentAmountOfCrystals <= 0)
            {
                break;
            }
            //instantiate crystal block drop item
            if (networkObject.IsOwner)
            {
                DropItemBehavior dropItemBehavior = NetworkManager.Instance.InstantiateDropItem(playerInventory.CrystalIndexToNetworkInstantiate, playerNetworkOwner.transform.position, Quaternion.identity);
                DetectPlayerTouchedBlockDropItem detectPlayerTouchedBlockDropItem = dropItemBehavior.GetComponentInChildren<DetectPlayerTouchedBlockDropItem>();
                detectPlayerTouchedBlockDropItem.localAlreadySentRPC = true;

                DropItem dropItem = dropItemBehavior.GetComponent<DropItem>();
                dropItem.targetPlayerNetwork = playerNetworkOwner;

                if (dropItem.networkHasStarted)
                    dropItem.networkObject.SendRpc(DropItem.RPC_TRIGGER_DROP_ITEM,
                        Receivers.Owner,
                        dropItem.targetPlayerNetwork.networkObject.NetworkId,
                        transform.position,
                        (int)dropItem.dropType,
                        false);
                else
                    dropItem.networkStarted += dropItem.CallRpcAfterNetworkStartToGoToChest;

            }

            currentAmountOfCrystals--;
        }
        //remove blocks from player's inventory
        if (networkObject.IsOwner)
        {
            playerNetworkOwner.networkObject.SendRpc(PlayerNetwork.RPC_ADD_DROP_ITEM_TO_INVENTORY, Receivers.All, -(playerInventory.NumberOfCrystals - currentAmountOfCrystals), playerOwnerId, (int)DropType.Crystal);
        }
    }
}
