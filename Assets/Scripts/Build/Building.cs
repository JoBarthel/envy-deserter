﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using EnvyDeserter.Combat;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer), typeof(PolygonCollider2D), typeof(Rigidbody2D))]
public class Building : BuildingBehavior, IDamageable, IDestroyable
{
    protected Sprite sprite = null;
    public Sprite Sprite => sprite ?? (sprite = SpriteRenderer.sprite);

    protected SpriteRenderer spriteRenderer = null;
    public SpriteRenderer SpriteRenderer => spriteRenderer ?? (spriteRenderer = GetComponent<SpriteRenderer>());

    protected new PolygonCollider2D collider = null;
    public PolygonCollider2D Collider => collider ?? (collider = GetComponent<PolygonCollider2D>());

    protected new Rigidbody2D rigidbody = null;
    public Rigidbody2D Rigidbody => rigidbody ?? (rigidbody = GetComponent<Rigidbody2D>());

    protected bool colliderSet;

    [SerializeField] protected int maxHealth = 100;
    [SerializeField] protected int health = 100;
    [SerializeField] private ItemHealthDisplay display;
    [SerializeField] public Slider healthBarSlider;  //UI slider used to represent the current HP of a building
    [SerializeField] ColorChangeOnHit colorChangeOnHit;

    public int Health => health;

    public int Team;
    public uint ownerId;

    public Player Owner;

    [SerializeField] protected int cost = 5;
    public int Cost => cost;

    protected bool activated;

    public bool Activated
    {
        get => activated;
        set
        {
            if (value && !colliderSet)
            {
                // snapping PolygonCollider points on the grid (assuming that one block = 1.0f)

                var newPoints = new Vector2[Collider.points.Length];

                for (var i = 0; i < newPoints.Length; i++)
                    newPoints[i] = new Vector2(Mathf.Round(Collider.points[i].x),
                        Mathf.Round(Collider.points[i].y));

                Collider.points = newPoints;

                colliderSet = true;
            }

            activated = value;
        }
    }

    protected void Awake()
    {
        transform.SetParent(Game.Instance.Buildings);

        health = maxHealth;

        if (healthBarSlider != null)
        {
            //healthBarSlider.transform.parent.gameObject.SetActive(true);
            healthBarSlider.maxValue = maxHealth;
            healthBarSlider.value = health;
        }
    }

    protected override void NetworkStart()
    {
        base.NetworkStart();

        ActivateAndShowElements();
    }

    private void Start()
    {
        if(display != null)
            display.Subscribe(this);
    }

    protected void Update()
    {
        if (networkObject == null) return;

        transform.localPosition = networkObject.position;
        Team = networkObject.team;
        Activated = networkObject.activated;
    }

    protected void OnTriggerEnter2D(Collider2D collider)
    {
        if (!Activated) return;

        var player = collider.GetComponent<Player>();

        if (player == null || player.Stats.Team != Team) return;
    }

    protected void OnTriggerExit2D(Collider2D collider)
    {
        if (!Activated) return;

        var player = collider.GetComponent<Player>();

        if (player == null || player.Stats.Team != Team) return;
    }

    public override void SetOwnerId(RpcArgs args)
    {
        Animator animator = GetComponent<Animator>();
        if (animator != null) animator.enabled = true;
        ownerId = args.GetNext<uint>();

        ShootingTurret shootingTurret = GetComponent<ShootingTurret>();
        if (shootingTurret != null)
        {
            shootingTurret.ownerID = ownerId;
            shootingTurret.playerNetwork = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(ownerId);
            shootingTurret.ActivateShooting();
        }

        ChestNetwork chestNetwork = GetComponent<ChestNetwork>();
        if(chestNetwork != null)
        {
            chestNetwork.playerOwnerId = ownerId;
            chestNetwork.playerNetworkOwner = GameManagerNetwork.gameManagerNetwork.GetPlayerNetworkById(ownerId);
            chestNetwork.Initializations();
        }
    }

    public void SetIdAfterNetworkStart(NetworkBehavior networkBehavior)
    {
        if (networkObject == null)
        {
            Debug.LogWarning("Building.cs, SetIdAfterNetworkStart : networkObject is null.");
            return;
        }

        networkObject.SendRpc(Building.RPC_SET_OWNER_ID, Receivers.All, ownerId);
    }

    public override void TakeDamage(RpcArgs args)
    {
        TakeDamage(args.GetNext<int>());
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (healthBarSlider != null)
        {
            healthBarSlider.value = health;
        }

        if(colorChangeOnHit != null && damage > 0)
        {
            colorChangeOnHit.TakeDamage(damage);
        }

        foreach (UnityAction<float> callback in _onHealthChangedCallbacks)
        {
            callback.Invoke(health);
        }
        if (health <= 0) Die();
    }

    [ContextMenu("Take 40 Damage")]
    public void Take40Damage() => TakeDamage(40);

    public void Die()
    {
        Destroy(gameObject);
    }

    //Use this function to activate and/or show elements that should not be visible in the building gun construction indicator
    public void ActivateAndShowElements()
    {
        if(healthBarSlider != null) healthBarSlider.transform.parent.gameObject.SetActive(true);
    }

    public override void UpdateChestPosition(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public override void UpdateChestInventory(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public override void AddDropItemToInventory(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public override void ChangeTurretRangeColor(RpcArgs args)
    {
        GetComponent<ShootingTurret>()?.ChangeRangeColor(args.GetNext<Color>());
    }
    private readonly List<UnityAction<float>> _onHealthChangedCallbacks = new List<UnityAction<float>>();
    public void AddOnHealthChanged(UnityAction<float> callback)
    {
        _onHealthChangedCallbacks.Add(callback);
    }
    
}