﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Lobby;
using BeardedManStudios.Forge.Networking.Unity;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerNetwork : GameManagerNetworkBehavior
{
    public static GameManagerNetwork gameManagerNetwork;

    public List<IClientMockPlayer> currentPlayers;

    List<PlayerNetwork> connectedPlayers = new List<PlayerNetwork>();

    public uint localPlayerId;

    public int numberOfConnectedPlayers => connectedPlayers.Count;

    //Cinemachine
    [Header("Scene's Cinemachine")]
    [SerializeField] CinemachineVirtualCamera cinemachineVirtualCamera;

    [SerializeField] float timeIntervalBetweenConnectionChecks = 5f;
    float timeSinceLastConnectionCheck;

    [SerializeField] private Player playerPrefab;
    private Player player;
    private PlayerNetwork playerNetwork;

    [Space]
    [SerializeField] Transform minimapPlayerMarker;

    [Space]
    [Header("UI")]
    [SerializeField] private Slider healthBarSlider;  //UI slider used to represent the current HP of a player
    [SerializeField] private InventoryObserver inventoryObserver;
    public InventoryObserver chestInventoryObserver;
    [SerializeField] private ModeSelectionObserver modeSelectionObserver;
    [SerializeField] private GrapplingHookObserver grapplingHookObserver;
    [SerializeField] private CursorArrow cursorArrow;

    [SerializeField] private GameEndPanel gameEndPanel;

    public readonly List<Player> AlivePlayers = new List<Player>();

    [SerializeField] private FollowMouse followMouse;

    private void Awake()
    {
        gameManagerNetwork = this;
    }

    protected override void NetworkStart()
    {
        base.NetworkStart();

        timeSinceLastConnectionCheck = timeIntervalBetweenConnectionChecks;
    }

    public void SpawnPlayer(Vector3 positionPlayerSpawned)
    {
        if (NetworkManager.Instance == null)
        {
            player = Instantiate(playerPrefab, positionPlayerSpawned, Quaternion.identity);
        }
        else
        {
            playerNetwork = (PlayerNetwork) NetworkManager.Instance.InstantiatePlayerNetwork(0, positionPlayerSpawned, Quaternion.identity, true);
            playerNetwork.networkStarted += UpdateLocalPlayerId;
            player = playerNetwork.GetComponent<Player>();
        }
        
        AlivePlayers.Add(player);

        //Setting UI
        PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
        if (playerHealth != null) playerHealth.healthBarSlider = healthBarSlider;
        
        Inventory playerInventory = player.GetComponent<Inventory>();
        inventoryObserver.Subscribe(playerInventory);
        
        GunSelection gunSelection = player.GetComponentInChildren<GunSelection>();
        modeSelectionObserver.Subscribe(gunSelection);
        
        GrapplingHook grapplingHook = player.GetComponentInChildren<GrapplingHook>();
        grapplingHookObserver.Subscribe(grapplingHook);
        
        cursorArrow.controller = player.Controller;
        
        BuildWeapon buildWeapon = player.GetComponentInChildren<BuildWeapon>();
        inventoryObserver.PlayFeedbackOnFailedBuild(buildWeapon);
        chestInventoryObserver.PlayFeedbackOnFailedBuild(buildWeapon);

        PickaxeAnimator pickaxeAnimator = player.GetComponentInChildren<PickaxeAnimator>();
        inventoryObserver.PlayFeedbackOnFailedPlace(pickaxeAnimator);
        chestInventoryObserver.PlayFeedbackOnFailedPlace(pickaxeAnimator);
        
        followMouse.SetController(player.Controller);
        cinemachineVirtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
        cinemachineVirtualCamera.Follow = player.transform;
        cinemachineVirtualCamera.LookAt = player.transform;

        Vector3 minimapPlayerMarkerPosition = player.transform.position;
        minimapPlayerMarkerPosition.z = 1f;
        minimapPlayerMarker.transform.position = minimapPlayerMarkerPosition;
        minimapPlayerMarker.SetParent(player.transform);
    }

    void UpdateLocalPlayerId(NetworkBehavior playerNetwork)
    {
        localPlayerId = this.playerNetwork.networkObject.NetworkId;
    }

    private void Update()
    {
        if (LobbyService.Instance == null) return;

        timeSinceLastConnectionCheck += Time.deltaTime;

        if(timeSinceLastConnectionCheck >= timeIntervalBetweenConnectionChecks)
        {
            timeSinceLastConnectionCheck = 0f;

            //Get list of players connected
            currentPlayers = LobbyService.Instance.MasterLobby.LobbyPlayers;
        }
    }

    public void RegisterNewPlayer(PlayerNetwork newPlayerNetwork)
    {
        connectedPlayers.Add(newPlayerNetwork);
    }

    void UnregisterNewPlayer(PlayerNetwork newPlayerNetwork)
    {
        connectedPlayers.Remove(newPlayerNetwork);
    }

    public PlayerNetwork GetPlayerNetworkById(uint id)
    {
        PlayerNetwork playerNetworkToReturn = null;
        for (int i = 0; i < connectedPlayers.Count; i++)
        {
            if(connectedPlayers[i].networkObject.NetworkId == id)
            {
                playerNetworkToReturn = connectedPlayers[i];
                break;
            }
        }

        return playerNetworkToReturn;
    }

    public void PlayerLost(Player deadPlayer)
    {
        AlivePlayers.Remove(deadPlayer);

        if (deadPlayer.Network != null
            && deadPlayer.Network.networkObject != null
            && deadPlayer.Network.networkObject.IsOwner)
        {
            gameEndPanel.gameObject.SetActive(true);
            gameEndPanel.Text.text = "You lose";
            deadPlayer.gameObject.SetActive(false);

            return;
        }

        if (AlivePlayers.Count == 1
            && AlivePlayers[0].Network != null
            && AlivePlayers[0].Network.networkObject != null
            && AlivePlayers[0].Network.networkObject.IsOwner)
        {
            gameEndPanel.gameObject.SetActive(true);
            gameEndPanel.Text.text = "You win";
        }
    }

    public override void DisconnectFromServer(RpcArgs args)
    {
        //NetworkManager.Instance.Disconnect();
        SceneManager.LoadScene(1);
    }
}
