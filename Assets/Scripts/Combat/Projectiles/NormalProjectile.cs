﻿using UnityEngine;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;

[RequireComponent(typeof(Rigidbody2D))]
public class NormalProjectile : Projectile
{
    protected override void Hit(Player player)
    {
        player?.Network?.networkObject?.SendRpc(PlayerNetworkBehavior.RPC_TAKE_DAMAGE, Receivers.All, damage);
    }

    protected override void Hit(Building building)
    {
        if (building.networkObject == null) building.TakeDamage(damage);
        else building.networkObject.SendRpc(BuildingBehavior.RPC_TAKE_DAMAGE, Receivers.All, damage);
    }

    protected override void Hit() {} // Terrain
}