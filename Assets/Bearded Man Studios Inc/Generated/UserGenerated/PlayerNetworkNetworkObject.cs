using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0.15,0,0.15,0,0]")]
	public partial class PlayerNetworkNetworkObject : NetworkObject
	{
		public const int IDENTITY = 16;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _position;
		public event FieldEvent<Vector3> positionChanged;
		public InterpolateVector3 positionInterpolation = new InterpolateVector3() { LerpT = 0.15f, Enabled = true };
		public Vector3 position
		{
			get { return _position; }
			set
			{
				// Don't do anything if the value is the same
				if (_position == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_position = value;
				hasDirtyFields = true;
			}
		}

		public void SetpositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_position(ulong timestep)
		{
			if (positionChanged != null) positionChanged(_position, timestep);
			if (fieldAltered != null) fieldAltered("position", _position, timestep);
		}
		[ForgeGeneratedField]
		private bool _isGrounded;
		public event FieldEvent<bool> isGroundedChanged;
		public Interpolated<bool> isGroundedInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool isGrounded
		{
			get { return _isGrounded; }
			set
			{
				// Don't do anything if the value is the same
				if (_isGrounded == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_isGrounded = value;
				hasDirtyFields = true;
			}
		}

		public void SetisGroundedDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_isGrounded(ulong timestep)
		{
			if (isGroundedChanged != null) isGroundedChanged(_isGrounded, timestep);
			if (fieldAltered != null) fieldAltered("isGrounded", _isGrounded, timestep);
		}
		[ForgeGeneratedField]
		private float _velocityX;
		public event FieldEvent<float> velocityXChanged;
		public InterpolateFloat velocityXInterpolation = new InterpolateFloat() { LerpT = 0.15f, Enabled = true };
		public float velocityX
		{
			get { return _velocityX; }
			set
			{
				// Don't do anything if the value is the same
				if (_velocityX == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_velocityX = value;
				hasDirtyFields = true;
			}
		}

		public void SetvelocityXDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_velocityX(ulong timestep)
		{
			if (velocityXChanged != null) velocityXChanged(_velocityX, timestep);
			if (fieldAltered != null) fieldAltered("velocityX", _velocityX, timestep);
		}
		[ForgeGeneratedField]
		private bool _flipX;
		public event FieldEvent<bool> flipXChanged;
		public Interpolated<bool> flipXInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool flipX
		{
			get { return _flipX; }
			set
			{
				// Don't do anything if the value is the same
				if (_flipX == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x8;
				_flipX = value;
				hasDirtyFields = true;
			}
		}

		public void SetflipXDirty()
		{
			_dirtyFields[0] |= 0x8;
			hasDirtyFields = true;
		}

		private void RunChange_flipX(ulong timestep)
		{
			if (flipXChanged != null) flipXChanged(_flipX, timestep);
			if (fieldAltered != null) fieldAltered("flipX", _flipX, timestep);
		}
		[ForgeGeneratedField]
		private bool _hasChest;
		public event FieldEvent<bool> hasChestChanged;
		public Interpolated<bool> hasChestInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool hasChest
		{
			get { return _hasChest; }
			set
			{
				// Don't do anything if the value is the same
				if (_hasChest == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x10;
				_hasChest = value;
				hasDirtyFields = true;
			}
		}

		public void SethasChestDirty()
		{
			_dirtyFields[0] |= 0x10;
			hasDirtyFields = true;
		}

		private void RunChange_hasChest(ulong timestep)
		{
			if (hasChestChanged != null) hasChestChanged(_hasChest, timestep);
			if (fieldAltered != null) fieldAltered("hasChest", _hasChest, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			positionInterpolation.current = positionInterpolation.target;
			isGroundedInterpolation.current = isGroundedInterpolation.target;
			velocityXInterpolation.current = velocityXInterpolation.target;
			flipXInterpolation.current = flipXInterpolation.target;
			hasChestInterpolation.current = hasChestInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _position);
			UnityObjectMapper.Instance.MapBytes(data, _isGrounded);
			UnityObjectMapper.Instance.MapBytes(data, _velocityX);
			UnityObjectMapper.Instance.MapBytes(data, _flipX);
			UnityObjectMapper.Instance.MapBytes(data, _hasChest);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_position = UnityObjectMapper.Instance.Map<Vector3>(payload);
			positionInterpolation.current = _position;
			positionInterpolation.target = _position;
			RunChange_position(timestep);
			_isGrounded = UnityObjectMapper.Instance.Map<bool>(payload);
			isGroundedInterpolation.current = _isGrounded;
			isGroundedInterpolation.target = _isGrounded;
			RunChange_isGrounded(timestep);
			_velocityX = UnityObjectMapper.Instance.Map<float>(payload);
			velocityXInterpolation.current = _velocityX;
			velocityXInterpolation.target = _velocityX;
			RunChange_velocityX(timestep);
			_flipX = UnityObjectMapper.Instance.Map<bool>(payload);
			flipXInterpolation.current = _flipX;
			flipXInterpolation.target = _flipX;
			RunChange_flipX(timestep);
			_hasChest = UnityObjectMapper.Instance.Map<bool>(payload);
			hasChestInterpolation.current = _hasChest;
			hasChestInterpolation.target = _hasChest;
			RunChange_hasChest(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _position);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _isGrounded);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _velocityX);
			if ((0x8 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _flipX);
			if ((0x10 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _hasChest);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (positionInterpolation.Enabled)
				{
					positionInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					positionInterpolation.Timestep = timestep;
				}
				else
				{
					_position = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_position(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (isGroundedInterpolation.Enabled)
				{
					isGroundedInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					isGroundedInterpolation.Timestep = timestep;
				}
				else
				{
					_isGrounded = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_isGrounded(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (velocityXInterpolation.Enabled)
				{
					velocityXInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					velocityXInterpolation.Timestep = timestep;
				}
				else
				{
					_velocityX = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_velocityX(timestep);
				}
			}
			if ((0x8 & readDirtyFlags[0]) != 0)
			{
				if (flipXInterpolation.Enabled)
				{
					flipXInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					flipXInterpolation.Timestep = timestep;
				}
				else
				{
					_flipX = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_flipX(timestep);
				}
			}
			if ((0x10 & readDirtyFlags[0]) != 0)
			{
				if (hasChestInterpolation.Enabled)
				{
					hasChestInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					hasChestInterpolation.Timestep = timestep;
				}
				else
				{
					_hasChest = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_hasChest(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (positionInterpolation.Enabled && !positionInterpolation.current.UnityNear(positionInterpolation.target, 0.0015f))
			{
				_position = (Vector3)positionInterpolation.Interpolate();
				//RunChange_position(positionInterpolation.Timestep);
			}
			if (isGroundedInterpolation.Enabled && !isGroundedInterpolation.current.UnityNear(isGroundedInterpolation.target, 0.0015f))
			{
				_isGrounded = (bool)isGroundedInterpolation.Interpolate();
				//RunChange_isGrounded(isGroundedInterpolation.Timestep);
			}
			if (velocityXInterpolation.Enabled && !velocityXInterpolation.current.UnityNear(velocityXInterpolation.target, 0.0015f))
			{
				_velocityX = (float)velocityXInterpolation.Interpolate();
				//RunChange_velocityX(velocityXInterpolation.Timestep);
			}
			if (flipXInterpolation.Enabled && !flipXInterpolation.current.UnityNear(flipXInterpolation.target, 0.0015f))
			{
				_flipX = (bool)flipXInterpolation.Interpolate();
				//RunChange_flipX(flipXInterpolation.Timestep);
			}
			if (hasChestInterpolation.Enabled && !hasChestInterpolation.current.UnityNear(hasChestInterpolation.target, 0.0015f))
			{
				_hasChest = (bool)hasChestInterpolation.Interpolate();
				//RunChange_hasChest(hasChestInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerNetworkNetworkObject() : base() { Initialize(); }
		public PlayerNetworkNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerNetworkNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
