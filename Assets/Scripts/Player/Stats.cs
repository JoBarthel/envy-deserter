﻿using UnityEngine;

namespace Assets.Scripts.Character
{
    public class Stats : MonoBehaviour
    {
        #region Fields

        [Header("Ground horizontal speed parameters")]
        [SerializeField] [Range(0, 20)]
        [Tooltip("Speed the player goes at if he is on the ground and his horizontal input is at its maximum")]
        private float horizontalGroundSpeedMax = 10.0f;
        [SerializeField] [Range(0, 200)]
        [Tooltip("Acceleration of the player when he is on the ground and his horizontal input is not null")]
        private float horizontalGroundAcceleration = 80.0f;
        [SerializeField] [Range(0, 200)]
        [Tooltip("Acceleration of the player when he is on the ground and his horizontal input is null")]
        private float horizontalGroundDeceleration = 40.0f;

        [Header("Air horizontal speed parameters")]
        [SerializeField] [Range(0, 20)]
        [Tooltip("Speed the player goes at if he is in the air and his horizontal input is at its maximum")]
        private float airSpeedMax = 10.0f;
        [SerializeField] [Range(0, 200)]
        [Tooltip("Acceleration of the player when he is in the air and his horizontal input is not null")]
        private float airAcceleration = 40.0f;
        [SerializeField] [Range(0, 200)]
        [Tooltip("Acceleration of the player when he is in the air and his horizontal input is null")]
        private float airDeceleration = 20.0f;

        [Header("Grappling Hook parameters")]
        [SerializeField] [Range(0, 40)]
        [Tooltip("Maximum speed at which the player can swing at the end of his rope")]
        private float swingSpeedMax = 30.0f;
        [SerializeField] [Range(0, 40)]
        [Tooltip("Acceleration of the player when he swings on his rope")]
        private float swingSideAcceleration = 10.0f;
        [SerializeField]
        [Range(0, 100)] [Tooltip("Down acceleration (constant) when swinging on the rope")]
        private float swingDownAcceleration = 50.0f;
        [SerializeField]
        [Range(0, 5)] [Tooltip("Friction of the swing")]
        private float swingFriction = 0.5f;

        [Header("Jump parameters")]
        [SerializeField] [Tooltip("x : time, y : height of the jump")]
        private AnimationCurve jumpCurve;
        [SerializeField] [Tooltip("Time at which the jump starts going down")]
        private float jumpMiddle = 0.4f;
        [SerializeField] [Tooltip("Time after exiting a platform during which you can still jump")]
        private float coyoteTime = 0.1f;
        [SerializeField] [Tooltip("Number of jumps the player is allowed to do before landing")]
        private int jumpCount = 1;

        [SerializeField] private float jumpInputTimeMax = 0.3f;

        [SerializeField] [Range(0, 100)] [Tooltip("Maximum health of the player")]
        private int healthMax = 10;
        private int health;

        public int Team;

        #endregion

        #region Properties

        public float HorizontalGroundSpeedMax => horizontalGroundSpeedMax;
        public float HorizontalGroundAcceleration => horizontalGroundAcceleration;
        public float HorizontalGroundDeceleration => horizontalGroundDeceleration;

        public float AirSpeedMax => airSpeedMax;
        public float AirAcceleration => airAcceleration;
        public float AirDeceleration => airDeceleration;
        
        public float SwingSpeedMax => swingSpeedMax;
        public float SwingSideAcceleration => swingSideAcceleration;
        public float SwingDownAcceleration => swingDownAcceleration;
        public float SwingFriction => swingFriction;

        public float CoyoteTime => coyoteTime;
        public float JumpMiddle => jumpMiddle;
        public float JumpEnd => jumpCurve[jumpCurve.length - 1].time;
        public float JumpCount => jumpCount;
        public float JumpInputTimeMax => jumpInputTimeMax;
        public float FallSpeed => jumpCurve[jumpCurve.length - 1].inTangent;

        public int HealthMax => healthMax;
        public int Health
        {
            get => health;
            set
            {
                if (value <= 0)
                {
                    health = 0;
                    // TODO raiso player death event
                }
                else if (value > healthMax) health = healthMax;
                else health = value;
            }
        }

        #endregion

        #region Unity Event Functions

        private void OnEnable()
        {
            health = healthMax;
        }

        #endregion

        #region Methods

        public float GetJumpPosition(float atTime)
        {
            if (atTime < jumpCurve[0].time
                || atTime > jumpCurve[jumpCurve.length - 1].time)
                return 0.0f;

            return jumpCurve.Evaluate(atTime);
        }

        public float GetJumpSpeed(float atTime)
        {
            if (atTime - Time.deltaTime < jumpCurve[0].time)
                return jumpCurve[0].outTangent;

            if (atTime > jumpCurve[jumpCurve.length - 1].time)
                return jumpCurve[jumpCurve.length - 1].inTangent;

            return (jumpCurve.Evaluate(atTime) - jumpCurve.Evaluate(atTime - Time.deltaTime)) / Time.deltaTime;
        }

        public float GetJumpTime(float atSpeed)
        {
            if (atSpeed > jumpCurve[0].outTangent)
                return 0.0f;

            if (atSpeed < jumpCurve[jumpCurve.length - 1].inTangent)
                return jumpCurve[jumpCurve.length - 1].time;

            var derived = jumpCurve[0].outTangent;
            var dt = 0.1f;
            var t = 0.0f;

            while (derived < atSpeed && t + dt < jumpCurve[jumpCurve.length - 1].time)
            {
                derived = (jumpCurve.Evaluate(t + dt) - jumpCurve.Evaluate(t)) / dt;
                t += dt;
            }

            return t;
        }

        #endregion
    }
}
