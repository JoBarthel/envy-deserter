﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class MouseCursorHandler : MonoBehaviour
{
    [SerializeField] private Texture2D targetTexture;

    private static MouseCursorHandler instance;

    public static MouseCursorHandler Instance
    {
        get => instance;
        set => instance = value;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            if (Instance != this) Destroy(this);
        }
        else
            Instance = this;
    }

    public void SetTargetingMode()
    {
        Cursor.SetCursor(targetTexture, Vector2.zero, CursorMode.Auto);
    }

}
