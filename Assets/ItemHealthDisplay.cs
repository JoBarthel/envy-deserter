﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using EnvyDeserter.Combat;
using TMPro;
using UnityEngine;

public class ItemHealthDisplay : MonoBehaviour
{
    private Canvas _canvas;
    [SerializeField] private TextMeshProUGUI text;

    private void Start()
    {
        _canvas = GetComponentInParent<Canvas>();
        if(_canvas != null)
            _canvas.worldCamera = Camera.main;
    }

    public void Subscribe(IDestroyable item)
    {
        item.AddOnHealthChanged(SetText);
    }

    private void SetText(float value)
    {
        // TODO nullref here
        if(text != null) text.text = value.ToString(CultureInfo.CurrentCulture);
    }
}
