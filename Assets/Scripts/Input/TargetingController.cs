﻿using System.CodeDom;
using UnityEngine;
using UnityEngine.InputSystem;

/**
 * Handles cursor movement difference between controller control schemes
 * and mouse control schemes
 */
public class TargetingController
{
    private enum DeviceUsed
    {
        Keyboard,
        Gamepad
    }

    private DeviceUsed _currentDeviceUsed = DeviceUsed.Keyboard;
    private readonly InputMaster _inputMaster;

    public TargetingController(InputMaster inputMaster, float range, float margin)
    {
        _inputMaster = inputMaster;
        _range = range;
        _margin = margin;
    }

    public Vector2 GetCursorPositionInWorldSpace(Vector2 playerPosition)
    {
        if (Gamepad.current != null)
            return GetGamepadCursorPosition(playerPosition);
        
        return Camera.main.ScreenToWorldPoint(_inputMaster.BasicControls.CursorPosition.ReadValue<Vector2>());
    }

    public Vector2 GetCursorPositionInScreenSpace(Vector2 playerPosition)
    {
        if (Gamepad.current != null)
            return Camera.main.WorldToScreenPoint(GetGamepadCursorPosition(playerPosition));
        return _inputMaster.BasicControls.CursorPosition.ReadValue<Vector2>();
    }

    private readonly float _range;
    private Vector2 _previousTargetingStickDirection = Vector2.zero;
    private Vector2 GetGamepadCursorPosition(Vector2 playerPosition)
    {
        Vector2 stickDirection = _inputMaster.BasicControls.TargetingDirection.ReadValue<Vector2>();
        if (IsCloseToZero(stickDirection)) return _previousTargetingStickDirection * _range + playerPosition;
        _previousTargetingStickDirection = stickDirection;
        return stickDirection * _range + playerPosition;
    }

    private readonly float _margin;
    private bool IsCloseToZero(Vector2 stickDirection)
    {
        return stickDirection.x < _margin 
               && stickDirection.x > -_margin 
               && stickDirection.y < _margin 
               && stickDirection.y > -_margin;
    }

    public void DetectDeviceUsed()
    {
        if (Gamepad.current != null && _currentDeviceUsed == DeviceUsed.Keyboard && Gamepad.current.wasUpdatedThisFrame)
            _currentDeviceUsed = DeviceUsed.Gamepad;
        else if (_currentDeviceUsed == DeviceUsed.Gamepad && Keyboard.current.wasUpdatedThisFrame)
            _currentDeviceUsed = DeviceUsed.Keyboard;
    }

    public Vector2 GetLookPosition(Vector3 playerPosition)
    {
        if (Gamepad.current != null)
            return GetGamepadLookPosition(playerPosition);
        return Camera.main.ScreenToWorldPoint(_inputMaster.BasicControls.CursorPosition.ReadValue<Vector2>());
    }

    private Vector2 _previousMovingStickDirection = Vector2.zero;
    private Vector2 GetGamepadLookPosition(Vector2     playerPosition)
    {
        var stickXDirection = _inputMaster.BasicControls.Move.ReadValue<float>();
        var stickYDirection = _inputMaster.BasicControls.Climb.ReadValue<float>();
        Vector2 stickDirection = new Vector2(stickXDirection, stickYDirection);
        if (IsCloseToZero(stickDirection)) return _previousMovingStickDirection * _range + playerPosition;
        _previousMovingStickDirection = stickDirection;
        return stickDirection * _range + playerPosition;
    }
}
