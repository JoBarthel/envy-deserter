﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeamManager : MonoBehaviour
{
    private static int playerCounter;

    // Start is called before the first frame update
    void Start()
    {
        playerCounter++;
        gameObject.tag = $"Player{playerCounter.ToString()}";
        //var gun = GetComponentInChildren<ProjectileWeapon>();
        var gun = GetComponentInChildren<NormalProjectileWeapon>();
        //gun.gameObject.tag = gameObject.tag;
    }
}
