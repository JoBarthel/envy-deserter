﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterAnimation : MonoBehaviour
{
    private void Awake()
    {
        Animator anim = GetComponent<Animator>();

        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

        float timeToDisappear = clips[0].length;

        Destroy(gameObject, timeToDisappear);
    }
}
